import React, { Component } from 'react';
import { View, Modal, Text, TextInput, ScrollView, Image, TouchableOpacity, Alert, Linking, Platform } from 'react-native';
import { Container, Body, Card, CheckBox, ListItem } from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import SubHeader from '../../Components/SubHeader'
import MainHeader from '../../Components/MainHeader'
import * as commanFn from '../../utility/index';
import * as Service from '../../api/services';
import * as Url from '../../constants/urls';
import Loader from '../../Components/Loader'
import styles from './style';
import AsyncStorage from '@react-native-community/async-storage';

export default class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedId: '',
      categories: [],
      productCategories: [],
      noOfProductCategoriesSelected: 0,
      storeId: '',
      vendorName: '',
      isLoading: false,
      modalVisible: false,
      products: [],
      shipTo: '',
      name: '',
      search: '',
      searchSelectedCategory: '',
    };
    this.getProducts()
    this.getAddress()
    this.getCategoires()
  }

  componentDidMount = async () => {
    let storeId = await AsyncStorage.getItem('storeId');
    this.setState({
      storeId: storeId
    });
  }

  getAddress = async () => {
    let res = await commanFn.getFromLocalStorge('address')
    let addressDetail = JSON.parse(res)
    console.log("addressDetail.address", addressDetail)
    if (addressDetail !== null) {
      this.setState({
        shipTo: addressDetail.address,
        name: addressDetail.name
      })
    }
  }

  goToPdfViewer = (pdf) => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'PdfViewer' })]
      })
    );
    this.props.navigation.navigate('PdfViewer', { pdf: pdf });
  }

  getCategoires = async () => {
    this.setState({
      isLoading: true
    })
    let token = await commanFn.getFromLocalStorge('token')
    let customerGroup = await commanFn.getFromLocalStorge('customerGroup')
    const res = await Service.get(Url.FavouriteProductCategories_URL + `categoryId=${customerGroup}`, token)
    console.log('products::getCategoires::categories::res', res)
    if (res.data) {
      this.setState({
        productCategories: res.data.categories,
        noOfProductCategoriesSelected: res.data.favouritesCount,
        isLoading: false
      })

    }
  }

  updateFavouriteSubCategories = async (favourites) => {

    let body = []
    let token = await commanFn.getFromLocalStorge('token')
    if (favourites && favourites.length) {
      favourites.forEach(item => {
        if (item.isShow) {
          body.push({ id: item.id })
        }
      });
      console.log('updateFavouriteSubCategories::favouriteSubCategories::body', { favouriteSubCategories: this.state.noOfProductCategoriesSelected })
      if (this.state.noOfProductCategoriesSelected !== 0) {
        this.setState({
          isLoading: true
        })
        const res = await Service.put(Url.UPDATE_FAVOURITE_SUBCATEGORIES_URL, token, { favouriteSubCategories: body })
        if (res) {
          this.setState({
            isLoading: false
          })
        }
      }

    }

  }

  setModalVisible = async (visible, productCategories) => {
    this.setState({
      modalVisible: visible,
    });
    if (productCategories !== undefined) {
      this.setState({
        productCategories: productCategories,
      });
    }
    if (visible === false) {
      this.updateFavouriteSubCategories(this.state.productCategories)
    }
  }

  getProducts = async (getProductsAs) => {
    this.setState({
      isLoading: true
    })
    let customerGroup = await commanFn.getFromLocalStorge('customerGroup')
    let res
    try {
      console.log('global.user.customerGroup.idglobal.user.customerGroup.id..??', customerGroup)
      let userId = await commanFn.getFromLocalStorge('userId')
      if (getProductsAs === 'viewall') {
        res = await Service.get(Url.ProductList_URL + `pageNo=${1}&pageSize=${1000}&storeId=${this.state.storeId}&userId=${userId}&role=customer&customerGroup=${customerGroup}`, "")
      } else if (getProductsAs === 'viewentirecatalogue') {
        res = await Service.get(Url.ProductList_URL + `pageNo=${1}&pageSize=${1000}&storeId=${this.state.storeId}&userId=${userId}&role=customer&excludeCustomerGroup=true&customerGroup=${customerGroup}`, "")
      }
      else {
        res = await Service.get(Url.ProductList_URL + `pageNo=${1}&pageSize=${5}&storeId=${this.state.storeId}&userId=${userId}&role=customer&customerGroup=${customerGroup}`, "")
      }
      console.log('proucts', res)
      this.setState({
        isLoading: false
      })
      if (res.items) {
        this.setState({
          products: res.items,
          isLoading: false
        })
      } else {
        this.setState({
          isLoading: false
        })
        Alert.alert('', 'Something Went Wrong')
      }
    } catch (err) {
      this.setState({ isLoading: false })
      Alert.alert('', err.message)
    }
  }

  onCheckBoxPress = async (value, key) => {
    console.log('onCheckBoxPress.....', key)
    let a = false
    let count = this.state.noOfProductCategoriesSelected
    this.state.productCategories.forEach(item => {
      if (item.id == value.id) {
        if (item.isShow) {
          console.log('item.isShow::minus', count)
          if (count < 7) {
            count = this.state.noOfProductCategoriesSelected - 1
            console.log('minus', count)
            this.state.productCategories[key].isShow = false;
          }
        }
        else {
          a = true
          if (count < 6) {
            count = this.state.noOfProductCategoriesSelected + 1
            console.log('add', count)
          } else {
            alert("you cann't select more than 6 categories")
            return
          }
          return this.state.productCategories[key].isShow = true;
        }
      }
    });
    await this.setState({
      productCategories: this.state.productCategories,
      noOfProductCategoriesSelected: count
    });
  }

  getProductDetial = (product) => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'ProductDetails' })]
      })
    );
    this.props.navigation.navigate('ProductDetails', product)
  }

  openDailer = () => {
    let number = '';
    if (Platform.OS === 'ios') {
      number = 'telprompt:${091123456789}';
    }
    else {
      number = 'tel:${091123456789}';
    }
    Linking.openURL(number);
  }

  openMessages = () => {
    let message = '';
    if (Platform.OS === 'ios') {
      message = 'sms:1-408-555-1212'
    }
    else {
      message = 'sms:1-408-555-1212?body=yourMessage'
    }
    Linking.openURL(message);
  }

  searchProduct = async () => {
    // this.setState({
    //   isLoading: true
    // })
    console.log('this.state.selectedCategories', this.state.productCategories)
    let selected = ''
    let isFirst = true
    this.state.productCategories.forEach((item, i) => {
      if (item.isShow) {
        if (isFirst) {
          selected = selected + `${item.name}`
          isFirst = false
        } else {
          selected = selected + `,${item.name}`
        }
      }
    });
    console.log('this.state.selectedCategories::selected', selected)
    try {
      const res = await Service.get(Url.ProductSearch_URL + `storeId=${this.state.storeId}&name=${this.state.search}&subCategories=${selected}`, "")
      console.log('searchProduct', res)
      if (res.data) {
        this.setState({
          products: res.data,
          isLoading: false
        })
      }
      else {
        this.setState({ isLoading: false })
        Alert.alert('', 'Something Went Wrong')
      }
    }
    catch (err) {
      this.setState({ isLoading: false })
      Alert.alert('', err.message)
    }
  }

  getProductByCategories = async (selectedCategory) => {
    // this.setState({
    //   isLoading: true
    // })
    console.log('this.state.selectedCategories::value', selectedCategory)
    try {
      const res = await Service.get(Url.GET_Product_BySubCategories_UserGroup_URL + `pageNo=${1}&pageSize=${20}&storeId=${this.state.storeId}&subCategories=${selectedCategory.name}&customerGroup=${global.user.customerGroup}`, "")
      console.log('getProductByCategories', res)
      if (res.items) {
        this.setState({
          products: res.items,
          isLoading: false
        })
      }
      else {
        this.setState({ isLoading: false })
        Alert.alert('', 'Something Went Wrong')
      }
    }
    catch (err) {
      this.setState({ isLoading: false })
      Alert.alert('', err.message)
    }
  }

  isValidPDF(pdfUrl) {
    console.log('url to be evaluated :: ', pdfUrl);
    pdfUrl = pdfUrl.substr(1 + pdfUrl.lastIndexOf("/"));
    pdfUrl = pdfUrl.split('?')[0];
    pdfUrl = pdfUrl.split('#')[0];
    pdfUrl = pdfUrl.split('.').pop();

    console.log('pdf extension :: ', pdfUrl);
    return pdfUrl.trim() === 'pdf';
  }

  render() {
    return (
      <Container>
        <MainHeader navigate={this.props.navigation} />
        <ScrollView>
          <SubHeader title='Products' />
          <Loader isLoading={this.state.isLoading} />
          <View style={styles.mainHeadingView}>
            <Card
              style={styles.subHeadingCard}>
              <View style={styles.subHeadingView}>
                <Image
                  source={require('../../assets/lee.png')}
                  style={styles.icon}
                  resizeMode='center'
                />
              </View>
            </Card>
          </View>
          <View style={styles.mainView}>
            <View
              style={styles.contactMainView}>
              <View style={styles.contactView}>
                <TouchableOpacity onPress={() => this.openDailer()}>
                  <Image
                    source={require('../../assets/call.png')}
                    style={styles.callImage}
                  />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.openMessages()}>
                  <Image
                    source={require('../../assets/message.png')}
                    style={styles.callImage}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.addressView}>
                <Text
                  allowFontScaling={false}
                  style={styles.addressText}>
                  SHIP-TO:
                </Text>
                <Text
                  allowFontScaling={false}
                  style={styles.shipToText}>
                  {this.state.shipTo}
                </Text>
              </View>
            </View>
            <Text
              allowFontScaling={false} style={styles.name}> {this.state.name}</Text>
            <View
              style={styles.editCategoryView}>
              {this.state.productCategories.map((value, key) => {
                return (value.isShow ?
                  <TouchableOpacity
                    onPress={() => {
                      this.getProductByCategories(value);
                    }}
                    style={styles.Btn}>
                    <Text
                      allowFontScaling={false}
                      style={styles.categoryList}>{value.name}</Text>
                  </TouchableOpacity>
                  : null)
              })}
            </View>

            <View style={styles.viewCatalogueView}>
              <View style={styles.viewCatalogueSubView}>
                <TouchableOpacity
                  onPress={() => {
                    this.getProducts('viewentirecatalogue');
                  }}
                >
                  <Text
                    allowFontScaling={false} style={styles.editCategoryText}>
                    view entire catalogue
                </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.viewCatalogueSubView}>
                <TouchableOpacity
                  onPress={() => {
                    this.setModalVisible(true, this.state.productCategories);
                  }}
                >
                  <Text
                    allowFontScaling={false} style={styles.editCategoryText}>
                    EDIT CATEGORIES
                </Text>
                </TouchableOpacity>
              </View>
            </View>
            <Card>
              <View style={styles.searchView} >
                <TextInput
                  style={styles.searchTextInput}
                  placeholder="search"
                  placeholderTextColor="grey"
                  onChangeText={(search) => this.setState({ search })}
                // style={styles.loginTxtInput}
                />
                <TouchableOpacity onPress={() => this.searchProduct()}>
                  <Image
                    source={require('../../assets/search.png')}
                    style={styles.searchIcon}
                  />
                </TouchableOpacity>
              </View>
            </Card>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.getProducts('viewall');
                }}
              >
                <Text
                  allowFontScaling={false} style={styles.editCategoryText}>
                  VIEW ALL
                </Text>
              </TouchableOpacity>
            </View>
            <Modal
              style={{ height: 200 }}
              animationType="slide"
              transparent={true}
              backdropColor={'black'}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                this.setModalVisible(false, this.state.productCategories);
              }}
              onBackdropPress={() => console.log('Modal has been closed.Modal has been closed.Modal has been closed.Modal has been closed.')}
            >

              <TouchableOpacity
                style={styles.container}
                activeOpacity={1}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>

                <View style={styles.categoryModalView}>
                  <Text
                    allowFontScaling={false}
                    style={styles.categoryHeadingText}>
                    SELECT CATEGORY
                  </Text>

                  {this.state.productCategories.map((item, key) => {
                    return <ListItem style={{ height: 30, marginLeft: 0, backgroundColor: key % 2 === 0 ? '#cacaca' : '#f0f0f0' }}>
                      <CheckBox style={styles.checkBox} checked={item.isShow}
                        onPress={() => this.onCheckBoxPress(item, key)} />
                      <Body>
                        <Text
                          allowFontScaling={false} style={styles.category}>
                          {item.name}
                        </Text>
                      </Body>
                    </ListItem>
                  })}

                  {/* categories not found */}

                  {!this.state.productCategories.length ? <View style={styles.submitCategoryBtn}>
                    <Text
                      allowFontScaling={false}
                      style={styles.empty}> No categories found for selected customer type.</Text>
                  </View> : null
                  }
                  {/* end */}

                  {this.state.productCategories.length ? <View
                    style={styles.submitCategoryBtn}>
                    <TouchableOpacity style={styles.ModalBtn} onPress={() => {
                      this.setModalVisible(false, this.state.productCategories);
                    }}>
                      <Text allowFontScaling={false} style={styles.ModalBtnTxt}>SUBMIT CATEGORIES</Text>
                    </TouchableOpacity>
                  </View> : null}
                </View>
              </TouchableOpacity>
            </Modal>
            {this.state.products.map((item, key) => (
              <Card>

                <TouchableOpacity
                  style={styles.productList}
                  onPress={() => this.getProductDetial(item)}>

                  <View style={styles.productImgView}>
                    <Image
                      source={{ uri: item.image }}
                      resizeMode='cover'
                      style={styles.productImg}
                    />
                  </View>

                  <View style={styles.productDetails}>
                    <Text
                      allowFontScaling={false} style={[styles.productData, {
                        fontWeight: 'bold',
                      }]}>
                      {item.name}
                    </Text>
                    <Text
                      allowFontScaling={false} style={styles.productData}>
                      {item.category.name}
                    </Text>
                    <Text
                      allowFontScaling={false} style={styles.productData}>
                      ${item.costPerEach}
                    </Text>
                  </View>
                  {(item.pdf && this.isValidPDF(item.pdf)) ?
                    <View style={{ justifyContent: 'center', paddingRight: '2%' }}>
                      <TouchableOpacity onPress={() => this.goToPdfViewer(item.pdf)} >
                        <Text
                          allowFontScaling={false} style={styles.viewdetail}>
                          VIEW DETAIL
                      </Text>
                      </TouchableOpacity>
                    </View> : null
                  }
                </TouchableOpacity>



              </Card>
            ))}
          </View>
        </ScrollView>
      </Container>
    );
  }
}
