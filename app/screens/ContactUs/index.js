import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert,
} from 'react-native';
import {
    Container,
    Card,
} from 'native-base';
import SubHeader from '../../Components/SubHeader'
import MainHeader from '../../Components/MainHeader'
import Loader from '../../Components/Loader'

export default class ContactUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

        };
    }

    render() {
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <SubHeader title='Contact Us' />
                    <Loader isLoading={this.state.isLoading} />
                </ScrollView>
            </Container >
        );
    }
}
