import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../constants/colors';
import { heightPercentageToDP, widthPercentageToDP } from '../../utility';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  selectRole: {
    fontWeight: '500',
    fontSize: 20,
    marginBottom: '3%',
    color: "#fff"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  logo: {
    width: 200,
    height: 180,
  },
  formView: { paddingLeft: 50, paddingRight: 50, marginTop: '5%' },
  radioBtnLabelStyle: {
    ...Platform.select({
      ios: {
        fontSize: widthPercentageToDP('3.5%'),
        marginRight: widthPercentageToDP('8%'),

      },
      android: {
        fontSize: widthPercentageToDP('4%'),
        marginRight: widthPercentageToDP('7%'),
      }

    }),
  },
  logoContainer: {
    alignItems: 'center',
    marginTop: heightPercentageToDP('4%'),
  },
  loginTxtInput: {
    // backgroundColor: 'transparent',
    color: 'white',
    flex: 1,
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: heightPercentageToDP("3%"),
    // backgroundColor:'red'
  },
  ImageStyle: {
    padding: 5,
    // margin: 5,
    color: '#fff',
  },
  txtInput: {
    ...Platform.select({
      ios: {
        fontSize: widthPercentageToDP('3.5%'),

      },
      android: {
        fontSize: widthPercentageToDP('4%'),

      }

    }),
    color: 'white',

  },
  txtInputView: {
    flex: 1,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
  },
  LoginBtn: {
    height: 45,
    backgroundColor: '#fff',
    marginTop: 10,
    borderRadius: 30,
    // borderRadius: 8,
    borderColor: colors.primaryColor,
    // borderWidth: 1,
    elevation: 10,
    // marginLeft: 50,
    // marginRight: 50,
  },
  AJ: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  LoginBtnTxt: {
    color: colors.primaryColor,
    fontSize: widthPercentageToDP('4%'),
    textAlign: 'center',
    fontWeight: 'bold',
  },
  LoginBtn2: {
    height: 50,
    backgroundColor: colors.primaryColor,
    borderRadius: 30,
    elevation: 10,
  },
  linkedAccText: {
    color: '#fff',
    fontSize: widthPercentageToDP('4%'),
  },
  linkedText: {
    color: '#fff',
    fontSize: widthPercentageToDP('4%'),
    textDecorationLine: 'underline',
  },
  LoginBtnTxt2: {
    color: 'white',
    fontSize: widthPercentageToDP('3.5%'),
    textAlign: 'center',
    fontWeight: 'bold',
  },
  linkView: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  bottomView: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: heightPercentageToDP('2%'),
    flexDirection: 'column'
  },
  tncView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 22,
    height: 22,
    marginRight: 10,
    marginTop: 5,
  },
  orTxt: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10
  }
});

export default styles;
