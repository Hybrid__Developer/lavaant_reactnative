import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import {
  Container,
  Card,
} from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import styles from './style';
import SubHeader from '../../Components/SubHeader'
import MainHeader from '../../Components/MainHeader'
import * as Service from '../../api/services';
import * as Url from '../../constants/urls';
import * as commanFn from '../../utility/index';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../../Components/Loader'
export default class Vendor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vendor: [
        // {
        //   url:
        //     'https://casaplex.com/wp-content/uploads/2013/08/jbl-logo-wide.jpg',
        // },
        // {
        //   url:
        //     'https://casaplex.com/wp-content/uploads/2013/08/jbl-logo-wide.jpg',
        // },
        // {
        //   url:
        //     'https://casaplex.com/wp-content/uploads/2013/08/jbl-logo-wide.jpg',
        // },
        // {
        //   source: '../../assets/lee.png'
        // }
      ],
      vendorId: '',
      vendorName: '',
      isLoading: false,
      // token: '',
    };
  }
  componentWillMount() {
    //TOdo form handling two view
    //check vendor list lenght
    // then break it into equal lenght list
    // make diff arry according to lenght list
  }

  componentDidMount() {
    this.getVendors();
  }

  getAddress = async(value) => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'ChooseAddress' })]
      })
    );
    // navigation.navigate('ScreenTwo')
    await AsyncStorage.setItem('storeId', value.id);
    this.props.navigation.navigate('ChooseAddress', { logo: value.logo });
  };

  getVendors = async () => {
    console.log('inside get vendor')

    this.setState({
      isLoading: true
    })

    try {
      let token = await commanFn.getFromLocalStorge('token')
      console.log('token :: ', token);
      const res = await Service.get(Url.GET_VENDOR_URL, token)
      console.log('getVendor..........%%%%%%', res)
      if (res.data) {
        this.setState({
          vendor: res.data,
          isLoading: false
        })
        AsyncStorage.setItem('storeID', '5ed0adfa1016710bb8801f23');
      }
      else {
        this.setState({ isLoading: false })
        Alert.alert('', 'Something Went Wrong')
      }
    } catch (err) {
      this.setState({ isLoading: false })
      Alert.alert('', err.message)
    }
  }
  render() {
    return (
      <Container>
        <MainHeader navigate={this.props.navigation} />
        <ScrollView>
          <SubHeader
            title='Select Vendor'
            navigate={this.props.navigation}
            navigateTo="AddVendor" />
          <View
            style={styles.cardView}>
            <View
              style={styles.list}>
              {/* <View style={{
              flexDirection: 'row',
              padding: 10
            }}>
              <Card
                style={{
                  width: 160,
                  height: 120,
                  top: -40,
                }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', top: 15 }}>
                  <TouchableOpacity onPress={() => this.getAddress()}>
                    <Image
                      source={require('../../assets/lee.png')}
                      style={styles.icon}
                      resizeMode='center'
                    />
                  </TouchableOpacity>
                </View>
              </Card>
            </View>
            <View style={{
              flexDirection: 'row',
              padding: 10
            }}>
              <Card
                style={styles.cardStyle}>
                <View style={{ justifyContent: 'center', alignItems: 'center', }}> */}
              {/* <TouchableOpacity onPress={() => this.getAddress()}> */}
              {/* <Text style={{ textAlign: 'center', alignSelf: 'center' }}> Coming Soon</Text> */}
              {/* <Image
                      source={require('../../assets/lee.png')}
                      resizeMode='center'
                      style={styles.icon}
                    /> */}
              {/* </TouchableOpacity> */}
              {/* </View>
              </Card>
            </View> */}

              {this.state.vendor.map((value, key) => {
                return (
                  <View style={styles.cardList}>
                    <Card
                      style={styles.cardStyle}>
                      <TouchableOpacity onPress={() => this.getAddress(value)}>
                        {value.logo ? <Image
                          source={{
                            uri: value.logo,
                          }}
                          style={styles.icon}
                        /> : <Image
                            source={require('../../assets/userGrey.png')}
                            resizeMode='center'
                            style={styles.icon}
                          />}
                      </TouchableOpacity>
                    </Card>
                  </View>

                );
              })}
              <View style={styles.cardList}>
                <Card
                  style={styles.cardStyle}>
                  {/* <TouchableOpacity 
                    onPress={() => this.getAddress()}
                    > */}
                  <Text style={{ textAlign: 'center', alignSelf: 'center', fontSize: 16, fontWeight: 'bold' }}> Coming Soon</Text>
                  {/* <Image
                          source={require('../../assets/lee.png')}
                          resizeMode='center'
                          style={styles.icon}
                        /> */}
                  {/* <Image
                    source={require('../../assets/vendor_image.png')}
                    resizeMode='center'
                    style={styles.icon}
                  /> */}
                  {/* </TouchableOpacity> */}
                </Card>
              </View>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
