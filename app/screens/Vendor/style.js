import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
const styles = StyleSheet.create({
  cardView:{ marginLeft: '4%', marginRight: '4%' },
  cardList:{
    flexDirection: 'row',
    padding: '3%'
  },
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center', alignItems: 'center',
    // padding: 10
  },
  item: {
    width: 150,
    ...Platform.select({
      ios: {
        height: hp('13.4%'),
      },
      android: {
        height: hp('17.4%'),

      }
    }),
    top: -40,
  },
  cardStyle: {
    width: wp('37%'),
    ...Platform.select({
      ios: {
        height: hp('13.4%'),
      },
      android: {
        height: hp('17.4%'),

      }
    }),
    top: -40,
    justifyContent: 'center', alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      width: 0, height: 2
    },
  },
  input: {
    // elevation: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    width: 90,
    height: 85,
  },
  txtInput: {
    color: '#000',
    flex: 1,
  },
  BtnTxt: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  Selectvendor: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center', alignItems: 'center'
  },
  mainview: {
    flexDirection: 'row',
    padding: 10
  },
  card: {

    width: 150,
    height: 120,
    top: -40,
  },
  Btn: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    backgroundColor: colors.primaryColor,
    marginTop: 20,
    borderRadius: 30,
    marginBottom: 20,
    // borderColor: colors.primaryColor,
    elevation: 7,
  },

  drawerHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    marginBottom: 25,
    // backgroundColor: 'pink'
  },
});
export default styles;
