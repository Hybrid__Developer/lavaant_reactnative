import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'
import * as commanFn from '../../../../utility/index';
import Loader from '../../../../Components/Loader';
import * as Url from '../../../../constants/urls';
import * as Service from '../../../../api/services';

import { ScrollView } from 'react-native-gesture-handler';

export default class OrderModule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 'current',
            // tasks: this.PastOrder,
            orders: [],
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
        }

    }
    // changeColor = (value) => {
    //     this.setState({
    //         selected: value

    //     })
    // }

    goToOrderDetails = (item) => {
        this.props.navigation.navigate('AcceptNewOrder', { item: item })
    }

    componentDidMount() {
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            let activeTab = this.props.navigation.state.params ? this.props.navigation.state.params.activeTab : 'current';
            this.getOrders(activeTab);
            this.setState({
                selected: activeTab
            });
        });
    }
    componentWillUnmount() {
        this.focusListener.remove();
    }
    getOrders = async (status) => {
        console.log('inside orders list')
        this.setState({
            isLoading: true,
            selected: status
        })
        let token = await commanFn.getFromLocalStorge('token')
        console.log('token :: ', token);
        const res = await Service.get(`${Url.GET_ORDERS_BY_STATUS_URL}status=${status}`, token)
        console.log('response of ordersList :: ::', res);
        if (res.data) {
            this.setState({
                orders: res.data,
                isLoading: false
            })


        }
    }



    render() {
        return (
            <View>
                <MainHeader navigate={this.props.navigation} />
                <Loader isLoading={this.state.isLoading} />
                <ScrollView>

                    <SubHeader title="ORDERS" />
                    <View style={styles.changeColor}>



                        <TouchableOpacity onPress={() => this.getOrders('past')}>
                            <Text allowFontScaling={false} style={this.state.selected === 'past' ? styles.regionalTxt : styles.Font5}>Past Order</Text>
                            <View style={this.state.selected === 'past' ? styles.borderColor1 : null}>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.getOrders('current')}>
                            <Text allowFontScaling={false} style={this.state.selected === 'current' ? styles.regionalTxt : styles.Font6}>Current Order</Text>
                            <View style={this.state.selected === 'current' ? styles.borderColor1 : null}></View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.getOrders('new')}>
                            <Text allowFontScaling={false} style={this.state.selected === 'new' ? styles.regionalTxt : styles.Font6}>New Order</Text>
                            <View style={this.state.selected === 'new' ? styles.borderColor1 : null}></View>
                        </TouchableOpacity>

                    </View>

                    <View>
                        {

                            this.state.orders.map((item, key) => (
                                <View style={styles.headerview}>
                                    <View style={styles.subview}>

                                        <Image
                                            style={styles.userImg}
                                            resizeMode='cover'
                                            source={{ uri: item.user.profilePic ? item.user.profilePic : this.state.pic }}

                                        />
                                        <Image
                                            style={styles.userImgBackgroung}
                                            resizeMode='cover'
                                            source={require('../../../../assets/image_view1.png')}
                                        />
                                    </View>

                                    <TouchableOpacity style={styles.subview1} onPress={() => this.goToOrderDetails(item)}>

                                        <Text>{item.orderNo}</Text>
                                        {this.state.selected === 'current' ? <Text style={styles.textTransform}>Status: {item.status}</Text> : null}
                                    </TouchableOpacity>


                                </View>

                            ))}
                    </View>




                </ScrollView>
            </View>
        )
    }

}


