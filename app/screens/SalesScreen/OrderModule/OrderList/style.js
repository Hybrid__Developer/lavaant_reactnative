import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');

const styles = StyleSheet.create({
    textTransform:{
        textTransform:'capitalize',
    },
    changeColor: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    userImg: {
        left: 2,
        width: 50,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 100,
        marginLeft: 7
    },
    userImgBackgroung: {
        width: 54,
        height: 54,
        position: 'absolute',
        marginLeft: 7

    },
    regionalTxt: {
        color: "black",
        fontSize: 20,

    },
    Font5: {
        fontSize: 16,
        marginTop: 5,
        color: "grey",
        textTransform: 'capitalize',

    },
    Font6: {
        fontSize: 16,
        marginTop: 5,
        color: "grey",
        textTransform: 'capitalize',

    },
    borderColor1: {
        borderColor: "black",
        borderBottomWidth: 4,
        opacity: 100,
        marginTop: 10,
        borderRadius: 8
    },
    headerview: {
        flexDirection: 'row',
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    subview: {
        flexDirection: 'column',
        width: 70,
        justifyContent: 'center',
        borderRadius: 1,
        elevation: 1,
        borderRightWidth: 1,
        borderRightColor: 'black'
    },
    subview1: {
        width: 300,
        padding: 20,
        bottom: 4,
    },
});

export default styles;
