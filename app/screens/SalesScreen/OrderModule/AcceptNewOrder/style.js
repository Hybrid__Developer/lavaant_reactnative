import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({
    orderHeader: {
        width: '100%',
        backgroundColor: '#199bf1',
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    inputStyle: {
        marginTop:'10%',
        borderWidth: 1,
        flexDirection: 'row',
        height: 30,
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#199bf1',
    },
    orderHeaderTxt: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white',
        fontWeight: 'bold',
        padding: 14
    },
    mainView: { padding: 20 },


    orderDetailView: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    width: { width: '49%', },
    shipToTxt: {
        fontSize: 19,
        fontWeight: 'bold'
    },
    margin: {
        marginTop: 4,
        marginBottom: 20
    },
    nameTxt: {
        fontSize: 19,
        fontWeight: 'bold'
    },
    adressTxt: { fontSize: 14, },
    mainCardView: {
        flexDirection: 'row', borderWidth: 1,
    },
    subCardView: {
        flex: 2,
        flexDirection: 'column',
        backgroundColor: 'pink'
    },
    mainHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        height: 30
    },
    productView: {
        width: '60%',
        justifyContent: 'center',
        paddingLeft: '2%'
    },
    quantityView: {
        justifyContent: 'center',
        borderLeftWidth: 1,
        borderLeftColor: 'gray',
        width: '40%',
        alignItems: 'center'
    },
    flexDirection: { flexDirection: 'row' },
    ProductView: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    ProductTxt: {
        textAlign: 'left',
        fontWeight: 'bold',
        paddingTop: '3%',
        paddingLeft: '2%',
        fontSize: 15
    },
    // quantityView: {
    //     flex: 1,
    //     flexDirection: 'column',
    //     borderLeftWidth: 1
    // },
    quantityTxt: {
        textAlign: 'center',
        fontWeight: 'bold',
        paddingTop: '3%',
        paddingLeft: '2%',
        fontSize: 15
    },
    ProductView2: {
        flex: 2,
        flexDirection: 'column',
        backgroundColor: 'grey'
    },
    ProductTxt2: {
        textAlign: 'left',
        paddingTop: '3%',
        paddingLeft: '2%',
        paddingTop: '3%',
        paddingLeft: '2%',
        fontSize: 15
    },
    // quantityView2: {
    //     flex: 1,
    //     flexDirection: 'column',
    //     backgroundColor: 'grey',
    //     borderLeftWidth: 1
    // },
    quantityTxt2: {
        textAlign: 'center',
        paddingTop: '3%',
        paddingLeft: '2%',
        fontSize: 15
    },
    acceptButtonView: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: '#199bf1',
        padding: 7,
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 24,
        borderRadius: 10,
        justifyContent: 'center'
    },
    acceptImg: {
        height: 20,
        width: 20,
        marginRight: 10
    },
    acceptTxt: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },
    rejectView: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'red',
        padding: 7,
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 24,
        borderRadius: 10,
        justifyContent: 'center'
    },




});


export default styles;

