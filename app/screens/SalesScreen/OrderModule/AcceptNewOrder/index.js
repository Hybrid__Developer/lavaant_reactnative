
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Alert, } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import {
    Container,
    Picker, Icon
} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import styles from './style'
import Loader from '../../../../Components/Loader';
import * as Url from '../../../../constants/urls';
import * as Utility from '../../../../utility/index';
import * as Service from '../../../../api/services';
export default class AcceptNewOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: '',
            selected2: 'show',
            statusList: [
                'packed', 'ready to ship', 'onhold', 'delivered'
            ],
            orderDetails: {},
            lastProductIndex: this.props.navigation.state.params.item.products.length - 1,
            orderDetails: this.props.navigation.state.params.item
        };
    }

    componentDidMount() {
        let statusList = this.state.statusList
        console.log('oreder details....', this.state.orderDetails.status)
        if (this.state.orderDetails.status === 'packed') {
            statusList = ['ready to ship', 'onhold', 'delivered']
        } else if (this.state.orderDetails.status === 'ready to ship') {
            statusList = ['onhold', 'delivered']
        } else if (this.state.orderDetails.status === 'onhold') {
            statusList = ['delivered']
        }
        this.setState({
            statusList: statusList
        })
        this.setState({
            displayButtons: this.state.orderDetails.status === 'waiting for approval'
            // orderDetails: orderDetails,
            // lastProductIndex: orderDetails.products.length - 1
        })
    }


    openRejectAlert() {
        Alert.alert(
            'Warning',
            'Are you sure you want to reject this order ?',
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: 'Yes', onPress: () => this.updateOrderStatus('rejected', 'new') },
            ],
            { cancelable: false },
        );

    }


    updateOrderStatus = async (status, activeTab) => {
        console.log('inside order status ........................', status)
        let token = await Utility.getFromLocalStorge('token');
        console.log('inside order status token........................', token)
        const res = await Service.put(`${Url.ORDER_UPDATE_STATUS_URL}${this.state.orderDetails.id}?status=${status}`, token)
        console.log("update order status", res)
        if (res.data) {
            this.props.navigation.navigate('OrderList', { activeTab: activeTab })
        }
    }

    render() {
        const lastRowIndex = this.state.lastProductIndex;
        const shipTo = this.state.orderDetails.shipTo;
        return (
            <Container>

                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <SubHeader title="PRODUECTS ORDER 1" />
                    <View style={styles.mainView}>
                        <View style={styles.orderDetailView}>
                            <View style={styles.width}>
                                <Text style={styles.shipToTxt}>SHIP-TO:</Text>
                                <View style={styles.margin}>
                                    <Text style={styles.nameTxt}>{`${shipTo.contactName}`}</Text>
                                    <Text style={styles.adressTxt}>{`${shipTo.address}, ${shipTo.area}`}</Text>
                                    <Text style={styles.adressTxt}>{`${shipTo.city}`}</Text>
                                    <Text style={styles.adressTxt}>{`${shipTo.state}`}</Text>
                                    <Text style={styles.adressTxt}>{`${shipTo.zipCode}`}</Text>
                                </View>
                            </View>
                            <View style={styles.width}>
                                {!this.state.displayButtons ? <View style={[styles.inputStyle]}>
                                    <Picker
                                        iosIcon={<Icon name="ios-arrow-down" />}
                                        mode='dropdown'
                                        selectedValue={this.state.status}
                                        onValueChange={(itemValue) => {
                                            if (itemValue != "0") {
                                                this.setState({ status: itemValue })
                                                this.updateOrderStatus(itemValue, 'current')
                                            }
                                        }}>
                                        <Picker.Item enabled={false} label="Update Status" value="0" />
                                        {this.state.statusList.map((item, key) =>
                                            <Picker.Item key={key} label={item} value={item} />
                                        )}
                                    </Picker>
                                </View> : null}
                            </View>

                        </View>



                        <View style={{ borderRadius: 5, borderColor: 'gray', borderWidth: 2 }}>
                            <View style={styles.mainHeading}>
                                <View style={styles.productView}>
                                    <Text style={{ fontWeight: 'bold' }}>Product</Text>
                                </View>
                                <View style={styles.quantityView}>
                                    <Text style={{ fontWeight: 'bold' }}>Quantity</Text>
                                </View>
                            </View>
                            {/* Loop start */}
                            {this.state.orderDetails.products.map((item, key) => (
                                <View style={{
                                    borderBottomLeftRadius: (lastRowIndex === key) ? 3 : 0, borderBottomRightRadius: (lastRowIndex === key) ? 3 : 0,
                                    flexDirection: 'row', justifyContent: 'space-between', height: 30, backgroundColor: key % 2 === 0 ? '#cacaca' : '#f0f0f0'
                                }}>
                                    <View style={styles.productView}>
                                        <Text>{item.name}</Text>
                                    </View>
                                    <View style={styles.quantityView}>
                                        <Text>{item.quantity}</Text>
                                    </View>
                                </View>
                            ))}
                            {/* loop ends */}

                        </View>

                        {this.state.displayButtons ?
                            <View>
                                <TouchableOpacity onPress={() => this.updateOrderStatus('approved', 'new')}>
                                    <View style={styles.acceptButtonView}>
                                        <Image source={require('../../../../assets/accept.png')} style={styles.acceptImg}></Image>
                                        <Text style={styles.acceptTxt}>ACCEPT</Text>

                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.openRejectAlert()}>
                                    <View style={styles.rejectView}>
                                        <Image source={require('../../../../assets/reject.png')} style={styles.acceptImg}></Image>
                                        <Text style={styles.acceptTxt}>REJECT</Text>

                                    </View>
                                </TouchableOpacity>
                            </View> : null}

                    </View>
                </ScrollView>
            </Container >

        );
    }
}
