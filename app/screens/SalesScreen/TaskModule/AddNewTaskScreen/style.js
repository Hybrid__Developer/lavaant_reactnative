import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    subheader: {
        flex: 1,
        flexDirection: 'column',
        padding: 1,


    },
    mainview: {
        flex: 1,
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: "space-around",

    },
    mainview1: {
        flex: 1,
        margin: 5,
        justifyContent: "center",
        alignSelf: 'center',
        width: 320,
        height: 49,
        borderColor: "white",
        elevation: 2,
        borderWidth: 2

    },
    card: {

        // width: 320,
        height: 220,

    },
    card1: {
        width: 320,
        height: 60,

    },
    subView: {
        flexDirection: "row", marginTop: '5%',
        justifyContent: "space-between",
    },
    checkview: {
        marginTop: 14,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    datePickerView: {
        flexDirection: 'row',
        // backgroundColor: "white",
        // width: 320,
        // alignSelf: 'center',
        // borderWidth: 1,
        // borderColor: 'white',
        marginTop: 10,
        justifyContent: 'space-between',
        // elevation: 4,
        // paddingLeft: 4,
        // paddingRight: 10
    },
    dateTxt: {
        color: "grey",
        fontSize: 24,
        bottom: 7,
    },
    placeHolderText: {
        color: "grey",
        fontSize: 20,
        textAlign: 'center',
        bottom: 4.2
    },
    datePickerView2: {
        backgroundColor: "white",
        width: 320,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: 'white',
        marginBottom: 24,
        marginTop: 10,
        elevation: 4,
    },
    datePickerView3: {
        flexDirection: 'row',
        backgroundColor: "white",
        width: 320,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: 'white',
        marginTop: 10,
        justifyContent: 'space-between',
        paddingLeft: '2%',
        paddingRight: 10
    },
    dropDownImg: {
        height: 17,
        width: 24,
        marginTop: 10
    },
    dropDownImg2: {
        height: 17,
        width: 24,
        marginTop: 14
    },
    addCategory: {
        height: 21,
        width: 24,

    },
    addCategoryPlaceHolder: {
        fontSize: 20,
        color: 'black'
    },
    chooseCatogryView: {
        paddingLeft: '4%',
        height: 220,
        marginBottom: 10
    },
    catogoryListView: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        paddingTop: 14,
        paddingBottom: 14,
        marginRight: '4%'
    },
    addCategoryView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopColor: 'grey',
        borderTopWidth: 1,
        paddingTop: 10,
        marginRight: '4%'
    },
    dataTillRemindingTxt: {
        textAlign: 'center',
        fontSize: 27,
        color: 'black'
    },
    checkBoxView: {
        padding: 10,
        width: 90,
        paddingTop: '2%',
        paddingLeft: '2%',
        // borderColor: 'black',
        // backgroundColor: 'red',
        // borderWidth: 1,
        // padding: '4%'
    },
    flexDirectionView: { flexDirection: 'row' },
    tickView: {
        height: 22,
        width: 20,
        elevation: 1,
        borderColor: '#199bf1',
        borderWidth: 1,
        // backgroundColor: 'orange'
    },
    imgAcceptView: {
        height: 18,
        width: 18,
        backgroundColor: '#199bf1'
    },
    input: {
        width: 20,
        height: 30
    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: '#fff',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
    },

    LoginBtn2: {
        width: 200,
        height: 50,
        marginLeft: '25%',
        backgroundColor: colors.primaryColor,
        marginTop: 120,
        borderRadius: 4,
        elevation: 10,
        marginBottom: 20
    },

    LoginBtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    img: {
        height: 40,
        width: 40,
        marginLeft: '40%'
    },
    img2: {
        width: 10,
        height: 10
    },
    arrow: {
        width: 20,
        height: 20,
        margin: 20,

    }
});




export default styles;