import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    subHeaderView: {
        // paddingTop: '8%',
        // paddingLeft: '4%',
        // width: '100%',
        minHeight: 80,
        justifyContent: 'center',
        paddingLeft: '5%',
        backgroundColor: '#199bf1'
    },
    view: {
        flexDirection: 'row',
        height: 400,
        borderLeftWidth: 1
    },

    headerview: {
        flexDirection: 'row',
        justifyContent: "space-between",


    },
    tabView: {
        flexDirection: 'row',
        height: 40,
        // backgroundColor: 'red'
    },
    dateText:
    {
        fontSize: 24,
        color: 'white',
        fontWeight: "bold"
    },
    layoutView:
    {
        width: '90%',
        height: 40,
        flexDirection: 'row',
        // backgroundColor: 'pink'
    },
    tabRightImgView: {
        // flexDirection: "row",
        height: 40,
        width: '10%',
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },
    subview: {
        flexDirection: 'column',
        width: 90,
        justifyContent: 'center',
        borderColor: 'black',
        borderRadius: 1,
        elevation: 1,



    },
    subview1: {
        flexDirection: 'row',
        width: 390,
        padding: 20,
        borderColor: 'black',
        borderRadius: 1,
        elevation: 1,


    },
    subview2: {
        flexDirection: 'row',
        borderLeftWidth: 1,
    },
    subview3: {
        flexDirection: 'column',
        height: 50,
        width: '50%',
        borderColor: 'white',
        borderRadius: 1,
        elevation: 1,
        borderLeftWidth: 1
    },
    subview4: {
        flexDirection: 'column',
        height: 50,
        width: '50%',
        borderColor: 'white',
        borderRadius: 1,
        borderLeftWidth: 3,
        elevation: 1,
    },
    image: {
        height: 50,
        width: 50,
        borderRadius: 40,
    },
    image1: {
        height: 15,
        width: 15,
        // margin: 14,

    },
    image5: {
        height: 5,
        width: 5,
        marginTop: 23,
        right: 12

    },

    image2: {
        height: 15,
        width: 15,
        margin: 5,
        marginLeft: 10


    },
    scrollButton: {
        height: 10,
        width: 5,

    },
    scrollButtonView: {
        width: 20,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green'
    },
    catagoryView: {
        alignSelf: 'center', marginTop: 10

    },
    borderWidth: {
        borderTopColor: "black",
        borderTopWidth: 1
    },
    delete: {
        height: 15,
        width: 15,
        margin: 5,
        marginLeft: 14
    },
    add: {
        height: 17,
        width: 17,
    },
    textInModal: {
        borderBottomColor: "grey",
        // backgroundColor: 'red',
        paddingLeft: 10
    },
    checkBoxView: {
        marginLeft: -10,
        marginHorizontal: 10
    },
    dateTxt: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: "center"
    },
    monthTxt: {
        color: 'black',
        fontSize: 14,
        textAlign: "center"
    },
    modalMainView: {
        width: 150,
        backgroundColor: "white",
        margin: 180,
        // elevation: 100,
        padding: '2%',
        borderRadius: 6
    },

    modalSubView: {
        flexDirection: "row",
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        padding: '2%',
        marginBottom: 10,
    },

    // modalMainView2: {
    //     width: 150,
    //     backgroundColor: "white",
    //     // height: 180,
    //     margin: 180,
    //     elevation: 100,
    //     padding: '2%',
    //     // justifyContent: 'space-between'
    // },
    // modalSubView2: {
    //     flexDirection: "row",
    //     borderBottomWidth: 1,
    //     marginBottom: 10,
    //     padding: '2%'
    // },
    doubleTabTxt: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    addImgView: { justifyContent: 'center', alignSelf: 'center', },
    margin: { marginTop: 29 }
})

export default styles;