import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    subheader: {
        flex: 1,
        flexDirection: 'column',
        padding: 1,

    },
    mainview: {
        flex: 1,
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: "space-around",

    },

    add: {
        flexDirection: "row",
        width: '92%',
        marginTop: 23,
        borderColor: "grey",
        borderWidth: 1,
        padding: 10,
        justifyContent: "space-between",
        alignSelf: 'center',

    },
    catagoryMainView: {
        flexDirection: "row",
        borderBottomColor: "grey",
        borderBottomWidth: 1,
        padding: 15,
        justifyContent: 'space-between'

    },

    subView: {
        flexDirection: "row", marginTop: '5%',
        justifyContent: "space-between", backgroundColor: "white",
    },
    checkview: {
        flex: 1,
        marginTop: 5,
        flexDirection: 'row',

    },
    input: {
        width: 20,
        height: 30
    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: '#fff',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
    },

    LoginBtn2: {
        width: 200,
        height: 50,
        marginLeft: '25%',
        backgroundColor: colors.primaryColor,
        marginTop: 100,
        borderRadius: 4,
        elevation: 10,
        marginBottom: 20
    },

    LoginBtnTxt2: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    img: {
        height: 30,
        width: 70,
        marginTop: '13%'
    },
    img2: {
        width: 10,
        height: 10
    },
    scroll: {
        height: 24,
        width: 24,
        marginTop: '4%'

    },
    saveButton: {
        width: 50,
        height: 30,
        backgroundColor: colors.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
    },

});




export default styles;