import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput, Alert, FlatList } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style';
import * as Utility from '../../../../utility/index';
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import Loader from '../../../../Components/Loader'
import { NavigationActions, StackActions } from 'react-navigation';

import {


    Container,

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';


export default class CategoryScreen extends Component {
    constructor(props) {
        super(props);

        this.array = [
        ],



            this.state = {
                AddNewCategory: '',
                data: [],
                enable: false,
                Category: '',
                arrayHolder: [],
                AddNewCategory: '',
                isLoading: false,
                catogoryList: [],
                id: "",
                selectedCatogory: [],
                showSave: false,


            }
    }
    componentDidMount() {

        this.setState({ arrayHolder: [...this.array] })
        this.GetCategory()

    }

    profile = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CategoryScreen' })]
            })
        );
        this.props.navigation.navigate('CategoryScreen')
    }

    AddCategory = async () => {
        let token = await Utility.getFromLocalStorge('token')
        console.log("alert mesge :::::", token)
        if (Utility.isFieldEmpty(this.state.AddNewCategory
        )) {
            Alert.alert("", "Please enter Category")
            return
        }

        else {
            let body = {
                name: this.state.AddNewCategory,
                categoryOf: "task"
            }
            const res = await Service.post(Url.CREATE_TASKCATEGORIES_URL, token, body)
            console.log("alert mesge :::::", res.message)

            var responsee = res
            // if (res)
            //     var responsee = res
            // console.log("add category ::::response", responsee)

            // this.setState({
            //     isLoading: false,
            // })
            console.log("isSucess", responsee.isSuccess)

            if (res.message == "Category Added Successfully") {
                Alert.alert(
                    '',
                    responsee.message,
                    [


                        { text: 'OK', onPress: () => this.profile() },
                    ],
                    { cancelable: false },
                );
            }
            else {
                Alert.alert(responsee.message)
            }


        }

    }




    UpdateCategory = async () => {
        this.setState({
            showSave: false
        });
        let token = await Utility.getFromLocalStorge('token')
        // categoryId = await Utility.getFromLocalStorge('categoryId')
        // console.log("sahi;laery::", categoryId)
        // this.setState({ aaa: categoryId })
        let body = {
            name: this.state.AddNewCategory,
        }
        console.log("body", body)
        if (this.state.AddNewCategory == "") {
            this.props.navigation.navigate("AddNewTaskScreen")
        }
        const res = await Service.put(Url.PUT_TASKCATEGORIES_URL + this.state.selectedCatogory, token, body)
        console.log("aaaaaaaaa::::::::", res)
        var responsee = res

        if (res.message == "Category Updated Successfully") {
            Alert.alert(
                '',
                res.message,
                [


                    { text: 'OK', },
                ],
                { cancelable: false },
            );
        }
        else {
            this.props.navigation.navigate('AddNewTaskScreen')

            Alert.alert(res.message)
        }
        // if (res)
        //     var responsee = res
        // console.log("add category ::::response", responsee)
        // this.props.navigation.navigate('AddNewTaskScreen')
    }

    GetCategory = async () => {
        let token = await Utility.getFromLocalStorge('token')
        let userId = await Utility.getFromLocalStorge('userId')
        await this.setState({
            isLoading: true
        })
        const res = await Service.get(Url.GET_TASKCATEGORIES_URL + `categoryOf=task&id=${userId}`, token)
        await this.setState({
            isLoading: false
        })
        var responsee = res
        if (res.data)
            var responsee = res.data
        console.log("add category ::::response", responsee)

        this.setState({
            isLoading: false,
            catogoryList: responsee
        })
        console.log("add category ::::response", this.state.catogoryList.name)
        responsee.forEach(item => {
            Utility.setInLocalStorge('categoryId', item.id)
            this.setState({ id: item.id })

        })

    }
    selectCatogory = (value) => {
        this.setState({
            selectedCatogory: value,
            showSave: true
            // enable: !this.state.enable
        })
    }
    goToAllTask = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AddNewTaskScreen' })]
            })
        );
        this.props.navigation.navigate('AddNewTaskScreen')
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <SubHeader
                        title='ADD NEW CATEGORY'
                    />
                    {
                        this.state.catogoryList.map((item, key) => (

                            <View style={styles.catagoryMainView} >

                                <TextInput
                                    editable={this.state.selectedCatogory === item.id}
                                    onChangeText={(AddNewCategory) => this.setState({ AddNewCategory })}
                                    style={{ fontSize: 18, color: 'black' }}>{item.name}</TextInput>
                                <View style={{ flexDirection: 'row' }}>
                                    {this.state.showSave && (this.state.selectedCatogory === item.id) ? (<TouchableOpacity
                                        onPress={() => this.UpdateCategory()}>
                                        <View style={styles.saveButton}>
                                            <Text style={{ color: 'white' }}>Save</Text>
                                        </View>
                                    </TouchableOpacity>) :
                                        (<TouchableOpacity onPress={() => this.selectCatogory(item.id)}>
                                            <Image
                                                source={require('../../../../assets/pencil.png')}
                                                style={styles.scroll}
                                            />
                                        </TouchableOpacity>)}


                                </View>
                            </View>
                        ))
                    }

                    <View style={{ marginTop: 10 }}>
                        <View style={styles.add}>
                            <TextInput
                                style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }}
                                placeholder="Add New Category"
                                placeholderTextColor="black"
                                multiline={true}
                                onChangeText={(AddNewCategory) => this.setState({ AddNewCategory })}
                            />
                            <TouchableOpacity onPress={() => this.AddCategory()}>
                                <Image
                                    source={require('../../../../assets/button.png')}
                                    style={styles.img}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={[styles.LoginBtn2, styles.AJ]}
                        onPress={() => this.UpdateCategory()}>
                        <Text style={styles.LoginBtnTxt2}>Submit</Text>
                    </TouchableOpacity>

                </ScrollView>
            </Container>
        )
    }
}


