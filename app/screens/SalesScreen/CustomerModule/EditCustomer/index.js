import React, { Component } from 'react';
import {
    View, Text, TextInput, ScrollView, Image, TouchableOpacity, Alert, Button, TouchableHighlightBase
} from 'react-native';
import { Container, Picker } from 'native-base';
import styles from './style'
import MainHeader from '../../../../Components/MainHeader'
import DatePicker from 'react-native-datepicker'
import Loader from '../../../../Components/Loader';
import * as Utility from '../../../../utility/index';
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import RNPickerSelect from 'react-native-picker-select';
import { NavigationActions, StackActions } from 'react-navigation';
var date = new Date().getDate();
var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();
export default class EditCustomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.navigation.state.params.id,
            profilePic: {},
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
            firstName: this.props.navigation.state.params.firstName,
            lastName: this.props.navigation.state.params.lastName,
            mobileNo: this.props.navigation.state.params.mobileNo,
            dob: this.props.navigation.state.params.dob,
            query: '',
            country: '',
            anniversary: this.props.navigation.state.params.anniversary,
            companyName: this.props.navigation.state.params.companyName,
            companyAddress: '',
            companyPhone: this.props.navigation.state.params.companyPhone,
            isLoading: false,
            selectedGroup: '',
            customerGroups: [],
            email: '',
            alternateMobileNo: this.props.navigation.state.params.alternateMobileNo,
            address: this.props.navigation.state.params.address,
            user: '',
            selected: "",
            selectedGender: "",
            showHideGender: "",
            GenderStatus: false,
            textInput: [],
            inputData: [],
            interests: this.props.navigation.state.params.interests,
            hobbies: this.props.navigation.state.params.hobbies,
            companionName: this.props.navigation.state.params.companionName,
            JSON_ListView_Clicked_Item: [],
            ids: this.props.navigation.state.params.ids,
            editAdd: "add"


        };
        this.getRole()
    }
    getRole = async () => {
        let role = await Utility.getFromLocalStorge('role')
        this.setState({
            role: role
        })
        this.getUserGroups()
    }

    getUserGroups = async () => {
        let token = await Utility.getFromLocalStorge('token')
        console.log('user::profile::getUser', token)
        this.setState({
            isLoading: true
        })
        let res = await Service.get(Url.GET_UserGroups_URL, token)
        console.log('profile::getUserGroups', res.data)
        await this.setState({
            isLoading: false,
            customerGroups: res.data
        })
    }
    updateUser = (user) => {
        this.setState({ user: user })
    }
    showHideGender = (value) => {
        this.setState({
            selected2: value

        })
    }
    GenderStatus = (value) => {
        this.setState({
            GenderStatus: value,

        })
    }
    showHide = (value) => {
        this.setState({
            selected: value

        })
    }
    addNewUser = (value) => {
        this.setState({
            editAdd: value
        })
        Alert.alert("please fill all new kids field for update new kids ")
    }
    addTextInput = (index) => {
        let textInput = this.state.textInput;

        textInput.push(<TextInput style={styles.textInput}
            placeholderTextColor="black"
            placeholder="Enter kids name"
            onChangeText={(text) => this.addValues(text, index)} />);
        this.setState({ textInput });
    }

    //function to remove TextInput dynamically
    removeTextInput = () => {
        let textInput = this.state.textInput;
        let inputData = this.state.inputData;
        textInput.pop();
        inputData.pop();
        this.setState({ textInput, inputData });
    }

    //function to add text from TextInputs into single array
    addValues = (name, index) => {
        let dataArray = this.state.inputData;
        let checkBool = false;
        if (dataArray.length !== 0) {
            dataArray.forEach(element => {
                if (element.index === index) {
                    element.name = name;
                    checkBool = true;
                }
            });
        }
        if (checkBool) {
            this.setState({
                inputData: dataArray
            });
        }
        else {
            dataArray.push({ 'text': name, 'index': index });
            this.setState({
                inputData: dataArray
            });
        }
    }

    //function to console the output
    getValues = () => {
        console.log('Data', this.state.inputData);
    }
    select = () => {
        JSON_ListView_Clicked_Item.otherInformation.isMarried(true)
    }
    UpdateUser = async () => {
        let token = await Utility.getFromLocalStorge('token')
        if (this.state.firstName && this.state.lastName &&
            this.state.mobileNo && this.state.alternateMobileNo && this.state.companyPhone && this.state.interests
            && this.state.hobbies && this.state.dob
        ) {
            let body = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                mobileNo: this.state.mobileNo,
                alternateMobileNo: this.state.alternateMobileNo,
                address: this.state.address,

                dob: this.state.dob,
                // customerGroup: this.state.customerGroups,
                workDetails: {
                    companyName: this.state.companyName,
                    // companyAddress: this.state.companyAddress || '',
                    companyPhone: this.state.companyPhone || '',
                },
                otherInformation: {
                    isMarried: this.state.GenderStatus,
                    kids: this.state.inputData,
                    anniversary: this.state.anniversary,
                    interests: this.state.interests,
                    hobbies: this.state.hobbies

                },
                // companyPhone: this.companyPhone,
                hobbies: this.state.hobbies,
                companionName: this.state.companionName,
                anniversary: this.state.anniversary,
                kids: this.state.inputData


            }
            console.log("body::::::::", body)


            this.setState({
                isLoading: true
            })
            const res = await Service.put(Url.PUT_CUSTOMER_USER_URL + this.state.id, token, body)
            console.log("aaaaaaaaa::::::::", res)
            var responsee = res
            this.setState({
                isLoading: false
            })
            if (responsee.isSuccess == true) {
                console.log("response::::::", responsee.isSuccess)

                Alert.alert(
                    '',
                    "update sucesfully ",
                    [


                        { text: 'OK', onPress: () => this.goToCustomerList() },
                    ],
                    { cancelable: false },
                );
            }
        }
        else {

            Alert.alert("please enter all the field")

        }

    }
    goToCustomerList = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CustomerList' })]
            })
        );
        this.props.navigation.navigate('CustomerList')
    }

    render() {
        const { navigation } = this.props;
        const JSON_ListView_Clicked_Item = navigation.getParam('JSON_ListView_Clicked_Item');

        // const { query } = this.state;
        // console.log('query..................', query)
        // const data = this.findCustomer(query);
        // const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <View style={styles.subHeaderView}>
                        <Text style={styles.currentDateTxt}>{date + '-' + month + '-' + year}</Text>
                    </View>

                    <View>
                        <View style={styles.drawerHeader}>
                        </View>
                        <View style={styles.pickerView}>
                            <Text style={{
                                fontWeight: 'bold',
                                fontSize: 18,
                                marginLeft: '3%'
                            }}>Customer Type </Text>

                            <View style={styles.inputViewStyle}>
                                <Picker
                                    mode='dropdown'
                                    selectedValue={this.state.selectedGroup}
                                    itemTextStyle={{ textTransform: 'capitalize' }}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ selectedGroup: itemValue })
                                    }>
                                    {this.state.customerGroups.map((item, key) =>
                                        <Picker.Item key={key} label={item.name} value={item.id} />
                                    )}
                                </Picker>

                            </View>
                        </View>

                        <View>
                            <View style={styles.mainView}>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/userGrey.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="First Name"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(firstName) => this.setState({ firstName })}
                                            value={this.state.firstName}

                                            style={styles.txtInput}></TextInput>

                                    </View>

                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/userGrey.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="last Name"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            value={this.state.lastName}
                                            onChangeText={(lastName) => this.setState({ lastName })}

                                            style={styles.txtInput}></TextInput>

                                    </View>

                                </View>

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/email.png')}
                                        style={styles.email}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Email Address"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(email) => this.setState({ email })}

                                            style={styles.txtInput}>{JSON_ListView_Clicked_Item.email}</TextInput>

                                    </View>

                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/address.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Address"
                                            value={this.state.address}
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(address) => this.setState({ address })}
                                            style={styles.txtInput}
                                        />
                                    </View>

                                </View>

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/phone_number.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Phone Number"
                                            placeholderTextColor="#000"
                                            keyboardType="numeric"
                                            value={this.state.mobileNo}
                                            onChangeText={(mobileNo) => this.setState({ mobileNo })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/phone_number.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Alternate Number"
                                            keyboardType="numeric"
                                            placeholderTextColor="#000"
                                            value={this.state.alternateMobileNo}
                                            onChangeText={(alternateMobileNo) => this.setState({ alternateMobileNo })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/userGrey.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Company Name"
                                            placeholderTextColor="#000"
                                            value={this.state.companyName}
                                            onChangeText={(companyName) => this.setState({ companyName })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/phone_number.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Company Phone"
                                            placeholderTextColor="#000"
                                            keyboardType="numeric"
                                            value={this.state.companyPhone}
                                            onChangeText={(companyPhone) => this.setState({ companyPhone })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <View style={styles.txtHeaderBorder}>
                                {/* <Text style={styles.txtHeader}>Personal Details</Text> */}
                            </View>

                            <View style={styles.mainView}>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/interseted.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Interests"
                                            placeholderTextColor="#000"
                                            value={this.state.interests}
                                            onChangeText={(interests) => this.setState({ interests })}
                                            style={styles.txtInput}></TextInput>

                                    </View>
                                </View>


                                <View style={styles.input}>
                                    <Image

                                        source={require('../../../../assets/hobies.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Hobbies"
                                            // underlineColorAndroid="#000"
                                            value={this.state.hobbies}
                                            placeholderTextColor="#000"
                                            onChangeText={(hobbies) => this.setState({ hobbies })}
                                            style={styles.txtInput}></TextInput>

                                    </View>
                                </View>

                                <View style={styles.input}>
                                    <Image

                                        source={require('../../../../assets/married.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle2}>

                                        <TextInput
                                            editable={false}
                                            style={{ color: 'black' }}
                                            placeholderTextColor="black"
                                            placeholder="Married Status">{JSON_ListView_Clicked_Item.otherInformation.isMarried}</TextInput>

                                        {this.state.selected == 'Down' ?
                                            <TouchableOpacity onPress={() => this.showHide('Up')}>
                                                <Image source={require('../../../../assets/up.png')} style={styles.dropDownImg2}></Image>
                                            </TouchableOpacity> : <TouchableOpacity onPress={() => this.showHide('Down')}>
                                                <Image source={require('../../../../assets/left-arrow.png')} style={styles.dropDownImg2}></Image>
                                            </TouchableOpacity>}
                                    </View>



                                </View>
                                {this.state.selected == 'Down' ?
                                    <View style={styles.genderStatus}>
                                        <View style={{ flexDirection: 'column', }}>
                                            <Text style={{ marginBottom: 7 }}>Married</Text>
                                            <Text style={{ marginBottom: 7 }}>Unmarried</Text>

                                        </View>
                                        <View style={{ flexDirection: 'column', }}>
                                            <TouchableOpacity onPress={() => this.GenderStatus(true)}>
                                                <View style={styles.checkBox}>
                                                    <View>
                                                        {this.state.GenderStatus == true ?
                                                            <View style={styles.checkBoxTick}>

                                                            </View> : null}
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.GenderStatus(false)}>
                                                <View style={styles.checkBox}>
                                                    <View>


                                                        {this.state.GenderStatus == false ?
                                                            <View style={styles.checkBoxTick}>
                                                            </View> : null}

                                                    </View>
                                                </View>
                                            </TouchableOpacity>


                                        </View>
                                    </View> : null}
                                {this.state.GenderStatus == true ?
                                    <View style={styles.input}>
                                        <Image

                                            source={require('../../../../assets/userGrey.png')}
                                            style={styles.icon}
                                        />
                                        <View style={styles.inputViewStyle}>
                                            <TextInput
                                                require
                                                placeholder="Companion Name"
                                                // underlineColorAndroid="#000"
                                                placeholderTextColor="#000"
                                                value={this.state.companionName}
                                                onChangeText={(companionName) => this.setState({ companionName })}
                                                style={styles.txtInput}></TextInput>

                                        </View>
                                    </View> : null}
                                {this.state.GenderStatus == true ?
                                    <View>
                                        <View style={styles.input}>
                                            <Image

                                                source={require('../../../../assets/userGrey.png')}
                                                style={styles.icon}
                                            />
                                            <View style={styles.inputViewStyle2}>

                                                <TextInput
                                                    editable={false}
                                                    style={{ color: 'black' }}
                                                    placeholderTextColor="black"
                                                    placeholder="Kids Name"></TextInput>
                                                <TouchableOpacity onPress={() => { this.addTextInput(this.state.textInput.length); this.addNewUser("edit") }}>
                                                    {this.state.editAdd == "add" ?
                                                        <Image source={require('../../../../assets/pencil.png')} style={styles.editImg}></Image> : <Image source={require('../../../../assets/add.png')} style={styles.editImg}></Image>}
                                                </TouchableOpacity>

                                            </View>
                                        </View>
                                        {

                                            this.state.ids.map((item, key) => (
                                                <View>
                                                    <View style={{ marginLeft: '12%', marginTop: 10, borderBottomColor: 'black', borderBottomWidth: 1 }}>
                                                        <Text>{item.name}</Text>

                                                    </View>
                                                    {this.state.textInput.map((value) => {
                                                        return value
                                                    })}
                                                </View>
                                            ))}
                                    </View> : null}

                                {/* {this.state.textInput.map((value) => {
                                    return value
                                })} */}
                                {/* <View style={styles.input}>
                                <Image

                                    source={require('../../../../assets/userGrey.png')}
                                    style={styles.icon}
                                />
                                <View style={styles.inputViewStyle}>
                                    <TextInput
                                        require
                                        placeholder="Kids Name"
                                        // underlineColorAndroid="#000"
                                        placeholderTextColor="#000"
                                        value={this.state.companyAddress}
                                        onChangeText={(companyAddress) => this.setState({ companyAddress })}
                                        style={styles.txtInput}
                                    />
                                </View>
                            </View> */}

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/dob.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <DatePicker
                                            allowFontScaling={false}
                                            showIcon={false}
                                            date={this.state.dob}
                                            mode="date"
                                            placeholder={this.state.dob}
                                            format="DD-MM-YYYY"
                                            // minDate={new Date()}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateInput: styles.dateInput,
                                                dateText: styles.dateText,
                                                placeholderText: styles.placeholderText,
                                                btnTextConfirm: styles.btnTextConfirm,
                                            }}
                                            onDateChange={(date) => { this.setState({ dob: date }) }}
                                        />
                                    </View>
                                </View>
                                {this.state.GenderStatus == true ?
                                    <View style={styles.input}>
                                        <Image
                                            source={require('../../../../assets/dob.png')}
                                            style={styles.icon}
                                        />
                                        <View style={styles.inputViewStyle}>
                                            <DatePicker
                                                allowFontScaling={false}
                                                showIcon={false}
                                                date={this.state.anniversary}
                                                mode="date"
                                                placeholder={this.state.anniversary}
                                                format="DD-MM-YYYY"
                                                // minDate={new Date()}
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                customStyles={{
                                                    dateInput: styles.dateInput,
                                                    dateText: styles.dateText,
                                                    placeholderText: styles.placeholderText,
                                                    btnTextConfirm: styles.btnTextConfirm,
                                                }}
                                                onDateChange={(anniversary) => { this.setState({ anniversary: anniversary }) }}
                                            />
                                        </View>
                                    </View> : null}
                                <View>
                                    <View style={styles.btnView}>
                                        <TouchableOpacity
                                            style={[styles.Btn, styles.shadow]}
                                            onPress={() => this.UpdateUser(JSON_ListView_Clicked_Item)}>
                                            <Text style={styles.BtnTxt}>Submit</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </Container >
        );
    }
}

