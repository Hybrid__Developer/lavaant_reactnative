import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
const styles = StyleSheet.create({
    subHeaderView: {
        paddingTop: '8%',
        paddingLeft: '4%',
        width: '100%',
        backgroundColor: '#199bf1'
    },
    currentDateTxt: {
        fontSize: 30,
        color: 'white',
        fontWeight: '900'
    },
    pickerView: {
        // marginLeft: "4%",
        marginBottom: "5%",
        //  marginRight: '5%', zIndex: 1 
    },
    dateInput: {
        borderWidth: 0,
    },
    dateText: {
        left: -28,
        color: '#000',
    },
    placeholderText: {
        left: -28,
        color: '#000',
    },
    btnTextConfirm: {
        color: colors.primaryColor,
        fontWeight: 'bold',
        fontSize: 18
    },
    autocompleteText: {
        ...Platform.select({
            android: {
                fontWeight: '600',
            }
        }), fontSize: 17, color: 'black'
    },
    autocompleteListStyle: {
        borderColor: 'transparent',
        backgroundColor: '#fff',
    },
    inputContainer: {
        fontSize: 24,
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        // justifyContent: 'center',
        // alignContent: 'center',
        overflow: 'hidden',
    },
    container: {
        zIndex: 1,
    },
    txtHeader: {
        marginLeft: "3.5%",
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 30
    },
    txtHeaderBorder: {
        borderBottomColor: colors.borderColor,
        borderBottomWidth: 1,
        marginBottom: '2%'
    },
    input: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        width: 20,
        height: 20,
        marginRight: '5%',
    },
    email: {
        width: 24,
        height: 17,
        marginRight: '4%',
    },
    txtInput: {
        color: '#000',
        flex: 1,
    },
    BtnTxt: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: colors.primaryColor,
        borderRadius: 30,
    },
    drawerHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        marginBottom: 25,
    },
    inputViewStyle: {
        flex: 1,
        borderBottomColor: '#000',
        borderBottomWidth: 1,
        height: 40,
    },
    inputViewStyle2: {
        flex: 1,
        borderBottomColor: '#000',
        borderBottomWidth: 1,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    drawerImageView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
    profilePic: {
        top: 5,
        left: 5,
        width: 120,
        height: 120,
        backgroundColor: 'white',
        borderRadius: 100,

    },
    picker: {
        height: 30, width: '100%',
        ...Platform.select({
            android: {
                marginLeft: 4
            }
        })
    },
    mainView: { paddingLeft: '6.5%', paddingRight: '6.5%' },
    btnView: { marginBottom: '5%', marginTop: '8%' },
    datePickerView3: {
        flexDirection: 'row',
        backgroundColor: "white",
        width: 320,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: 'white',
        marginTop: 10,
        justifyContent: 'space-between',
        paddingLeft: '2%',
        paddingRight: 10
    },
    dropDownImg2: {
        height: 17,
        width: 24,
        marginTop: 14
    },
    editImg: {
        height: 17,
        width: 17,
        marginTop: 14
    },
    catogoryListView: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        paddingTop: 14,
        paddingBottom: 14,
        marginRight: '4%'
    },
    chooseCatogryView: {
        paddingLeft: '4%',
        height: 220,
        marginBottom: 10
    },
    genderStatus: {
        width: '80%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 17,
        marginTop: 7
    },
    checkBox: {
        height: 20,
        width: 20,
        borderColor: '#199bf1',
        borderWidth: 1,
        padding: 4,
        marginBottom: 4
    },
    checkBoxTick: {
        height: 10,
        width: 10,
        borderRadius: 10,
        backgroundColor: '#199bf1',
    },
    container: {

        backgroundColor: 'black',
    },
    buttonView: {
        flexDirection: 'row'
    },
    // textInput: {
    //     height: 30,
    //     borderBottomWidth: 1,
    //     borderBottomColor: 'black',
    //     left: '10%'
    // },
    textInput: {
        height: 40,
        left: 37,
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
});
export default styles;
