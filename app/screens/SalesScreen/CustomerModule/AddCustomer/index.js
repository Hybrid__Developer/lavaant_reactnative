import React, { Component } from 'react';
import {
    View, Text, TextInput, ScrollView, Image, TouchableOpacity, Alert, Button, TouchableHighlightBase
} from 'react-native';
import { Container, Picker } from 'native-base';
import styles from './style'
import MainHeader from '../../../../Components/MainHeader'
import SubHeader from '../../../../Components/SubHeader'
import DatePicker from 'react-native-datepicker'
import Loader from '../../../../Components/Loader';
import * as Utility from '../../../../utility/index';
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import { NavigationActions, StackActions } from 'react-navigation';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year
export default class EditCustomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            profilePic: {},
            firstName: '',
            lastName: '',
            address: '',
            mobileNo: '',
            dob: '',
            query: '',
            country: '',
            anniversary: '',
            companyName: '',
            companyAddress: '',
            companyPhone: '',
            isLoading: false,
            selectedGroup: '',
            customerGroups: [],
            email: '',
            alternateNo: '',
            user: '',
            selected: "",
            selectedGender: "",
            showHideGender: "",
            GenderStatus: false,
            textInput: [],
            inputData: [],
            inputData2: [],
            textInput2: [],
            intersts: "",
            hobbies: "",
            companionName: "",
            anniversary2: "",
            date: "",
            companyName: ""

        };
        this.getRole()
        this.getValues()
    }
    getRole = async () => {
        let role = await Utility.getFromLocalStorge('role')
        this.setState({
            role: role
        })
        this.getUserGroups()
    }

    getUserGroups = async () => {
        let token = await Utility.getFromLocalStorge('token')
        console.log('user::profile::getUser', token)
        this.setState({
            isLoading: true
        })
        let res = await Service.get(Url.GET_UserGroups_URL, token)
        console.log('profile::getUserGroups', res.data)
        await this.setState({
            isLoading: false,
            customerGroups: res.data
        })
    }
    updateUser = (user) => {
        this.setState({ user: user })
    }
    showHideGender = (value) => {
        this.setState({
            selected2: value

        })
    }
    GenderStatus = (value) => {
        this.setState({
            GenderStatus: value,

        })
    }
    showHide = (value) => {
        this.setState({
            selected: value

        })
    }
    MarriedStatus = (value) => {
        this.setState({
            selectedGender: value
        })
    }
    addTextInput = (index) => {
        let textInput = this.state.textInput;
        textInput.push(<View><View style={styles.input}>
            <Image

                source={require('../../../../assets/userGrey.png')}
                style={styles.icon}
            />
            <View style={styles.inputViewStyle2}>
                <TextInput
                    require
                    onChangeText={(text) => this.addValues(text, index)}
                    placeholder="Enter Kids Name."
                    // underlineColorAndroid="#000"
                    placeholderTextColor="#000"
                    style={styles.txtInput}></TextInput>
                <TouchableOpacity onPress={() => { this.removeTextInput(this.state.textInput.length) }}>

                    <Image source={require("../../../../assets/subtract.png")} style={styles.addImg}></Image>
                </TouchableOpacity>


            </View>


        </View><TextInput style={styles.textInput}
            placeholderTextColor="black"
            keyboardType={"numeric"}
            placeholder="Enter kids DOB"
            onChangeText={(birthday) => this.addValues2(birthday, index)} /></View>);
        this.setState({ textInput });
    }

    //function to remove TextInput dynamically
    removeTextInput = () => {
        let textInput = this.state.textInput;
        let inputData = this.state.inputData;
        textInput.pop();
        inputData.pop();
        this.setState({ textInput, inputData });
    }

    //function to add text from TextInputs into single array
    addValues = (name, index) => {
        let dataArray = this.state.inputData;
        let checkBool = false;
        if (dataArray.length !== 0) {
            dataArray.forEach(element => {
                if (element.index === index) {
                    element.name = name;
                    checkBool = true;
                }
            });
        }
        if (checkBool) {
            this.setState({
                inputData: dataArray
            });
        }
        else {
            dataArray.push({ 'text': name, 'index': index });
            this.setState({
                inputData: dataArray
            });
        }
    }

    //function to console the output
    getValues = () => {

        console.log('Data', this.state.inputData);
    }
    AddCustomerUser = async () => {
        if (Utility.isFieldEmpty(this.state.firstName && this.state.lastName && this.state.address
            && this.state.mobileNo && this.state.alternateNo && this.state.companyPhone && this.state.intersts
            && this.state.hobbies && this.state.dob
        )) {
            Alert.alert("", "Please enter All field")
            return
        }

        else {
            let body = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                mobileNo: this.state.mobileNo,
                alternateMobileNo: this.state.alternateNo,
                address: this.state.address,
                role: "customer",
                dob: this.state.dob,
                password: "",
                country: "",
                anniversary: "",
                customerGroup: this.state.selectedGroup,
                workDetails: {
                    companyName: this.state.companyName,
                    companyAddress: "",
                    companyPhone: this.state.companyPhone || '',
                },
                otherInformation: {
                    isMarried: this.state.GenderStatus,
                    interests: this.state.intersts,
                    kids: this.state.inputData,
                    hobbies: this.state.hobbies,
                    companionName: this.state.companionName,
                    anniversary: this.state.anniversary,


                },

                kids: this.state.inputData,


            }
            console.log("body::::::::", body)
            await this.setState({
                isLoading: true
            })
            const res = await Service.post(Url.CREATE_CUSTOMER_USER_URL, '', body)
            console.log("alert mesge :::::", res.message)
            await this.setState({
                isLoading: false
            })
            var responsee = res

            console.log("isSucess", responsee.isSuccess)

            if (res.message == "User Resgiter Successfully") {
                Alert.alert(
                    '',
                    responsee.message,
                    [


                        { text: 'OK', onPress: () => this.goToCustomerList() },
                    ],
                    { cancelable: false },
                );
            }
            else {
                Alert.alert(responsee.message)
            }


        }

    }
    goToCustomerList = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CustomerList' })]
            })
        );
        navigate.navigate('CustomerList');
    }


    addTextInput2 = (index) => {
        let textInput2 = this.state.textInput2;
        textInput2.push(<TextInput style={styles.textInput}
            placeholderTextColor="black"
            placeholder="Enter kids DOB"
            onChangeText={(text) => this.addValues(text, index)} />);
        this.setState({ textInput2 });
    }

    //function to remove TextInput dynamically
    removeTextInput2 = () => {
        let textInput2 = this.state.textInput2;
        let inputData2 = this.state.inputData2;
        textInput2.pop();
        inputData2.pop();
        this.setState({ textInput2, inputData2 });
    }

    //function to add text from TextInputs into single array
    addValues2 = (birthday, index) => {
        let dataArray = this.state.inputData;
        let checkBool = false;
        if (dataArray.length !== 0) {
            dataArray.forEach(element => {
                if (element.index === index) {
                    element.birthday = birthday;
                    checkBool = true;
                }
            });
        }
        if (checkBool) {
            this.setState({
                inputData: dataArray
            });
        }
        else {
            dataArray.push({ 'birthday': birthday, 'index': index });
            this.setState({
                inputData: dataArray
            });
        }
    }


    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />

                    <SubHeader
                        title={month + ' ' + date + ', ' + year}></SubHeader>

                    <View>
                        <View style={styles.drawerHeader}>
                        </View>
                        <View style={styles.pickerView}>
                            <Text style={{
                                fontWeight: 'bold',
                                fontSize: 18,
                                marginLeft: '3%'
                            }}>Customer Type </Text>

                            <View style={styles.inputViewStyle}>
                                <Picker
                                    mode='dropdown'
                                    selectedValue={this.state.selectedGroup}
                                    itemTextStyle={{ textTransform: 'capitalize' }}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ selectedGroup: itemValue })
                                    }>
                                    {this.state.customerGroups.map((item, key) =>
                                        <Picker.Item key={key} label={item.name} value={item.id} />
                                    )}
                                </Picker>

                            </View>
                        </View>

                        <View>
                            <View style={styles.mainView}>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/userGrey.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="First Name"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(firstName) => this.setState({ firstName })}

                                            style={styles.txtInput}></TextInput>

                                    </View>

                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/userGrey.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="last Name"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(lastName) => this.setState({ lastName })}

                                            style={styles.txtInput}></TextInput>

                                    </View>

                                </View>

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/email.png')}
                                        style={styles.email}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Email Address"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(email) => this.setState({ email })}

                                            style={styles.txtInput}></TextInput>

                                    </View>

                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/address.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Address"
                                            // underlineColorAndroid="#000"
                                            placeholderTextColor="#000"
                                            onChangeText={(address) => this.setState({ address })}
                                            style={styles.txtInput}
                                        />
                                    </View>

                                </View>

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/phone_number.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Phone Number"
                                            placeholderTextColor="#000"
                                            keyboardType="numeric"
                                            onChangeText={(mobileNo) => this.setState({ mobileNo })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/phone_number.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Alternate Number"
                                            keyboardType="numeric"
                                            placeholderTextColor="#000"
                                            onChangeText={(alternateNo) => this.setState({ alternateNo })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/userGrey.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Company Name"
                                            placeholderTextColor="#000"
                                            onChangeText={(companyName) => this.setState({ companyName })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/phone_number.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Company Phone"
                                            placeholderTextColor="#000"
                                            keyboardType="numeric"
                                            onChangeText={(companyPhone) => this.setState({ companyPhone })}
                                            style={styles.txtInput}></TextInput>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <View style={styles.txtHeaderBorder}>
                            </View>

                            <View style={styles.mainView}>
                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/interseted.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Interests"
                                            placeholderTextColor="#000"
                                            onChangeText={(intersts) => this.setState({ intersts })}
                                            style={styles.txtInput}></TextInput>

                                    </View>
                                </View>


                                <View style={styles.input}>
                                    <Image

                                        source={require('../../../../assets/hobies.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <TextInput
                                            require
                                            placeholder="Hobbies"
                                            placeholderTextColor="#000"
                                            onChangeText={(hobbies) => this.setState({ hobbies })}
                                            style={styles.txtInput}></TextInput>

                                    </View>
                                </View>

                                <View style={styles.input}>
                                    <Image

                                        source={require('../../../../assets/married.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle2}>

                                        <TextInput
                                            editable={false}
                                            style={{ color: 'black' }}
                                            placeholderTextColor="black"
                                            placeholder="Married Status"></TextInput>

                                        {this.state.selected == 'Down' ?
                                            <TouchableOpacity onPress={() => this.showHide('Up')}>
                                                <Image source={require('../../../../assets/up.png')} style={styles.dropDownImg2}></Image>
                                            </TouchableOpacity> : <TouchableOpacity onPress={() => this.showHide('Down')}>
                                                <Image source={require('../../../../assets/left-arrow.png')} style={styles.dropDownImg2}></Image>
                                            </TouchableOpacity>}
                                    </View>



                                </View>
                                {this.state.selected == 'Down' ?
                                    <View style={styles.genderStatus}>
                                        <View style={{ flexDirection: 'column', }}>
                                            <Text style={{ marginBottom: 7 }}>Married</Text>
                                            <Text style={{ marginBottom: 7 }}>Unmarried</Text>

                                        </View>
                                        <View style={{ flexDirection: 'column', }}>
                                            <TouchableOpacity onPress={() => this.GenderStatus(true)}>
                                                <View style={styles.checkBox}>
                                                    <View>
                                                        {this.state.GenderStatus == true ?
                                                            <View style={styles.checkBoxTick}>

                                                            </View> : null}
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.GenderStatus(false)}>
                                                <View style={styles.checkBox}>
                                                    <View>


                                                        {this.state.GenderStatus == false ?
                                                            <View style={styles.checkBoxTick}>
                                                            </View> : null}

                                                    </View>
                                                </View>
                                            </TouchableOpacity>


                                        </View>
                                    </View> : null}
                                {this.state.GenderStatus == true ?
                                    <View style={styles.input}>
                                        <Image

                                            source={require('../../../../assets/userGrey.png')}
                                            style={styles.icon}
                                        />
                                        <View style={styles.inputViewStyle}>
                                            <TextInput
                                                require
                                                placeholder="Companion Name"
                                                placeholderTextColor="#000"
                                                onChangeText={(companionName) => this.setState({ companionName })}
                                                style={styles.txtInput}></TextInput>

                                        </View>
                                    </View> : null}
                                {this.state.GenderStatus == true ?
                                    <View style={styles.input}>
                                        <Image

                                            source={require('../../../../assets/userGrey.png')}
                                            style={styles.icon}
                                        />
                                        <View style={styles.inputViewStyle2}>

                                            <TextInput
                                                style={{ color: 'black' }}
                                                placeholderTextColor="black"
                                                onChangeText={(text) => this.addValues(text)}
                                                placeholder="Enter Kid Name."></TextInput>
                                            <TouchableOpacity onPress={() => { this.addTextInput(this.state.textInput.length); this.addTextInput2(this.state.textInput2.length) }}>
                                                <Image source={require('../../../../assets/add.png')} style={styles.addImg}></Image>
                                            </TouchableOpacity>

                                        </View>
                                    </View>
                                    : null}

                                {this.state.GenderStatus == true ?
                                    <View style={styles.input}>
                                        <Image

                                            source={require('../../../../assets/white.png')}
                                            style={styles.icon}
                                        />
                                        <View style={styles.inputViewStyle2}>

                                            <TextInput
                                                style={{ color: 'black' }}
                                                placeholderTextColor="black"
                                                keyboardType={"numeric"}
                                                onChangeText={(birthday) => this.addValues2(birthday)}
                                                placeholder="Enter Kids DOB."></TextInput>


                                        </View>
                                    </View>
                                    : null}

                                {this.state.textInput.map((value) => {
                                    return value
                                })}

                                <View style={styles.input}>
                                    <Image
                                        source={require('../../../../assets/dob.png')}
                                        style={styles.icon}
                                    />
                                    <View style={styles.inputViewStyle}>
                                        <DatePicker
                                            allowFontScaling={false}
                                            showIcon={false}
                                            date={this.state.dob}
                                            mode="date"
                                            placeholder=" Date Of birth"
                                            format="DD-MM-YYYY"
                                            // minDate={new Date()}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateInput: styles.dateInput,
                                                dateText: styles.dateText,
                                                placeholderText: styles.placeholderText,
                                                btnTextConfirm: styles.btnTextConfirm,
                                            }}
                                            onDateChange={(date) => { this.setState({ dob: date }) }}
                                        />
                                    </View>
                                </View>
                                {this.state.GenderStatus == true ?
                                    <View style={styles.input}>
                                        <Image
                                            source={require('../../../../assets/dob.png')}
                                            style={styles.icon}
                                        />
                                        <View style={styles.inputViewStyle}>
                                            <DatePicker
                                                allowFontScaling={false}
                                                showIcon={false}
                                                date={this.state.anniversary}
                                                mode="date"
                                                placeholder="anniversary"
                                                format="DD-MM-YYYY"
                                                // minDate={new Date()}
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                customStyles={{
                                                    dateInput: styles.dateInput,
                                                    dateText: styles.dateText,
                                                    placeholderText: styles.placeholderText,
                                                    btnTextConfirm: styles.btnTextConfirm,
                                                }}
                                                onDateChange={(anniversary) => { this.setState({ anniversary: anniversary }) }}
                                            />
                                        </View>
                                    </View> : null}
                                <View>
                                    <View style={styles.btnView}>
                                        <TouchableOpacity
                                            style={[styles.Btn, styles.shadow]}
                                            onPress={() => this.AddCustomerUser()}>
                                            <Text style={styles.BtnTxt}>Submit</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </Container >
        );
    }
}

