import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({

    subHeaderView: {
        paddingTop: '8%',
        paddingLeft: '4%',
        paddingBottom: '9%',
        width: '100%',
        backgroundColor: '#199bf1'
    },
    currentDateTxt: {
        fontSize: 30,
        color: 'white',
        fontWeight: '900',
        textAlign: 'center'
    },
    QoutesView: {
        width: '90%',
        padding: 7,
        alignSelf: 'center',
        borderColor: 'grey',
        backgroundColor: 'white',
        borderWidth: 1,
        bottom: 20


    },
    mainSearchView: {
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row'
    },
    QoutesTxt: {
        marginLeft: 10,
        fontSize: 20,
        fontWeight: 'bold'
    },
    QoutesTxt2: {
        marginLeft: 14,
        fontSize: 14,
    },
    searchView: {
        flexDirection: 'row',
        width: '100%',
        height: 35,

    },
    searchTextInput: {
        width: '80%',
        height: 35,
        marginLeft: 5
    },
    searchIcon: {
        color: 'white',
        flex: 1,
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: '1%',
    },
    filterView: {
        color: 'white',
    },
    filterIcon: {
        color: 'white',
        flex: 1,
        width: 40,
        height: 40,
        resizeMode: 'contain',

    },
    userImg: {
        left: 2,
        width: 50,
        height: 51,
        backgroundColor: 'white',
        borderRadius: 100,
    },
    userImgBackgroung: {
        width: 54,
        height: 54,
        position: 'absolute',
        marginLeft: 14,
        top: 13

    },
    modalMainView: {
        height: 140, width: 170, backgroundColor: "white",
        justifyContent: "space-between",
        margin: 150, elevation: 120, top: '20%'
    },
    modalSubView: {
        flexDirection: "row", paddingTop: 5, borderBottomWidth: 1,
        borderBottomColor: "grey",
    },
    mainView: {
        justifyContent: 'space-between',
        bottom: 4,
        padding: '4%', marginTop: 10,
        width: '90%', alignSelf: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#ddd',
        borderBottomWidth: 2,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 10,
    },
    subView: {
        flexDirection: 'column',
        width: '70%',
        marginLeft: '4%'
    },
    editImg: {
        height: 24,
        width: 24
    },
    marginBottom: { marginBottom: 4 },
    padding: {
        paddingLeft: 20
    },
    fontSize: { fontSize: 17, },
    fontSize2: { fontSize: 14, },
})

export default styles;