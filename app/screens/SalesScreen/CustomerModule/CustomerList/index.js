import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import Modal from 'react-native-modal';
import styles from './style'
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import Loader from '../../../../Components/Loader';
import * as Utility from '../../../../utility/index';

import {
    Container,
    Card,
    CheckBox,

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate();
var month = monthNames[new Date().getMonth()];
var year = new Date().getFullYear();

export default class CustomerList extends Component {

    constructor(props) {
        super(props);
        this.openFilters = this.openFilters.bind(this)
        this.state = {

            isModalVisible: false,
            checked: false,
            Qoutes: "",
            search: "",
            CustomerList: [],
            id: "",
            list: [

                { name: "Sort by Company", id: 1, value: "asce", check: false },
                { name: "Sort by Name", id: 2, value: "asce", check: false },
                { name: "Ship-to", id: 3, value: "asce", check: false }, ,
                { name: "Sort by Area", id: 4, value: "asce", check: false }
            ],
            value: "",
            id: "",
            selected: ""

        }

    }
    _showModal = () => {
        if (this.state.isModalVisible) {
            this.setState({ isModalVisible: false })
        }
        else {
            this.setState({ isModalVisible: true })
        }

    }
    _hideModal = () => {
        this.setState({ isModalVisible: false })
    }
    componentDidMount() {

        this.getQuotes()
        // this.ViewAllUserList()

    }
    getQuotes = async () => {
        await this.setState({
            isLoading: true
        })
        const res = await Service.get(Url.GET_QUOTES_URL)
        var responsee = res
        await this.setState({
            isLoading: false
        })
        if (res.data)
            var responsee = res.data
        console.log("add category ::::response", responsee)

        this.setState({
            isLoading: false,
            Qoutes: responsee.title
        })
    }
    searchProduct = async () => {

        let token = await Utility.getFromLocalStorge('token')
        console.log("alert mesge :::::", token)
        if (this.state.selected == "company") {
            await this.setState({
                isLoading: true
            })
            const res = await Service.get(Url.CustomerUserSearch_URL + `searchString=${this.state.search}&sortByCompany=${this.state.value}&role=customer`, token)
            console.log('searchProduct::::::::::::', res)
            await this.setState({
                isLoading: false
            })
            if (res.data) {
                this.setState({
                    CustomerList: res.data,
                    isLoading: false,

                })


            }

            else {
                this.setState({ isLoading: false })
                Alert.alert('', 'Something Went Wrong')
            }
        }
        else if (this.state.selected == "name") {
            await this.setState({
                isLoading: true
            })
            const res = await Service.get(Url.CustomerUserSearch_URL + `searchString=${this.state.search}&sortByName=${this.state.value}&role=customer`, token)
            console.log('searchProduct::::::::::::', res)
            await this.setState({
                isLoading: false
            })
            if (res.data) {
                this.setState({
                    CustomerList: res.data,
                    isLoading: false,

                })


            }

            else {
                this.setState({ isLoading: false })
                Alert.alert('', 'Something Went Wrong')
            }
        }
        else if (this.state.selected == "shipTo") {
            await this.setState({
                isLoading: true
            })
            const res = await Service.get(Url.CustomerUserSearch_URL + `searchString=${this.state.search}&sortByShipTo=${this.state.value}&role=customer`, token)
            console.log('searchProduct::::::::::::', res)
            await this.setState({
                isLoading: false
            })
            if (res.data) {
                this.setState({
                    CustomerList: res.data,
                    isLoading: false,

                })


            }

            else {
                this.setState({ isLoading: false })
                Alert.alert('', 'Something Went Wrong')
            }
        }
        else if (this.state.selected == "Area") {
            await this.setState({
                isLoading: true
            })
            const res = await Service.get(Url.CustomerUserSearch_URL + `searchString=${this.state.search}&sortByArea=${this.state.value}&role=customer`, token)
            console.log('searchProduct::::::::::::', res)
            await this.setState({
                isLoading: false
            })
            if (res.data) {
                this.setState({
                    CustomerList: res.data,
                    isLoading: false,

                })


            }

            else {
                this.setState({ isLoading: false })
                Alert.alert('', 'Something Went Wrong')
            }
        }
        else {
            await this.setState({
                isLoading: true
            })
            const res = await Service.get(Url.CustomerUserSearch_URL + `searchString=${this.state.search}&role=customer`, token)
            console.log('searchProduct::::::::::::', res)
            await this.setState({
                isLoading: false
            })
            if (res.data) {
                this.setState({
                    CustomerList: res.data,
                    isLoading: false,

                })


            }

            else {
                this.setState({ isLoading: false })
                Alert.alert('', 'Something Went Wrong')
            }
        }
    }
    goToUpdateUser = (item) => {
        this.props.navigation.navigate('EditCustomer', {
            JSON_ListView_Clicked_Item: item,
            firstName: item.firstName,
            lastName: item.lastName,
            ids: item.otherInformation.kids,
            mobileNo: item.mobileNo,
            alternateMobileNo: item.alternateMobileNo,
            companyPhone: item.workDetails.companyPhone,
            interests: item.otherInformation.interests,
            hobbies: item.otherInformation.hobbies,
            companionName: item.otherInformation.companionName,
            dob: item.dob,
            anniversary: item.otherInformation.anniversary,
            companyName: item.workDetails.companyName,
            address: item.address,
            id: item.id,



        })

    }

    onFilterPress = () => {
        console.log('Filter button pressed');
    }
    openFilters = async () => {
        await this.setState({
            isLoading: true
        })
        const res = await Service.get(Url.GET_USERLIST_URL + `pageNo=${1}&pageSize=${10}&role=customer`, "")
        console.log(res)
        await this.setState({
            isLoading: false
        })

        this.setState({
            CustomerList: res.items,

        })

    }
    selectedName = (value) => {
        this.setState({
            selected: value,
            value: "asce"


        })

    }
    render() {
        const { data } = this.state;

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />

                    <SubHeader
                        title={month + ' ' + date + ', ' + year}
                        ViewAllPress={this.openFilters}
                        showViewAll={true}></SubHeader>
                    <View style={styles.QoutesView}>
                        <Text style={styles.QoutesTxt}>Quote of the Day</Text>
                        <Text style={styles.QoutesTxt2}>{this.state.Qoutes}</Text>
                    </View>
                    <View style={styles.mainSearchView}>
                        <Card>
                            <View style={styles.searchView} >
                                <TextInput
                                    style={styles.searchTextInput}
                                    placeholder=" Search by Name or Email"
                                    placeholderTextColor="grey"
                                    onChangeText={(search) => this.setState({ search })}
                                />
                                <TouchableOpacity onPress={() => this.searchProduct()}>
                                    <Image
                                        source={require('../../../../assets/search.png')}
                                        style={styles.searchIcon}
                                    />
                                </TouchableOpacity>

                                <Modal

                                    position='absolute'
                                    backdropOpacity={1}
                                    backdropColor="black"
                                    hasBackdrop={true}
                                    visible={this.state.isModalVisible}
                                    onBackdropPress={() => this._showModal()}
                                    onRequestClose={() => this._hideModal()}>
                                    <View style={styles.modalMainView}>

                                        <View style={styles.modalSubView}>
                                            <CheckBox
                                                style={styles.marginBottom}
                                                onPress={() => this.selectedName("company")}

                                                checked={this.state.selected == "company"}
                                            />
                                            <Text style={styles.padding}>
                                                Sort By Company
                                                    </Text>
                                        </View>
                                        <View style={styles.modalSubView}>
                                            <TouchableOpacity>
                                                <CheckBox
                                                    style={styles.marginBottom}

                                                    onPress={() => this.selectedName("name")
                                                    }
                                                    checked={this.state.selected == "name"} />
                                            </TouchableOpacity>
                                            <Text style={styles.padding}>Sort by Name</Text>
                                        </View>
                                        <View style={styles.modalSubView}>
                                            <CheckBox
                                                style={styles.marginBottom}
                                                onPress={() => this.selectedName("shipTo")
                                                }
                                                checked={this.state.selected == "shipTo"}
                                            />
                                            <Text style={styles.padding}>
                                                Ship to
                                       </Text>
                                        </View>
                                        <View style={styles.modalSubView}>
                                            <CheckBox
                                                style={styles.marginBottom}
                                                onPress={() => this.selectedName("Area")
                                                }
                                                checked={this.state.selected == "Area"}
                                            />
                                            <Text style={styles.padding}>
                                                Sort by Area
                                       </Text>
                                        </View>
                                    </View>

                                </Modal>

                            </View>

                        </Card>
                        <View style={styles.filterView}>
                            <TouchableOpacity onPress={() => this._showModal()}>
                                <Image
                                    source={require('../../../../assets/reportFilter.png')}
                                    style={styles.filterIcon}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {

                        this.state.CustomerList.map((item, key) => (
                            <TouchableOpacity >

                                <View style={styles.mainView}>

                                    <Image
                                        style={styles.userImg}
                                        resizeMode='cover'
                                        source={{ uri: item.profilePic }}

                                    />
                                    <Image
                                        style={styles.userImgBackgroung}
                                        resizeMode='cover'
                                        source={require('../../../../assets/image_view1.png')}
                                    />

                                    <View style={styles.subView}>
                                        <Text style={styles.fontSize}>{item.firstName}</Text>
                                        <Text style={styles.fontSize2}>{item.email}</Text>

                                    </View>
                                    <TouchableOpacity onPress={this.goToUpdateUser.bind(this, item)}>
                                        <Image source={require('../../../../assets/pencil.png')} style={styles.editImg}></Image>
                                    </TouchableOpacity>
                                </View>
                            </TouchableOpacity>
                        ))}
                </ScrollView>


            </Container>

        );

    }
}
