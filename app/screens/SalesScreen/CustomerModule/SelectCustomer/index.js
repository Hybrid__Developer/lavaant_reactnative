import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native';
import styles from './style';
import {
    Container,
} from 'native-base';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import { ScrollView } from 'react-native-gesture-handler';
export default class Select extends Component {
    constructor(props) {
        super(props);
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);

    }
    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }

    render() {
        return (

            <Container>

                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <SubHeader title="JUNE 20,2020" />
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ padding: 40, }}>

                            <TouchableOpacity
                            >
                                <Image
                                    source={require('../../../../assets/add.png')}
                                    style={{
                                        width: 100,
                                        height: 100,

                                    }}
                                />
                            </TouchableOpacity>
                            <TextInput
                                style={{ height: 40 }}
                                placeholder="IN PERSON"
                                textAlign="center"
                                underlineColorAndroid="black"
                                placeholderTextColor="black"
                                onChangeText={(text) => this.setState({ text })}
                                value={this.state.text}
                            />

                        </View>
                        <View style={{ padding: 40 }}>

                            <TouchableOpacity
                            >
                                <Image
                                    source={require('../../../../assets/add.png')}
                                    style={{
                                        width: 100,
                                        height: 100,

                                    }}
                                />
                            </TouchableOpacity>
                            <TextInput
                                style={{ height: 40 }}
                                placeholder="PHONE"
                                textAlign="center"
                                underlineColorAndroid="black"
                                placeholderTextColor="black"
                                onChangeText={(text) => this.setState({ text })}
                                value={this.state.text}
                            />
                        </View>
                    </View>
                </ScrollView>
            </Container >
        )
    }
}