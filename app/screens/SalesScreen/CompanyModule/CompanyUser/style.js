import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { Left } from 'native-base';
const styles = StyleSheet.create({



    companyDetailView: {
        marginTop: 20,
        borderBottomColor: 'grey',
        borderBottomWidth: 1
    },
    companyDetailSubView: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: '7%'
    },
    companyDetailTxt: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    editImg: {
        height: 24,
        width: 24
    },
    mobileAdressView: {
        flexDirection: 'row',
        marginTop: 20,
        paddingLeft: '7%',



    },
    borderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        width: '80%',

    },
    callImg: {
        height: 20,
        width: 20,
        marginTop: '7%',
        marginRight: 23
    },
    mobileTxt: {
        width: '90%',
        top: 9,
        color: 'black',
        fontSize: 17

    },
    companyAdressImg: {
        height: 27,
        width: 27,
        marginTop: 20,
        marginRight: 17
    },
    adressTxt: {
        width: '90%',
        top: 9,
        color: 'black',
        fontSize: 14
    },

    mainCardView: {
        width: '90%',
        marginBottom: 70,
        marginTop: 30,
        justifyContent: 'center',
        alignSelf: 'center',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#ddd',
        borderBottomWidth: 2,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 10,

    },
    margin: { marginTop: 20, },
    userTxt: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    subView: {
        flexDirection: 'row',
        marginRight: '3%',
        marginLeft: '3%',
        borderTopWidth: 1,
        borderTopColor: 'grey',

    },

    nameAdressView: {
        flexDirection: 'column',
        marginTop: '4%',
        marginLeft: '5%',
        width: '64%',

    },


    userImg: {
        left: 2,
        width: 47,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 100,
        marginLeft: 7

    },
    userImgBackgroung: {
        width: 51,
        height: 51,
        position: 'absolute',
        marginLeft: 7


    },
    btnView: { marginBottom: 20, },
    BtnTxt: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '80%',
        backgroundColor: colors.primaryColor,
        borderRadius: 30,
        alignSelf: "center",
        marginBottom: 30
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
    tableNumPoistion: { justifyContent: "center" },
    numTxt: { fontSize: 17, marginLeft: 7, textAlign: "center" },
    borderView: { borderRightColor: "black", borderRightWidth: 1, marginLeft: 10, },
    userImgMargin: { marginTop: '3%', marginBottom: '3%' },
    firstNameTxt: { fontSize: 17 },
    lastNameTxt: { fontSize: 17, marginLeft: 7 },
    adresTxt: { fontSize: 14 },
    flexDirection: { flexDirection: "row" }

});

export default styles;