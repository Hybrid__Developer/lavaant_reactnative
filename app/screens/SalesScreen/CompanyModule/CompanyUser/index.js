import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput, Alert, ScrollView } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import styles from './style';
import SubHeader from '../../../../Components/SubHeader'
import { TextInputMask } from 'react-native-masked-text'
import * as commanFn from '../../../../utility/index';
import Loader from '../../../../Components/Loader';
import * as Url from '../../../../constants/urls';
import * as Service from '../../../../api/services';
import * as Utility from '../../../../utility/index';
import { NavigationActions, StackActions } from 'react-navigation';

export default class CompanyUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile_no: '9090909090',
            myState: [],
            ticket: [],
            CounterState: 0,
            msg: 'hello',
            search: '',
            enable: false,
            name: this.props.navigation.state.params.name,
            users: this.props.navigation.state.params.users,
            companyName: this.props.navigation.state.params.companyName,
            companyAddress: this.props.navigation.state.params.companyAddress,
            companyPhone: this.props.navigation.state.params.companyPhone,
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
        }
    }


    editTrue = () => {

        this.setState({
            enable: !this.state.enable,
            submit: "viewbtn"

        })
    }
    UpdateCompany = async () => {
        let token = await Utility.getFromLocalStorge('token')

        if (this.state.companyAddress && this.state.name && this.state.companyPhone) {
            let body = {
                companyName: this.state.name,
                companyPhone: this.state.companyPhone,
                companyAddress: this.state.companyAddress
            }
            console.log("body", body)
            await this.setState({
                isLoading: true
            })
            const res = await Service.put(Url.UPDATE_COMPANYDETAIL_URL + `companyName=${this.state.companyName}`, token, body)
            console.log("aaaaaaaaa::::::::", res)
            await this.setState({
                isLoading: false
            })
            var responsee = res

            if (res.isSuccess == true) {
                Alert.alert(
                    '',
                    "Update sucessfull",
                    [


                        { text: 'OK', onPress: () => this.goToCompanyList() },
                    ],
                    { cancelable: false },
                );
            }
            else {
                this.props.navigation.navigate('CompanyList')

            }
        }
        else {
            Alert.alert("All field mandatory")
        }
    }
    goToCompanyList = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CompanyList' })]
            })
        );
        this.props.navigation.navigate('CompanyList')
    }





    render() {

        return (
            <View>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <View>
                        <SubHeader title={this.state.companyName} />

                        <View style={styles.companyDetailView}>
                            <View style={styles.companyDetailSubView}>
                                <Text style={styles.companyDetailTxt}>Company Details</Text>

                                <TouchableOpacity onPress={() => this.editTrue()}>
                                    <Image source={require('../../../../assets/pencil.png')} style={styles.editImg}></Image>
                                </TouchableOpacity>

                            </View>
                        </View>
                        <View style={styles.mobileAdressView}>
                            <Image source={require('../../../../assets/phone_number.png')} style={styles.callImg}></Image>
                            <View style={styles.borderBottom}>

                                <TextInputMask
                                    require
                                    ref={(ref) => this.companyPhone = ref}
                                    allowFontScaling={false} require
                                    placeholder="Company Number"
                                    placeholderTextColor="#000"
                                    style={styles.mobileTxt}
                                    type={'custom'}
                                    options={{
                                        mask: '9999999999'
                                    }}
                                    keyboardType='numeric'
                                    onChangeText={
                                        (companyPhone) => {
                                            this.setState({ companyPhone })

                                        }
                                    }
                                    value={this.state.companyPhone}
                                    style={styles.txtInput}
                                />
                            </View>
                        </View>
                        <View style={styles.mobileAdressView}>
                            <Image source={require('../../../../assets/company_address.png')} style={styles.companyAdressImg}></Image>
                            <View style={styles.borderBottom}>
                                <TextInput
                                    editable={this.state.enable}
                                    placeholder={"Company Name"}
                                    multiline={true}
                                    onChangeText={(name) => this.setState({ name })}
                                    value={this.state.name}
                                    placeholderTextColor="black"
                                    style={styles.adressTxt}></TextInput>
                            </View>
                        </View>
                        <View style={styles.mobileAdressView}>
                            <Image source={require('../../../../assets/company_address.png')} style={styles.companyAdressImg}></Image>
                            <View style={styles.borderBottom}>
                                <TextInput
                                    editable={this.state.enable}
                                    placeholder={"Company Adress"}
                                    multiline={true}
                                    onChangeText={(companyAddress) => this.setState({ companyAddress })}
                                    value={this.state.companyAddress}
                                    placeholderTextColor="black"
                                    style={styles.adressTxt}></TextInput>
                            </View>
                        </View>



                        <View style={styles.mainCardView}>
                            <View style={styles.margin}>
                                <Text style={styles.userTxt}>
                                    Users
                           </Text>
                            </View>
                            {

                                this.state.users.map((item, key) => (

                                    <View style={styles.subView}>
                                        <View style={styles.tableNumPoistion}>
                                            <Text style={styles.numTxt}>{key + 1}{"."}</Text>
                                        </View>
                                        <View style={styles.borderView}></View>

                                        <View style={styles.userImgMargin}>
                                            {item.profilePic ?
                                                <Image
                                                    style={styles.userImg}
                                                    resizeMode='cover'
                                                    source={{ uri: item.profilePic }}

                                                /> : <Image
                                                    style={styles.userImg}
                                                    resizeMode='cover'
                                                    source={{ uri: this.state.pic }}

                                                />}
                                            <Image
                                                style={styles.userImgBackgroung}
                                                resizeMode='cover'
                                                source={require('../../../../assets/image_view1.png')}
                                            />


                                        </View>

                                        <View style={styles.nameAdressView}>
                                            <View style={{ flexDirection: "row" }}>
                                                <Text style={styles.firstNameTxt}>{item.firstName}</Text>
                                                <Text style={styles.lastNameTxt}>{item.lastName}</Text>
                                            </View>
                                            <Text style={styles.adresTxt}>{item.address}</Text>
                                        </View>
                                    </View>))}

                        </View>
                        {this.state.submit == "viewbtn" ?
                            <View style={styles.btnView}>
                                <TouchableOpacity
                                    style={[styles.Btn, styles.shadow]}
                                    onPress={() => this.UpdateCompany()}>
                                    <Text style={styles.BtnTxt}>Submit</Text>
                                </TouchableOpacity>
                            </View> : null}
                    </View>

                </ScrollView>
            </View >
        )
    }
}