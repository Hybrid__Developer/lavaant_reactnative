import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput, } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import styles from './style';

import { Searchbar } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import SubHeader from '../../../../Components/SubHeader';
import * as commanFn from '../../../../utility/index';
import Loader from '../../../../Components/Loader';
import * as Url from '../../../../constants/urls';
import * as Service from '../../../../api/services';
import { Card, } from 'native-base';
export default class CompanyModule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            enable: false,
            companyList: [],
            isLoading: false
        };


    }

    updateSearch = search => {
        this.setState({ search });
    };
    detail = (item) => {
        this.props.navigation.navigate('CompanyUser', {
            companyName: item.companyName,
            companyAddress: item.companyAddress,
            companyPhone: item.companyPhone,
            name: item.companyName,
            users: item.users
        })
    }

    updateSearch = async (search) => {
        this.setState({ search: search });
        if (search === '') {
            this.getCompanyList();
        }
    }
    searchCompany = async () => {

        console.log('inside  search company list')
        this.setState({
            isLoading: true
        })
        let token = await commanFn.getFromLocalStorge('token')
        console.log('token :: ', token);
        const res = await Service.get(`${Url.GET_SEARCH_COMPANY_URL}companyName=${this.state.search}`, token)
        console.log('response of search customerLit :: ::', res);
        if (res.data) {
            this.setState({
                companyList: res.data,
                isLoading: false
            })


        }
    }

    componentDidMount() {
        this.getCompanyList();
    }

    getCompanyList = async () => {
        console.log('inside company list')
        this.setState({
            isLoading: true
        })
        let token = await commanFn.getFromLocalStorge('token')
        console.log('token :: ', token);
        const res = await Service.get(Url.GET_COMPANYLIST_URL, token)
        console.log('response of customerLit :: ::', res);
        if (res.data) {
            this.setState({
                companyList: res.data,
                isLoading: false
            })


        }
    }

    render() {
        const { search } = this.state;
        return (
            <View>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>

                    <SubHeader title="COMPANY" />

                    <Card style={styles.searchCard}>

                        <View style={styles.searchView} >
                            <TextInput
                                style={styles.searchTextInput}
                                placeholder="Search"
                                placeholderTextColor="grey"
                                onChangeText={(search) => this.updateSearch(search)}
                            // style={styles.loginTxtInput}
                            />
                            <TouchableOpacity onPress={() => this.searchCompany()} >
                                <Image source={require('../../../../assets/search.png')} style={styles.searchIcon} />
                            </TouchableOpacity>
                        </View>
                    </Card>
                    <Loader isLoading={this.state.isLoading} />
                    <View style={styles.marginBottom}>
                        {

                            this.state.companyList.map((item, key) => (
                                <TouchableOpacity onPress={this.detail.bind(this, item)}>
                                    <View style={styles.shadow}>
                                        <View style={styles.mainView}>
                                            <View style={styles.subView}>

                                                <Text style={styles.companyNameTxt}>{item.companyName}</Text>
                                                <Text
                                                    style={styles.companyAdressTxt}>{item.companyAddress}</Text>
                                            </View>

                                        </View>
                                    </View>
                                </TouchableOpacity>


                            ))}
                    </View>
                </ScrollView>
            </View>



        )
    }
}









