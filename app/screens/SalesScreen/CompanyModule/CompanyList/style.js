import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { Left } from 'native-base';
const styles = StyleSheet.create({



    searchCard: {
        marginTop: -15,
        width: '90%',
        alignSelf: 'center',
        marginBottom: '5%',
        borderRadius: 5
    },
    searchView: {
        backgroundColor: 'white',
        bottom: 20,
        marginRight: 20,
        marginLeft: 20,
        elevation: 10
    },
    shadow: {
        borderRadius: 0,
        elevation: 1,
        justifyContent: 'space-between',
        marginTop: 10,
        width: '90%', alignSelf: 'center', flexDirection: 'row',
    },
    mainView: {
        justifyContent: 'space-between',
        padding: '4%', marginTop: 10,
        width: '90%', alignSelf: 'center', flexDirection: 'row',
        borderRadius: 10

    },
    subView: {
        flexDirection: 'column',
    },
    companyNameTxt: {
        fontSize: 19,
        color: 'black',
        fontWeight: 'bold',
        height: 70,
    },
    companyAdressTxt: {
        fontSize: 14,
        color: 'black',
        height: 70,
    },
    editImg: {
        height: 30,
        width: 30
    },

    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 50,
        padding: 30,
        backgroundColor: '#ffffff',
    },
    input: {
        width: 250,
        height: 44,
        padding: 10,
        margin: 20,
        backgroundColor: 'red',
    },
    viewStyle: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'white',
        marginTop: Platform.OS == 'ios' ? 30 : 0,
    },
    textStyle: {
        padding: 10,
    },
    searchView: {
        flexDirection: 'row',
        width: '100%',
        height: 35,

        // borderRadius: 15
        // backgroundColor: 'orange'
    },
    searchTextInput: {
        width: '80%',
        height: 35,
        marginLeft: 5,
        // backgroundColor: 'pink'
    },
    searchIcon: {

        // flex: 1,
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: '1%',
        // backgroundColor: 'red'
    },
    marginBottom: { marginBottom: 70 }
});

export default styles;
