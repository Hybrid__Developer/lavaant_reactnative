
import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, StatusBar, ScrollView, Image, SafeAreaView, ImageBackground, Alert, Platform } from 'react-native'
import MenuDrawer from 'react-native-side-drawer'
import styles from "./style"
import { NavigationActions, StackActions } from 'react-navigation';
import * as Utility from '../../../utility/index';
import Modal from 'react-native-modal';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import {
    Header, Left, Body,
    Container, Icon,

} from 'native-base';
import SubHeader from '../../../Components/SubHeader';
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 70 : null;
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            isModalVisible: false,
            modalVisible: true,
        };
    }

    toggleOpen = () => {
        this.setState({ open: !this.state.open });

    };
    onClose = () => this.setState({ modalVisible: false });
    profile = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'MyProfile' })]
            })
        );
        this.props.navigation.navigate('MyProfile')
    }
    changePassword = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ChangePassword' })]
            })
        );
        this.props.navigation.navigate('ChangePassword')
        // this.props.navigation.dispatch(DrawerActions.closeDrawer())
    }
    contactUs = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ContactUs' })]
            })
        );
        this.props.navigation.navigate('ContactUs')
        // this.props.navigation.dispatch(DrawerActions.closeDrawer())
    }
    SignOut = () => {
        Alert.alert(
            "",
            "Do You Want to Sign Out",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => this.resetUserId() },

            ],
            { cancelable: false },
        );
        return true;

    }
    resetUserId = async () => {
        await Utility.removeAuthKey('token')
        await Utility.removeAuthKey('user')
        await Utility.removeAuthKey('userId')
        global.favouriteSubCategories = []
        console.warn('userDetail successfully removed')
        this.props.navigation.navigate('SignIn')
    }
    goBack = (value) => {
        this.setState({
            open: value
        })
    }
    goToCustomerList = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CustomerList' })]
            })
        );
        this.props.navigation.navigate('CustomerList')
    }
    goToAllTask = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AllTask' })]
            })
        );
        this.props.navigation.navigate('AllTask')
    }
    goToCalandar = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CalanderScreen' })]
            })
        );
        this.props.navigation.navigate('CalanderScreen')
    }
    goToCompanyList = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CompanyList' })]
            })
        );
        this.props.navigation.navigate('CompanyList')
    }
    goToOrderList = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'OrderList' })]
            })
        );
        this.props.navigation.navigate('OrderList')
    }
    _hideModal = () => {
        this.setState({ isModalVisible: false })
        return;
    }
    _showModal = () => {
        if (this.state.isModalVisible) {
            this.setState({ isModalVisible: false })
        }
        else {
            this.setState({ isModalVisible: true })
        }
    }
    goBack = (value) => {
        this.setState({
            open: value
        })
    }
    drawerContent = () => {
        return (

            <View>


                <View style={{ height: '100%', backgroundColor: "#3492d4" }}>
                    <Modal backdropOpacity={0.40} backdropColor="white" isVisible={this.state.isModalVisible}
                        hasBackdrop={true}
                        animationInTiming={100}
                        animationOutTiming={100}
                        animationOut={"bounceOutLeft"}
                        animationIn={"bounceInLeft"}
                        onBackdropPress={() => this._showModal()}
                        onRequestClose={() => this._hideModal()}

                    >

                        <SafeAreaView style={{ right: 20, height: '110%', width: "83%", }}>
                            <ImageBackground source={require('../../../assets/bg_draw.png')} style={{ height: '100%' }}>
                                <View style={styles.cantainer}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 48, marginBottom: 50 }}>
                                        <ImageBackground source={{ uri: global.user.profilePic || this.state.pic }} style={{
                                            height: 160, width: 160,
                                        }}>
                                            <Image source={require('../../../assets/polygon2.png')} style={{ height: 160, width: 160, }} />
                                        </ImageBackground>
                                    </View>

                                    <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
                                        <Text
                                            allowFontScaling={false}
                                        >{this.state.name}</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: '#fff' }}>{this.state.email}</Text>
                                    </View>
                                    <View style={{ marginLeft: 20, }}>
                                        <TouchableOpacity onPress={this.profile}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../../../assets/userNew.png')} style={styles.icon} />
                                                <Text
                                                    allowFontScaling={false}
                                                    style={styles.txt}>My Profile</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={this.changePassword}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../../../assets/lockNew.png')} style={styles.icon} />
                                                <Text
                                                    allowFontScaling={false}
                                                    style={styles.txt}>Change Password</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={this.contactUs}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../../../assets/settingNew.png')} style={styles.icon} />
                                                <Text
                                                    allowFontScaling={false}
                                                    style={styles.txt}>Contact Us</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.SignOut()}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={require('../../../assets/logoutNew.png')} style={styles.icon} />
                                                <Text
                                                    allowFontScaling={false}
                                                    style={styles.txt} >Log Out</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </ImageBackground>
                        </SafeAreaView>
                    </Modal>

                </View>
            </View>
        );
    };

    render() {
        return (

            <MenuDrawer
                open={this.state.isModalVisible}
                drawerContent={this.drawerContent()}
                drawerPercentage={70}
                animationTime={240}
                overlay={true}
                opacity={1}
            >
                <View style={styles.bottom}>
                    <Header style={[{
                        backgroundColor: '#dcf0fe', height: STATUSBAR_HEIGHT,
                    }]}>

                        <Left>
                            <TouchableOpacity onPress={() => this._showModal()}>
                                <Icon name="md-menu" size={30} style={styles.iconColor} /></TouchableOpacity>
                        </Left>
                        <Body style={styles.headerBody}>
                            <View style={styles.headerFlexDirection}>
                                <Image source={require('../../../assets/logoSalesApp.png')} style={styles.headerLogo} />

                            </View>

                        </Body>
                    </Header>
                </View>

                <ScrollView style={styles.Bottom}>
                    <TouchableOpacity activeOpacity={1} onPress={() => this.goBack(false)}>
                        <SubHeader></SubHeader>

                        <View style={styles.mainview}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => this.goToAllTask()}>
                                <View style={styles.subview}>
                                    <Image
                                        source={require('../../../assets/tasks.png')}
                                        style={styles.taskImg}
                                    />
                                    <View style={styles.taskView}>
                                        <Text style={styles.taskTxt}>
                                            TASKS</Text></View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => this.goToCustomerList()}>
                                <View style={styles.subview} >
                                    <Image
                                        source={require('../../../assets/customers.png')}
                                        style={styles.customerImg}
                                    />
                                    <View style={styles.taskView}>
                                        <Text style={styles.customerTxt}>
                                            CUSTOMERS</Text></View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.mainView}>
                            <TouchableOpacity
                                activeOpacity={1}

                                onPress={() => this.goToCalandar()}>
                                <View style={
                                    styles.subview} >
                                    <Image
                                        source={require('../../../assets/calendar.png')}
                                        style={styles.calanderImg}
                                    />
                                    <View style={styles.calanderView}>
                                        <Text style={styles.taskTxt}>
                                            CALENDAR</Text></View>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={1}

                                onPress={() => this.goToCompanyList()}>

                                <View style={
                                    styles.subview} >
                                    <Image
                                        source={require('../../../assets/company.png')}
                                        style={styles.calanderImg}
                                    />
                                    <View style={styles.calanderView}>
                                        <Text style={styles.taskTxt}>
                                            COMPANY</Text></View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.orderMainView}>

                            <TouchableOpacity
                                activeOpacity={1}

                                onPress={() => this.goToOrderList()}>
                                <View style={styles.subview} >
                                    <Image
                                        source={require('../../../assets/orders.png')}
                                        style={styles.orderImg}
                                    />
                                    <View style={styles.orderTxtView}>
                                        <Text style={styles.customerTxt}>
                                            ORDERS</Text></View>
                                </View>
                            </TouchableOpacity>
                        </View>

                    </TouchableOpacity>
                </ScrollView>
            </MenuDrawer>
        );
    }
}


