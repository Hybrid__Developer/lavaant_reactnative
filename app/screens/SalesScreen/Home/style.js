import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../utility/index';
import { Left } from 'native-base';
const styles = StyleSheet.create({



    taskImg: {
        width: 50,
        height: 70,
    },
    taskView: { marginTop: 10 },
    taskTxt: {
        color: '#2a9df4',
        fontSize: 20,
        fontWeight: "bold"
    },
    customerImg: { height: 67, width: 74 },
    customerTxt: {
        color: '#2a9df4',
        fontWeight: "bold",
        fontSize: 18
    },
    mainView: {
        flexDirection: 'row',

        marginTop: 24
    },
    calanderImg: {
        width: 50,
        height: 50,
        marginTop: 24,
    },
    calanderView: {
        marginBottom: 30,
        marginTop: 17
    },
    companyImg: {
        width: 50,
        height: 50,
        marginBottom: 10

    },
    orderMainView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: '7.4%',
        bottom: 4,

    },
    orderImg: {
        width: 53,
        height: 59,
    },
    orderTxtView: { marginTop: 20 },
    icon: {
        width: 53,
        height: 50,
    },
    image: {
        width: 20,
        height: 20,
    },
    txtInput: {
        color: '#000',
        flex: 1,
    },
    BtnTxt: {
        color: 'white',
        fontSize: 18,
        // textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: colors.primaryColor,
        marginTop: 20,
        borderRadius: 30,
        marginBottom: 20,
        elevation: 7,
    },

    drawerHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        marginBottom: 25,

    },
    drawerImageBackground: {
        height: 130,
        width: 130,
        borderRadius: 50,

    },
    drawerImageView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
    },

    mainview: {
        marginTop: -40,
        flex: 1, flexDirection: 'row',


    },

    subview: {
        width: wp('44%'),
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        borderColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
        elevation: 3,



        borderRadius: 10,
        height: 140, marginLeft: wp('4%'),

    },


    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        zIndex: 0
    },
    animatedBox: {
        flex: 1,
        backgroundColor: "#38C8EC",
        padding: 10
    },
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'pink'
    },
    cantainer: {
        height: '100%'

    },
    Bottom: {
        ...Platform.select({
            android: {
                bottom: 3
            }
        })
    },


    txt: {
        fontSize: 20,
        color: '#fff'
    },
    icon: {
        height: 20,
        width: 20,
        marginRight: 12,
        marginBottom: 30
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        height: window.height
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        zIndex: 0,
    },
    animatedBox: {
        flex: 1,
        backgroundColor: "#38C8EC",
        padding: 10
    },
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F04812'
    },
    iconColor: { color: '#3492d4' },
    headerBody: { flex: 10, alignItems: 'center', },
    headerFlexDirection: { flexDirection: 'row' },
    headerLogo: { width: '48%', height: 54 },
    logoTxt: {
        fontWeight: 'bold',
        color: "#3492d4",
        fontSize: 20,
        textAlign: 'center',
        marginTop: 5
    },
    bottom: {
        ...Platform.select({
            android: {
                bottom: 4
            }
        })
    }
});

export default styles;
