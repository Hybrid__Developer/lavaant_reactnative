import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { Left } from 'native-base';
const styles = StyleSheet.create({
    orderHeader: {
        padding: '4%',
        width: '100%',
        backgroundColor: '#199bf1', alignItems: 'center'
    },
    orderHeaderTxt: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white',
        fontWeight: 'bold',
    },
    mainView: {
        backgroundColor: 'white',
        bottom: '7%',
        padding: 20,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopStartRadius: 10,
        borderTopEndRadius: 10
    },
    subView: {
        backgroundColor: 'white',
        bottom: '7%',
        padding: 20,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',


    },
    subView2: {
        backgroundColor: 'white',
        bottom: '7%',
        padding: 20,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10
    },

    editImgView: {
        height: 34,
        width: 34,
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
        alignSelf: 'flex-end',
        bottom: 30,
        alignItems: 'center'
    },
    startTxt: {
        fontSize: 17,
        top: 4,
        fontWeight: 'bold'
    },

    textInputView: {
        borderColor: 'grey',
        borderWidth: 1,
        flexDirection: 'row',
        width: '64%',
        borderRadius: 5,
        justifyContent: 'flex-start',
        paddingLeft: 10,
        alignItems: 'flex-start',
        borderColor: '#199bf1'
    },
    textInputTxt: {
        fontSize: 19,
        paddingBottom: 1,
        height: 30,
        color: 'black'
    },
    descriptionView: {
        borderColor: 'grey',
        borderWidth: 1,
        flexDirection: 'row',
        borderRadius: 5,
        width: '64%',
        justifyContent: 'space-around',
        height: 120,
        justifyContent: 'flex-start',
        paddingLeft: 10,
        borderColor: '#199bf1'
    },
    submitBtnView: {
        width: '90%',
        marginTop: 30,
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 10,
        borderRadius: 40,
        backgroundColor: '#199bf1'
    },
    submitBtnTxt: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center'
    },
    dropDownImg2: {
        height: 10,
        width: 20,
        marginRight: 20
    },




});

export default styles;
