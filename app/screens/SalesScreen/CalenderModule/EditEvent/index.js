import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput, } from 'react-native';

import MainHeader from '../../../../Components/MainHeader'

import styles from './style';



export default class EditEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            selectedHours: 0,
            selectedMinutes: 0,
            editable: false
        };
    }



    render() {
        const { navigation } = this.props;


        const JSON_ListView_Clicked_Item = navigation.getParam('JSON_ListView_Clicked_Item');



        return (
            <View>
                <MainHeader navigate={this.props.navigation} />
                <View style={styles.orderHeader}>

                    <Text style={styles.orderHeaderTxt}>EVENT DETAIL</Text>

                    <View style={styles.editImgView}>
                        <TouchableOpacity onPress={() => this.setState({ editable: !this.state.editable })}>

                            <Image source={require('../../../../assets/pencil.png')} style={{ height: 24, width: 24 }}></Image>
                        </TouchableOpacity>

                    </View>
                </View>
                <View style={styles.mainView}>
                    <Text style={styles.startTxt}>Start :</Text>
                    <View style={styles.textInputView}>
                        <TextInput
                            editable={this.state.editable}
                            multiline={true}
                            style={styles.textInputTxt}>{JSON_ListView_Clicked_Item.start}</TextInput>
                    </View>

                </View>
                <View style={styles.subView}>
                    <Text style={styles.startTxt}>Duration :</Text>
                    <View style={styles.textInputView}>
                        <TextInput
                            editable={this.state.editable}
                            multiline={true}
                            style={styles.textInputTxt}>{JSON_ListView_Clicked_Item.duration}</TextInput>
                    </View>

                </View>
                <View style={styles.subView}>
                    <Text style={styles.startTxt}>Event Name :</Text>
                    <View style={styles.textInputView}>
                        <TextInput
                            editable={this.state.editable}
                            multiline={true}
                            style={styles.textInputTxt}>{JSON_ListView_Clicked_Item.EventName}</TextInput>
                    </View>

                </View>
                <View style={styles.subView2}>
                    <Text style={styles.startTxt}>Descripition :</Text>
                    <View style={styles.descriptionView}>
                        <TextInput
                            editable={this.state.editable}
                            style={{ height: 100, color: 'black' }}
                            multiline={true}
                            textAlignVertical="top">{JSON_ListView_Clicked_Item.Description}</TextInput>


                    </View>

                </View>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                    <View style={styles.submitBtnView}>
                        <Text style={styles.submitBtnTxt}>Submit</Text>
                    </View>
                </TouchableOpacity>

            </View >
        )
    }
}