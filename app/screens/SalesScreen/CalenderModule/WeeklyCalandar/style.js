import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { Left } from 'native-base';
const styles = StyleSheet.create({

    headerStyle: {
        backgroundColor: '#81E1B8'
    },
    container: {
        flex: 1,
        backgroundColor: '#F8F8F8',
    },
    orderHeader:
    {
        backgroundColor: '#199bf1',
        justifyContent: 'center',
        alignItems: 'center'
    },

    orderHeaderTxt: {
        color: 'white',
        fontSize: 24,
        textAlign: 'center',
        fontWeight: 'bold',
        top: 14,
        paddingLeft: '7%'
    },
    hamBurgerImg: {
        alignSelf: 'flex-end',
        bottom: '27%',
        paddingRight: '7%'
    },
    eventView: {
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: '4%',
        marginBottom: '7%'
    },
    eventTxt: {
        textAlign: 'center',
        fontWeight: 'bold'
    },
    dayLabelStyle: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 17
    },
    tittleStyle: {
        color: '#199bf1',
        fontSize: 24,
        fontWeight: 'bold',
    },

});

export default styles;