
import React, { Component } from 'react';
import {
    View, Dimensions, Alert, Text, Image, TouchableOpacity
} from 'react-native';

import { ScrollView } from 'react-native-gesture-handler';
import styles from './style'
import MainHeader from '../../../../Components/MainHeader'
import { FloatingAction } from "react-native-floating-action";
import WeeklyCalendar from 'react-native-weekly-calendar';
import moment from 'moment'

let { width } = Dimensions.get('window')


const actions = [

    {
        text: "Event",
        icon: require("../../../../assets/event.png"),
        name: "bt_videocam",
        color: '#199bf1',
        position: 4,

        textStyle: { fontWeight: 'bold', fontSize: 17 },


    }
];
export default class WeeklyCalandar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,

        };
        this.events = [
            { EventName: 'Gravity', Description: "When the value of Switch change to true by calling onValueChange the Text component is reset to 'on'.", date: '30 April', start: '2020-04-30 14:00:00', duration: '02:00:00', },
            { EventName: 'Doctor\'s appointment', Description: "When the value of Switch change to true by calling onValueChange the Text component is reset to 'on'.", date: '02 May', start: '2020-05-02 09:30:00', duration: '01:00:00', },
            { EventName: 'Morning exercise', Description: "When the value of Switch change to true by calling onValueChange the Text component is reset to 'on'.", date: '05 May', start: '2020-05-05 09:30:00', duration: '01:00:00', },
        ]
    }



    editEvent = (event) => {
        this.props.navigation.navigate('EditEvent', {
            JSON_ListView_Clicked_Item: event
        })
    }

    addEditEvent = (item) => {
        this.props.navigation.navigate('AddEvent', {
            JSON_ListView_Clicked_Item: item
        })
    }
    render() {
        return (
            <View>


                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <View style={styles.orderHeader}>
                        <Text style={styles.orderHeaderTxt}>CALANDER</Text>
                        <View style={styles.hamBurgerImg}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('CalanderScreen')}>
                                <Image source={require('../../../../assets/menu1.png')} style={{ height: 30, width: 30, }}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.container}>

                        <WeeklyCalendar events={this.events}
                            renderEvent={(event, j) => {
                                let startTime = moment(event.start).format('LT').toString()
                                let duration = event.duration.split(':')
                                let seconds = parseInt(duration[0]) * 3600 + parseInt(duration[1]) * 60 + parseInt(duration[2])
                                let endTime = moment(event.start).add(seconds, 'seconds').format('LT').toString()
                                return (
                                    <View>
                                        <TouchableOpacity onPress={this.editEvent.bind(this, event)}>
                                            <View style={styles.eventView}>

                                                <Text style={styles.eventTxt}>{event.EventName}</Text>
                                            </View>
                                        </TouchableOpacity>

                                    </View>
                                )
                            }}

                            themeColor='#199bf1'

                            dayLabelStyle={styles.dayLabelStyle}

                            titleStyle={styles.tittleStyle}
                            style={{
                                height: 599,

                            }} />





                    </View>

                </ScrollView>
                <FloatingAction
                    actions={actions}
                    position="right"
                    color="#199bf1"
                    onPressItem={this.addEditEvent}
                    distanceToEdge={59}
                />
            </View>
        )
    }
}
