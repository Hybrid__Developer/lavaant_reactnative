import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, TextInput, } from 'react-native';

import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style';
import TimePicker from "react-native-24h-timepicker";

import {


    DatePicker,


} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import DateTimePicker from "react-native-modal-datetime-picker";
import Moment from "moment";



export default class AddEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            selectedHours: 0,
            selectedMinutes: 0,
            duration: "",
            selectedHour: 0,
            selectedMinute: 0
        };
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = time => {

        let hours = time.getHours();
        let minutes = time.getMinutes();
        if (minutes < 10) {
            minutes = `0${minutes}`
        }
        let am_pm = 'AM';
        if (hours > 11) {
            am_pm = 'PM';
            if (hours > 12) {
                hours = hours - 12;
            }
            if (hours < 10) {
                hours = `0${hours}`
            }
        }
        if (hours == 0) {
            hours = 12;
        }
        const selectedTime = `${hours}:${minutes} ${am_pm}`
        this.setState({
            reqTime: Moment(time).format('LT'),
            time: selectedTime,
            isEnabled: true
        });
        this.hideDateTimePicker();

    };
    submit = () => {
        this.props.navigation.navigate('CalanderScreen')
    }

    onCancel() {
        this.TimePicker.close();
    }

    onConfirm(hour, minute) {
        this.setState({ duration: `${hour}:${minute}` });
        this.TimePicker.close();
    }
    render() {

        return (
            <View>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <SubHeader title='ADD EVENT' />
                    <View style={styles.mainView}>
                        <Text style={styles.dateTxt}>Date :</Text>
                        <View style={styles.datePickerView}>
                            <DatePicker
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Select  Date"
                                textStyle={styles.datePickerTxt}
                                placeHolderTextStyle={styles.placeHolder}
                                onDateChange={this.setDate}
                                disabled={false}
                            />
                            <Image source={require('../../../../assets/dropdown.png')} style={styles.dropDownImg2}></Image>
                        </View>

                    </View>
                    <View style={styles.subView}>
                        <Text style={styles.selectTimeTxt}>Select Time :</Text>
                        <TouchableOpacity style={[styles.datePicker,]} onPress={this.showDateTimePicker}>

                            <DateTimePicker
                                is24Hour={false}
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                                textStyle={{ color: "red", fontSize: 30, }}

                                mode='time'
                            />
                            <Text style={styles.timeTxt} >{this.state.time}</Text>

                            <Image source={require('../../../../assets/dropdown.png')} style={styles.dropDownImg2}></Image>
                        </TouchableOpacity>


                    </View>

                    <View style={styles.subView}>
                        <Text style={styles.selectTimeTxt}>Duration :</Text>
                        <TouchableOpacity style={[styles.datePicker,]} onPress={() => this.TimePicker.open()}>


                            <Text style={styles.timeTxt}>{this.state.duration}</Text>
                            <TimePicker
                                ref={ref => {
                                    this.TimePicker = ref;
                                }}
                                onCancel={() => this.onCancel()}
                                onConfirm={(hour, minute) => this.onConfirm(hour, minute)}
                            />

                            <Image source={require('../../../../assets/dropdown.png')} style={styles.dropDownImg2}></Image>
                        </TouchableOpacity>


                    </View>
                    <View style={styles.subView}>
                        <Text style={styles.selectTimeTxt}>Event Name :</Text>
                        <View style={styles.eventTxtView}>
                            <TextInput style={styles.eventTxtInput}></TextInput>
                        </View>
                    </View>
                    <View style={styles.subView2}>
                        <Text style={styles.selectTimeTxt}>Descripition :</Text>
                        <View style={styles.descriptionView}>
                            <TextInput
                                style={{ height: 100, }}
                                multiline={true}
                                textAlignVertical="top"></TextInput>
                        </View>
                    </View>

                    <TouchableOpacity onPress={this.submit}>
                        <View style={styles.submitBtnView}>
                            <Text style={styles.submitBtnTxt}>Submit</Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View >
        )
    }
}