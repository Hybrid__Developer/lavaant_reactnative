import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { Left } from 'native-base';
const styles = StyleSheet.create({
    orderHeader: {
        padding: '7%',
        width: '100%',
        backgroundColor: '#199bf1'
    },
    orderHeaderTxt: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white',
        fontWeight: 'bold', bottom: 10
    },
    dateTxt: {
        fontSize: 20,
        top: 4,
        fontWeight: 'bold'
    },
    datePickerView: {
        borderWidth: 1,
        flexDirection: 'row',
        width: '64%',
        justifyContent: 'space-between',
        height: 30,
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#199bf1'
    },
    datePickerTxt: {
        color: "black",
        fontSize: 19,
    },
    placeHolder: {
        color: "grey",
        fontSize: 14,
        textAlign: 'center',
    },
    selectTimeTxt: {
        fontSize: 17,
        top: 4,
        fontWeight: 'bold'
    },
    timeTxt: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 19,
    },
    eventTxtView: {
        borderColor: 'grey',
        borderWidth: 1,
        flexDirection: 'row',
        width: '64%',
        borderRadius: 5,
        justifyContent: 'flex-start',
        paddingLeft: 10,
        height: 30,
        alignItems: 'flex-start',
        borderColor: '#199bf1'
    },
    eventTxtInput: {
        fontSize: 16,
        paddingBottom: 1,
        height: 30
    },
    descriptionView: {
        borderColor: 'grey',
        borderWidth: 1,
        flexDirection: 'row',
        borderRadius: 5,
        width: '64%',
        justifyContent: 'space-around',
        height: 120,
        justifyContent: 'flex-start',
        paddingLeft: 10,
        borderColor: '#199bf1'
    },
    submitBtnView: {
        width: '90%',
        marginTop: 30,
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 10,
        borderRadius: 40,
        backgroundColor: '#199bf1'
    },
    submitBtnTxt: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center'
    },
    mainView: {
        backgroundColor: 'white',
        bottom: '4%',
        padding: 20,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopStartRadius: 10,
        borderTopEndRadius: 10
    },
    subView: {
        backgroundColor: 'white',
        bottom: '7%',
        padding: 20,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',


    },
    subView2: {
        backgroundColor: 'white',
        bottom: '7%',
        padding: 20,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10
    },


    dropDownImg2: {
        height: 10,
        width: 20,
        marginRight: 20
    },
    datePicker: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'grey',
        width: '64%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: '4%',
        height: 30,
        alignItems: 'center',
        borderColor: '#199bf1'

    },

});

export default styles;
