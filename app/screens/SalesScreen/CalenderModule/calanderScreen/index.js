import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Switch } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import styles from './style';
import { FloatingAction } from "react-native-floating-action";
import RNCalendarEvents from 'react-native-calendar-events';
import {


    Container,



} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { LocaleConfig, Calendar, } from 'react-native-calendars';
LocaleConfig.locales['fr'] = {
    monthNames: ['Janvier', 'Février', 'Mars', 'April', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'],
    today: 'Aujourd\'hui',
};

LocaleConfig.defaultLocale = 'fr';
const actions = [

    {
        text: "Event",
        icon: require("../../../../assets/event.png"),
        name: "bt_videocam",
        color: '#199bf1',
        position: 4,

        textStyle: { fontWeight: 'bold', fontSize: 17 },


    }
];

export default class CalanderScreen extends Component {

    constructor(props) {

        super(props);

        this.state = {
            selectedDate: ['2020-04-30', , '2020-05-02', '2020-05-05'],
            items: {},
            switchValue: false,
            render: false,
        }
        this.EventDetail = [

            { EventName: 'Gravity', Description: "When the value of Switch change to true by calling onValueChange the Text component is reset to 'on'.", date: '30 April', start: '2020-04-30 14:00:00', duration: '02:00:00', },
            { EventName: 'Doctor\'s appointment', Description: "When the value of Switch change to true by calling onValueChange the Text component is reset to 'on'.", date: '02 May', start: '2020-05-02 09:30:00', duration: '01:00:00', },
            { EventName: 'Morning exercise', Description: "When the value of Switch change to true by calling onValueChange the Text component is reset to 'on'.", date: '05 May', start: '2020-05-05 09:30:00', duration: '01:00:00', },



        ]
    }
    getMarkedDates = () => { const marked = {}; this.state.selectedDate.forEach(item => { marked[item] = { marked: true, dotColor: 'red', selected: true, disableTouchEvent: false, startingDay: true, }; }); return JSON.parse(JSON.stringify(marked)); };

    detail = (item) => {
        this.props.navigation.navigate('TaskDetail', {
            JSON_ListView_Clicked_Item: item
        })
    }
    addEditEvent = (item) => {
        this.props.navigation.navigate('EditEvent', {
            JSON_ListView_Clicked_Item: item
        })
    }
    findEvents() {
        RNCalendarEvents.fetchAllEvents(startDate, endDate, calendars);
    }
    itemsCalendar = [];
    componentDidMount() {
        RNCalendarEvents.findCalendars().then(calendars => {
            this.itemsCalendar = calendars.map((item) => (
                <View key={item.id} style={{ backgroundColor: item.color }}>
                    <Text>{item.id}</Text>
                    <Text>{item.title}</Text>
                    <Text>{item.type}</Text>
                    <Text>{item.source}</Text>
                    <Text>{item.isPrimary.toString()}</Text>
                    <Text>{item.allowsModification}</Text>
                    <Text>{item.allowedAvailabilities}</Text>
                </View>
            ))
            this.setState({ render: true });
        });
    }






    createEvent = async (respone) => {
        if (respone === 'authorized') {
            await RNCalendarEvents.authorizeEventStore();
            await RNCalendarEvents.saveEvent('Pinjam Modal Event', {
                calendarId: '141',
                startDate: '2018-06-29T19:26:00.000Z',
                endDate: '2018-06-30T19:26:00.000Z'
            });
        }
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>

                    <View style={styles.orderHeader}>

                        <Text style={styles.orderHeaderTxt}>CALENDER</Text>
                        <View style={styles.hamBurgerImg}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('WeeklyCalandar')}>
                                <Image source={require('../../../../assets/menu2.png')} style={{ height: 30, width: 30, }}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        <Calendar
                            style={styles.calenderView}
                            markingType='custom'
                            markedDates={this.getMarkedDates()}
                        />
                    </View>
                    {
                        this.EventDetail.map((item, key) => (
                            <View style={styles.mainTaskView}>
                                <View style={styles.DateView}>
                                    <Text style={styles.dateTxt}>{item.date}</Text>
                                    <TouchableOpacity onPress={this.addEditEvent.bind(this, item)}>
                                        <Image source={require('../../../../assets/pencil.png')} style={{ height: 30, width: 30 }}></Image>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.mainView}>
                                    <View style={styles.Width}>
                                        <Text style={styles.nameTxt}>Event Name :</Text>
                                    </View>
                                    <View style={{ width: '70%', }}>
                                        <Text style={styles.nameTxt}>{item.EventName}</Text>
                                    </View>
                                </View>
                                <View style={styles.mainView}>
                                    <View style={styles.Width}>
                                        <Text style={{ fontSize: 16, }}>Description :</Text>
                                    </View>
                                    <View style={{ width: '70%', }}>
                                        <Text style={styles.nameTxt2}>{item.Description}</Text>
                                    </View>
                                </View>
                                <View style={styles.mainView}>
                                    <View style={styles.Width}>
                                        <Text style={styles.nameTxt}>Reminder :</Text>
                                    </View>
                                    <Text style={styles.timeTxt}>7:30</Text>
                                    <Switch
                                        style={{ paddingLeft: 10 }}
                                        onTintColor="#199bf1"
                                        thumbColor="#199bf1"
                                        value={this.state.switchValue}
                                        onValueChange={(switchValue) => this.setState({ switchValue })} />
                                </View>
                            </View>

                        ))}
                </ScrollView>

                <FloatingAction
                    actions={actions}
                    position="right"
                    color="#199bf1"
                    onPressItem={() => this.props.navigation.navigate('AddEvent')}
                />

                <ScrollView style={{ flex: 1 }}>
                    {this.itemsCalendar}
                </ScrollView>
            </Container >
        )
    }
}







