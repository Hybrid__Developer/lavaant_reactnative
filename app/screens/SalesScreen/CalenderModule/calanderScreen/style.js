import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
const styles = StyleSheet.create({
    orderHeader: {

        width: '100%',
        backgroundColor: '#199bf1',

        paddingLeft: '10%',


    },
    orderHeaderTxt: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white',
        padding: '4%',
        fontWeight: 'bold',
    },

    calenderView: {
        backgroundColor: 'white',
        bottom: '7%',
        padding: 20,
        borderRadius: 10,
        marginLeft: '4%',
        marginRight: '4%',
        elevation: 10
    },
    hamBurgerImg: { alignSelf: 'flex-end', bottom: '44%', paddingRight: '7%' },
    mainTaskView: {
        paddingTop: '9%',
        paddingBottom: 30,
        borderRadius: 10,
        width: '97%',
        alignSelf: 'center',
        elevation: 10,
        marginBottom: 10,
        paddingLeft: '5%',
        paddingRight: '5%',

    },
    DateView: { flexDirection: 'row', justifyContent: 'space-between', top: 4 },
    subViewTask: {
        flexDirection: 'row',
        borderRadius: 18,
        elevation: 4,
        paddingLeft: 10,
        paddingTop: 10,
        marginRight: 10,
        marginTop: 10
    },
    width: { width: '20%', },
    taskIcon: {
        height: 49,
        width: 37,
    },
    mainTxtView: {
        width: '84%',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    subTxtView: {
        width: '50%',
        flexDirection: 'row'
    },
    widthView: { width: '90%', },
    catogoryTxt: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    taskCatogoryTxt: {
        fontSize: 17,
        color: '#199bf1'
    },
    dateTxtView: {
        width: '50%',
        flexDirection: 'row',
        marginTop: 10
    },
    widthView2: { width: '90%', },
    dateTxt: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#199bf1'
    },
    mainView: { flexDirection: 'row', marginTop: 20, },
    Width: { width: '34%', },
    nameTxt: { fontSize: 15, },
    nameTxt2: { fontSize: 14, },
    timeTxt: { fontSize: 20, fontWeight: 'bold', paddingLeft: 10 },






});

export default styles;
