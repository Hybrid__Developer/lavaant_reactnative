import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { Left } from 'native-base';
const styles = StyleSheet.create({
    mainView: {
        width: '90%',
        backgroundColor: 'white',
        padding: 20,
        alignSelf: 'center',
    },
    headingTxt: {
        textAlign: 'center',
        color: '#199bf1',
        fontSize: 24,
        textDecorationLine: 'underline',
        fontWeight: 'bold'
    },
    subView: {
        width: '99%',
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 18,
        elevation: 4,
        padding: 10,
        marginTop: 20
    },
    leftTxtView: {
        flexDirection: 'row',
        marginBottom: 10
    },
    width: { width: '49%', },
    leftTxt: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    buttonView: {
        width: '90%',
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 10,
        backgroundColor: '#199bf1',
        marginTop: 20,
        borderRadius: 30,
        alignItems: 'center'
    },
    buttonTxt: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },
});

export default styles;