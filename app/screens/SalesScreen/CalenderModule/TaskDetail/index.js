import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, TouchableOpacity, TextInput } from 'react-native';
import Modal from 'react-native-modal';
import styles from './style';
export default class TaskDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            agentCode: '',
        };
    }

    navigate = async () => {
        this.props.navigation.navigate('CalanderScreen')
    }
    render() {
        const { navigation } = this.props;


        const JSON_ListView_Clicked_Item = navigation.getParam('JSON_ListView_Clicked_Item');
        return (
            <View>

                <Modal backdropOpacity={0.80} backdropColor="black" isVisible={true}
                    hasBackdrop={true}

                >
                    <View style={styles.mainView}>
                        <Text style={styles.headingTxt}>
                            Task Detail
                       </Text>


                        <View style={styles.subView}>
                            <View style={styles.leftTxtView}>
                                <View style={styles.width}>
                                    <Text style={styles.leftTxt}>Task</Text>
                                </View>
                                <Text>:  </Text>
                                <View style={styles.width}>
                                    <Text>{JSON_ListView_Clicked_Item.task}</Text>
                                </View>
                            </View>
                            <View style={styles.leftTxtView}>
                                <View style={styles.width}>
                                    <Text style={styles.leftTxt}>Task Catogory</Text>
                                </View>
                                <Text>:  </Text>
                                <View style={styles.width}>
                                    <Text>{JSON_ListView_Clicked_Item.catogory}</Text>
                                </View>
                            </View>
                            <View style={styles.leftTxtView}>
                                <View style={styles.width}>
                                    <Text style={styles.leftTxt}>Task Status</Text>
                                </View>
                                <Text>:  </Text>
                                <View style={styles.width}>
                                    <Text>Pending</Text>
                                </View>
                            </View>

                            <View style={styles.leftTxtView}>
                                <View style={styles.width}>
                                    <Text style={styles.leftTxt}>Task Date</Text>

                                </View>
                                <Text>:  </Text>
                                <View style={styles.width}>
                                    <Text>{JSON_ListView_Clicked_Item.date}</Text>
                                </View>
                            </View>


                        </View>
                        <TouchableOpacity onPress={this.navigate}>
                            <View style={styles.buttonView}>
                                <Text style={styles.buttonTxt}>OK</Text>
                            </View>

                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        )
    }
}


