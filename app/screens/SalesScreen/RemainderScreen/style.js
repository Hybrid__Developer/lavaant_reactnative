import {Dimensions, StyleSheet} from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../constants/colors';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  logo: {
    width: 200,
    height: 180,

  },
  logoContainer: {
    alignItems: 'center',
    marginTop: '20%',
  },
  loginTxtInput: {
    // backgroundColor: 'transparent',
    color: 'white',
    flex: 1,
  },
  SectionStyle: {
    elevation: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageStyle: {
    padding: 5,
    // margin: 5,
    color: '#fff',
  },
  LoginBtn: {
    height: 50,
    backgroundColor: colors.primaryColor,
    marginTop: 10,
    borderRadius: 30,
    borderColor: colors.primaryColor,
    elevation: 10,
  },
  AJ: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '2.5%',
  },
  LoginBtnTxt: {
    color: '#fff',
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
  },

  LoginBtn2: {
    width:300,
    height: 50,
    backgroundColor: colors.primaryColor,
    marginTop: 100,
    
    borderRadius: 30,
    elevation: 10,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    
  },
  LoginBtnTxt2: {
    
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
   // paddingRight:'5%'
  },
});




export default styles;