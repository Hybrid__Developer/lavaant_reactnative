import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert } from 'react-native';
import MainHeader from '../../../../Components/MainHeader';
import Subheader from '../../../../Components/SubHeader';
import * as Utility from '../../../../utility/index';
import { NavigationActions, StackActions } from 'react-navigation';
import styles from './style'

import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { Route } from 'react-router-dom';

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year
export default class InPerson extends Component {


    goToShipment = async () => {
        let route = await Utility.getFromLocalStorge('route')
        if (route == "Phone") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'ShipToPh' })]
                })
            );
            this.props.navigation.navigate('ShipToPh')

        }
        else if (route == "InPerson") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'ShipTo' })]
                })
            );
            this.props.navigation.navigate('ShipTo')
        }
    }
    goToSelectCustomer = async () => {
        let route = await Utility.getFromLocalStorge('route')
        if (route == "Phone") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'CustomerPh' })]
                })
            );
            this.props.navigation.navigate('CustomerPh')
        }
        else if (route == "InPerson") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'SelectCustomers' })]
                })
            );
            this.props.navigation.navigate('SelectCustomers')
        }
    }
    goToTaskReminder = async () => {
        let route = await Utility.getFromLocalStorge('route')
        if (route == "Phone") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'AreaPh' })]
                })
            );
            this.props.navigation.navigate('AreaPh')
        }
        else if (route == "InPerson") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Area' })]
                })
            );
            this.props.navigation.navigate('Area')

        }
    }
    goToSelectCompany = async () => {
        let route = await Utility.getFromLocalStorge('route')
        if (route == "Phone") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'CompanyPh' })]
                })
            );
            this.props.navigation.navigate('CompanyPh')
        }
        else if (route == "InPerson") {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'SelectCompany' })]
                })
            );
            this.props.navigation.navigate('SelectCompany')
        }
    }

    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Subheader
                        title={month + ' ' + date + ', ' + year}>

                    </Subheader>


                    <View style={styles.BtnMainView}>
                        <View style={styles.BtnSubView}>
                            <TouchableOpacity onPress={() => this.goToShipment()}>
                                <Image source={require('../../../../assets/add.png')} style={styles.AddImg} ></Image>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.goToSelectCustomer()}>
                                <Image source={require('../../../../assets/add.png')} style={styles.AddImg}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.BtnTxtView}>
                            <Text style={styles.BtnTxt}>SHIP-TO</Text>
                            <Text style={styles.BtnTxt}>CUSTOMER</Text>

                        </View>
                    </View>
                    <View style={styles.BtnMainView}>
                        <View style={styles.BtnSubView}>
                            <TouchableOpacity onPress={() => this.goToTaskReminder()}>
                                <Image source={require('../../../../assets/add.png')} style={styles.AddImg}></Image>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.goToSelectCompany()}>
                                <Image source={require('../../../../assets/add.png')} style={styles.AddImg}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.BtnTxtView}>
                            <Text style={styles.BtnTxt}>AREA</Text>
                            <Text style={styles.BtnTxt}>COMPANY</Text>

                        </View>
                    </View>

                </ScrollView>

            </Container>

        );

    }
}
