import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'
import * as Utility from '../../../../utility/index';


import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class SelectCustomers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, name: 'John', age: 18, useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: true },
                { id: 2, name: 'Lilli', age: 23, useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },
                { id: 3, name: 'Lavera', age: 46, useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },


            ],
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
            isModalVisible: false,
            checked: false,
            Qoutes: "",
            search: "",
            CustomerList: [],
            id: "",
            selected: "customer"
        }

    }
    onPressCheckbox(item) {
        const checkedCheckBox = this.state.data.find((cb) => cb.id === item.id);
        checkedCheckBox.checked = !checkedCheckBox.checked;
        console.log('checked checkbox :: ', checkedCheckBox);

        let shipings = this.state.data.map((shiping) => {
            return shiping.id === checkedCheckBox.id ? checkedCheckBox : shiping
        });
        console.log('categories after check box checked', shipings);
        this.setState({
            shipingdataDetails: shipings
        })
    }

    componentDidMount() {

        Utility.setInLocalStorge('route', this.state.selected)
        console.log("aaaaaa:::", this.state.selected)
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <View style={styles.subHeaderView}>
                        <Text style={styles.currentDateTxt}>{month + ' ' + date + ', ' + year}</Text>
                        <Text style={styles.leftTitle}>SELECT CUSTOMER:</Text>
                    </View>
                    <View style={styles.mainSearchView}>
                        <Card>
                            <View style={styles.searchView} >
                                <TextInput
                                    style={styles.searchTextInput}
                                    placeholder=" Search by Name or Email"
                                    placeholderTextColor="grey"
                                    onChangeText={(search) => this.setState({ search })}
                                />
                                <TouchableOpacity>
                                    <Image
                                        source={require('../../../../assets/search.png')}
                                        style={styles.searchIcon}
                                    />
                                </TouchableOpacity>


                            </View>

                        </Card>

                    </View>
                    {

                        this.state.data.map((item, key) => (
                            <TouchableOpacity >


                                <View style={styles.mainView}>

                                    <Image
                                        style={styles.userImg}
                                        resizeMode='cover'
                                        source={{ uri: this.state.pic }}

                                    />
                                    <Image
                                        style={styles.userImgBackgroung}
                                        resizeMode='cover'
                                        source={require('../../../../assets/image_view1.png')}
                                    />

                                    <View style={styles.subView}>
                                        <Text style={styles.nameTxt}>{item.name}</Text>
                                        <Text style={styles.adressTxt}>{item.useradress}</Text>

                                    </View>
                                    <View style={styles.checkBoxView}>
                                        <CheckBox style={styles.checkBox}
                                            checked={item.checked}
                                            onPress={() => this.onPressCheckbox(item)}
                                        />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                    <View style={styles.btnView}>
                        <TouchableOpacity
                            style={[styles.Btn, styles.shadow]}
                            onPress={() => this.props.navigation.navigate("InPersonVisit")}
                        // onPress={() => this.goToShipment()}
                        >
                            <Text style={styles.BtnTxt2}>SELECT</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

            </Container>

        );

    }
}
