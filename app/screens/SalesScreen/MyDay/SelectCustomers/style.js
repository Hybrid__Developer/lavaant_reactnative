import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../utility/index';
const styles = StyleSheet.create({

    subHeaderView: {
        paddingTop: '8%',
        paddingLeft: '4%',

        width: '100%',
        backgroundColor: '#199bf1',
        paddingBottom: 10
    },
    currentDateTxt: {
        fontSize: 20,
        color: 'white',
        fontWeight: "bold",
        textAlign: 'center', bottom: 20
    },
    leftTitle: {
        fontSize: 17,
        color: 'white',
        fontWeight: "bold",
        marginTop: 4,
        bottom: 20
    },
    QoutesView: {
        width: '90%',
        padding: 7,
        alignSelf: 'center',
        borderColor: 'grey',
        backgroundColor: 'white',
        borderWidth: 1,
        bottom: 20


    },
    mainSearchView: {
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row',
        bottom: 20
    },
    QoutesTxt: {
        marginLeft: 10,
        fontSize: 20,
        fontWeight: 'bold'
    },
    QoutesTxt2: {
        marginLeft: 14,
        fontSize: 14,
    },
    searchView: {
        flexDirection: 'row',
        width: '100%',
        height: 35,

    },
    searchTextInput: {
        width: '87%',
        height: 35,
        marginLeft: 5
    },
    searchIcon: {
        color: 'white',
        flex: 1,
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: '1%',
    },
    filterView: {
        color: 'white',
    },
    filterIcon: {
        color: 'white',
        flex: 1,
        width: 40,
        height: 40,
        resizeMode: 'contain',

    },
    userImg: {
        left: 2,
        width: 50,
        height: 51,
        backgroundColor: 'white',
        borderRadius: 100,
    },
    userImgBackgroung: {
        width: 54,
        height: 54,
        position: 'absolute',
        marginLeft: wp('4%'),
        top: hp("2%")

    },
    BtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: colors.primaryColor,
        borderRadius: 10,
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
    btnView: { marginBottom: '5%', marginTop: '13%' },
    mainView: {
        justifyContent: 'space-between',
        bottom: 4,
        padding: '4%', marginTop: 10,
        width: '90%', alignSelf: 'center',
        flexDirection: 'row',
        // borderWidth: 1,
        // borderRadius: 10,
        // borderColor: '#ddd',
        // borderBottomWidth: 2,
        // shadowColor: '#000000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.9,
        // shadowRadius: 10,
        borderRadius: 3,
        elevation: 2
    },
    subView: {
        flexDirection: 'column',
        width: '70%',
        marginLeft: '4%'
    },
    nameTxt: { fontSize: 17, },
    adressTxt: { fontSize: 14, },
    editImg: {
        height: 24,
        width: 24
    },
    areaView: {
        borderLeftWidth: 1,
        borderLeftColor: 'gray',
        width: '25%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkBox: { borderRadius: 1, },
    checkBoxView: { alignItems: "center", paddingRight: 10, justifyContent: "center", paddingBottom: 10 }
})

export default styles;