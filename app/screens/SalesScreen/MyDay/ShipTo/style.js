import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../utility/index';

const styles = StyleSheet.create({
    checkBox: { borderRadius: 1, },
    mainHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        height: 30
    },
    nameView: {
        width: '50%',
        justifyContent: 'center',
        paddingLeft: '2%'
    },
    areaView: {
        borderLeftWidth: 1,
        borderLeftColor: 'gray',
        width: '25%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnView: {
        marginBottom: '3%',
        marginTop: hp('30%'),
        flex: 1,
        // justifyContent: 'flex-end',
        marginBottom: '2%',
        // flexDirection: 'column'
    },
    BtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: colors.primaryColor,
        borderRadius: 10,
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
});

export default styles;