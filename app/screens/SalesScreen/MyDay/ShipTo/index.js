import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ScrollView, Switch } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import * as Utility from '../../../../utility/index';
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import Loader from '../../../../Components/Loader';
import styles from './style'
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class ShipTo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            selected: "ShipTo",
            CustomerList: [],
            data: [],
            checkedCheckBox: [],
            id: "",

        }
    }

    onPressCheckbox(item, index) {
        let checkedCheckBox = []
        checkedCheckBox = this.state.CustomerList.find((cb) => cb.id === item.id);
        checkedCheckBox.checked = !checkedCheckBox.checked;
        console.log('checked checkbox :: ', checkedCheckBox);

        let shipings = this.state.CustomerList.map((shiping) => {
            return shiping.id === checkedCheckBox.id ? checkedCheckBox : shiping
        });
        console.log('categories after check box checked', shipings.id);
        this.setState({
            CustomerList: shipings
        })
    }

    goToInPersonVisit = async (favourites) => {
        // let body = []
        // if (favourites && favourites.length) {
        //     favourites.forEach(item => {
        //         if (item.isShow) {
        //             body.push({ id: item.id })
        //         }
        //     });
        //     console.log('updateFavouriteSubCategories::favouriteSubCategories::body', body)
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPersonVisit' })]
            })
        );
        this.props.navigation.navigate('InPersonVisit')
    }
    componentDidMount() {

        Utility.setInLocalStorge('route', this.state.selected)
        console.log("aaaaaa:::", this.state.selected)
        this.getCustomer()
    }
    getCustomer = async () => {

        await this.setState({
            isLoading: true
        })
        const res = await Service.get(Url.GET_USERLIST_URL + `pageNo=${1}&pageSize=${10}&role=customer`, "")
        console.log(res)
        await this.setState({
            isLoading: false
        })

        this.setState({
            CustomerList: res.items,

        })
    }
    checkBoxPress = async (item) => {
        let token = await Utility.getFromLocalStorge('token')
        this.setState({

            isShow: !item.customerSelected,
            id: item.id
        })
        console.log("sahiii;;;", this.state.isShow)
        await this.setState({
            isLoading: true
        })
        let body = {
            customerSelected: this.state.isShow,
        }
        console.log("body", body)
        const res = await Service.put(Url.SELECTED_CUSTOMER_URL + this.state.id, token, body)
        await this.setState({
            isLoading: false
        })

        this.getCustomer()
    }
    // }
    // onCheckBoxPress = async (value, key) => {
    //     console.log('onCheckBoxPress.....', key)
    //     let a = false
    //     this.state.shipingDetails.forEach(item => {
    //         if (item.id == value.id) {
    //             if (item.isShow) {
    //                 this.state.noOfProductCategoriesSelected = this.state.noOfProductCategoriesSelected - 1
    //                 console.log('minus', this.state.noOfProductCategoriesSelected)
    //                 item.isShow = false;
    //             }
    //             else {
    //                 a = true
    //                 this.state.noOfProductCategoriesSelected = this.state.noOfProductCategoriesSelected + 1
    //                 console.log('add', this.state.noOfProductCategoriesSelected)
    //                 if (this.state.noOfProductCategoriesSelected + 1 > 6) {
    //                     alert("you cann't select more than 6 categories")
    //                     return
    //                 }
    //                 return item.isShow = true;
    //             }
    //         }
    //     });
    //     await this.setState({
    //         shipingDetails: this.state.shipingDetails,
    //         noOfProductCategoriesSelected: this.state.noOfProductCategoriesSelected
    //     });
    // }

    render() {
        const lastRowIndex = this.state.CustomerList.length - 1;
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Subheader title={month + ' ' + date + ', ' + year}
                        leftTitle="SELECT SHIP-TO"
                    ></Subheader>
                    <Loader isLoading={this.state.isLoading} />
                    <View style={{ padding: '6%', }}>


                        <View style={{ borderRadius: 5, borderColor: 'gray', borderWidth: 2 }}>
                            <View style={styles.mainHeading}>
                                <View style={styles.nameView}>
                                    <Text style={{ fontWeight: 'bold' }}>NAME</Text>
                                </View>
                                <View style={styles.areaView}>
                                    <Text style={{ fontWeight: 'bold' }}>AREA</Text>
                                </View>
                                <View style={styles.areaView}>
                                    <Text style={{ fontWeight: 'bold' }}>SELECT</Text>
                                </View>

                            </View>

                            {/* Loop start */}

                            {this.state.CustomerList.map((item, index, key) =>
                                <View style={{
                                    borderBottomLeftRadius: (lastRowIndex === index) ? 3 : 0, borderBottomRightRadius: (lastRowIndex === index) ? 3 : 0,
                                    flexDirection: 'row', justifyContent: 'space-between', height: 30, backgroundColor: index % 2 === 0 ? '#cacaca' : '#f0f0f0'
                                }}>
                                    <View style={styles.nameView}>
                                        <Text>{item.firstName} {item.lastName}</Text>
                                    </View>
                                    <View style={styles.areaView}>
                                        <Text style={{ fontWeight: 'bold' }}>NY</Text>
                                    </View>
                                    <View style={styles.areaView}>
                                        <CheckBox style={styles.checkBox}
                                            checked={item.customerSelected}
                                            onPress={() => { this.checkBoxPress(item) }}
                                        />
                                    </View>

                                </View>
                            )}
                            {/* Loop  ends  */}

                        </View>
                        <View style={styles.btnView}>
                            <TouchableOpacity
                                style={[styles.Btn, styles.shadow]}
                                onPress={() => this.goToInPersonVisit(this.state.shipingDetails)}
                            // onPress={() => this.goToShipment()}
                            >
                                <Text style={styles.BtnTxt2}>SELECT</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </Container>
        );
    }
}