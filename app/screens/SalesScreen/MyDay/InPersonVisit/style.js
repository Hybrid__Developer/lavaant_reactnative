import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({
    mainHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        height: 30
    },
    nameView: {
        width: '35%',
        justifyContent: 'center',
        paddingLeft: '2%'
    },
    areaView: {
        borderLeftWidth: 1,
        borderLeftColor: 'gray',
        width: '25%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectMainView: {
        height: 30,
        width: '40%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: '2%',
        paddingLeft: '2%'
    },
    selectRightView: {
        height: 30,
        width: '70%',
        borderLeftWidth: 1,
        borderLeftColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectRightText: {
        padding: 3,
        backgroundColor: "#6f6f6f",
        borderRadius: 3,
        fontWeight: 'bold',
        fontSize: 12,
        color: '#fff',
        textAlign: 'center',
    },

});
export default styles;