import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ScrollView } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import Loader from '../../../../Components/Loader';
import { NavigationActions, StackActions } from 'react-navigation';
import styles from './style'
import * as Utility from '../../../../utility/index';
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year
export default class InPersonVisit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            CustomerList: [],

        }
    }

    goToShipmentInfo = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ShipToInfo' })]
            })
        );
        this.props.navigation.navigate('ShipToInfo')
    }
    componentDidMount() {

        this.getCustomer()
    }
    getCustomer = async () => {

        await this.setState({
            isLoading: true
        })
        const res = await Service.get(Url.GET_USERLIST_URL + `pageNo=${1}&pageSize=${10}&role=customer`, "")
        console.log(res)
        await this.setState({
            isLoading: false
        })

        this.setState({
            CustomerList: res.items,

        })
    }
    goTOAddNotes = (item) => {
        Utility.setInLocalStorge('selectedId', item.id)
        console.log("selected id", item.id)
        this.props.navigation.navigate('AddNotes'
        )
    }
    render() {
        const lastRowIndex = this.state.CustomerList.length - 1;
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Subheader
                        title={month + ' ' + date + ', ' + year}
                        leftTitle="IN-PERSON VISIT:"
                    />
                    <Loader isLoading={this.state.isLoading} />
                    <View style={{ padding: '6%', }}>
                        <View style={{ borderRadius: 5, borderColor: 'gray', borderWidth: 2 }}>
                            <View style={styles.mainHeading}>
                                <View style={styles.nameView}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>IN-PERSON VISIT</Text>
                                </View>
                                <View style={styles.areaView}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>ADDRESS</Text>
                                </View>
                                <View style={[styles.areaView, { width: '40%' }]}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>SETTLE</Text>
                                </View>

                            </View>
                            {/* loop start */}
                            {this.state.CustomerList.map((item, index) =>
                                <View>
                                    {item.customerSelected === true ?

                                        <TouchableOpacity onPress={() => this.goToShipmentInfo()} style={{ borderBottomLeftRadius: (lastRowIndex === index) ? 3 : 0, borderBottomRightRadius: (lastRowIndex === index) ? 3 : 0, flexDirection: 'row', justifyContent: 'space-between', height: 30, backgroundColor: index % 2 === 0 ? '#cacaca' : '#f0f0f0' }}>

                                            <View style={styles.nameView} >
                                                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{item.firstName} {item.lastName}</Text>
                                            </View>
                                            <View style={styles.areaView}>
                                                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>NY</Text>
                                            </View>

                                            <View style={[styles.areaView, styles.selectMainView]}>
                                                <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>Y</Text>
                                                </View>

                                                <TouchableOpacity style={styles.selectRightView} onPress={() => this.goTOAddNotes(item)}>
                                                    <Text style={styles.selectRightText}>W/Record</Text>
                                                </TouchableOpacity>

                                            </View>
                                        </TouchableOpacity>
                                        : null}

                                </View>

                            )}
                            {/* Loop  ends  */}
                        </View>
                        {/* <View style={styles.btnView}>
                            <TouchableOpacity
                                style={[styles.Btn, styles.shadow]}
                            // onPress={() => this.goToInPersonVisit()}
                            >
                                <Text style={styles.BtnTxt2}>SUBMIT</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>
                </ScrollView>
            </Container>
        );
    }

}