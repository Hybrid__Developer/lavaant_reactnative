import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../utility/index';
const styles = StyleSheet.create({


    mainSearchView: {
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row',
        bottom: 20,

    },

    searchView: {
        flexDirection: 'row',
        width: '100%',
        height: 35,
        elevation: 2

    },
    searchTextInput: {
        width: '87%',
        height: 35,
        marginLeft: 5
    },
    searchIcon: {
        color: 'white',
        flex: 1,
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: '1%',
    },
    filterView: {
        color: 'white',
    },
    filterIcon: {
        color: 'white',
        flex: 1,
        width: 40,
        height: 40,
        resizeMode: 'contain',

    },
    userImg: {
        left: 2,
        width: 48,
        height: 49,
        backgroundColor: 'white',
        borderRadius: 100,
    },
    userImgBackgroung: {
        width: 52,
        height: 52,
        position: 'absolute',
        marginLeft: wp('4%'),
        top: hp('2%'),

    },
    BtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    callImg: {
        height: 40, width: 40
    },

    mesgImg: {
        height: 40, width: 40, marginLeft: 12
    },
    btnView: { marginBottom: '5%', marginTop: '13%' },
    mainView: {
        justifyContent: 'space-between',
        bottom: 4,
        padding: '4%', marginTop: 10,
        width: '90%', alignSelf: 'center',
        flexDirection: 'row',

        // borderWidth: 1,
        // borderRadius: 10,
        // borderColor: '#ddd',
        // borderBottomWidth: 2,
        // borderTopWidth: 1,
        // shadowColor: '#000000',
        // shadowOffset: { width: 4, height: 4 },
        // shadowOpacity: 0.20,
        // shadowRadius: 20,
        borderRadius: 3,
        elevation: 2
    },
    subView: {
        flexDirection: 'column',
        width: '50%',
        marginLeft: '4%'
    },
    nameTxt: { fontSize: 15, },
    adressTxt: { fontSize: 12, },
    editImg: {
        height: 24,
        width: 24
    },
    areaView: {
        borderLeftWidth: 1,
        borderLeftColor: 'gray',
        width: '25%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkBox: { borderRadius: 1, },
    checkBoxView: {
        alignItems: "center", flexDirection: "row",
        marginRight: 20,
        justifyContent: "center",
        paddingBottom: 16,
    }
})

export default styles;