import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ImageBackground } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import styles from './style'

import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year


export default class AddNotes extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    goToNotes = () => {
        this.props.navigation.navigate("Notes", {
        })
    }

    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Subheader
                        title={month + ' ' + date + ', ' + year}
                    ></Subheader>
                    <View style={{ marginTop: 17 }}>
                        <View style={styles.mainView}>

                            <Text style={styles.addTxt}>ADD NOTES</Text>
                            <TouchableOpacity onPress={() => this.goToNotes(this.state.id)}>

                                <Image source={require("../../../../assets/add.png")} style={styles.imgView}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.mainView}>
                            <Text style={styles.addTxt2}>ADD TASK/REMINDER</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("TaskReminder")}>

                                <Image source={require("../../../../assets/add.png")} style={styles.imgView}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.mainView}>
                            <Text style={styles.addTxt}>ADD CONTACT</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("AddContact")}>
                                <Image source={require("../../../../assets/add.png")} style={styles.imgView}>
                                </Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>

            </Container >

        );

    }
}
