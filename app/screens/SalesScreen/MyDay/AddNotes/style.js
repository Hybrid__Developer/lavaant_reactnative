import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({



    mainView: {
        flexDirection: "row",
        justifyContent: "space-around",
        width: '90%',
        alignSelf: 'center',


        marginTop: 14,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#ddd',
        borderBottomWidth: 2,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 10,
        height: 49,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: '4%',
        paddingRight: '4%',
    },
    addTxt: { fontSize: 20 },
    addTxt2: { fontSize: 20, },
    imgView: { height: 30, width: 30, borderRadius: 7 }
});

export default styles;