import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ImageBackground } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'
import * as Utility from '../../../../utility/index';
import * as Service from '../../../../api/services'
import * as Url from '../../../../constants/urls'
import { NavigationActions, StackActions } from 'react-navigation';
import Loader from '../../../../Components/Loader';
import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class Notes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notes: "",
        }

    }


    addNotes = async () => {

        let selectedId = await Utility.getFromLocalStorge('selectedId')
        console.log("selectedId", selectedId)
        let token = await Utility.getFromLocalStorge('token')
        let userId = await Utility.getFromLocalStorge('userId')
        console.log("alert mesge :::::", token)
        if (Utility.isFieldEmpty(this.state.notes
        )) {
            Alert.alert("", "Please enter Notes Field")
            return
        }

        else {
            let body = {
                recordNotes: this.state.notes,
                customerId: selectedId,
                userId: userId
            }
            await this.setState({
                isLoading: true
            })
            const res = await Service.post(Url.ADD_NOTES, token, body)
            console.log("alert mesge :::::", res.message)
            await this.setState({
                isLoading: false
            })
            if (res.message == "Notes Added Successfully") {
                Alert.alert(
                    '',
                    res.message,
                    [


                        { text: 'OK', onPress: () => this.goToPersonVisit() },
                    ],
                    { cancelable: false },
                );
            }
            else {
                Alert.alert(responsee.message)
            }


        }
        // this.props.navigation.navigate("InPersonVisit")
    }

    goToPersonVisit = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPersonVisit' })]
            })
        );
        this.props.navigation.navigate('InPersonVisit')
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <SubHeader title="ADD NOTES"></SubHeader>
                    <View style={styles.searchView}>
                        <TextInput
                            style={styles.searchTextInput}
                            placeholder=" Add Note.. "
                            placeholderTextColor="grey"
                            multiline={true}
                            onChangeText={(notes) => this.setState({ notes })}
                        />
                    </View>
                    <View style={styles.btnView}>
                        <TouchableOpacity
                            style={[styles.Btn, styles.shadow]}
                            onPress={() => this.addNotes()}
                        >
                            <Text style={styles.BtnTxt2}>SUBMIT</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Container>
        );

    }
}
