import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({


    searchView: {
        width: '80%',
        alignSelf: "center",
        elevation: 2,
        borderRadius: 3,
        marginTop: 30,
        height: 190
    },
    searchTextInput: {
        width: '94%',
        marginLeft: 5,
    },

    BtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '80%',
        alignSelf: 'center',
        backgroundColor: colors.primaryColor,
        borderRadius: 10,
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
    btnView: { marginBottom: '5%', marginTop: '13%' },


})

export default styles;