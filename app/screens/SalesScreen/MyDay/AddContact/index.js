import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ImageBackground } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'
import * as Utility from '../../../../utility/index';


import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import * as Service from '../../../../api/services'
import * as Url from '../../../../constants/urls'
import { NavigationActions, StackActions } from 'react-navigation';
import Loader from '../../../../Components/Loader';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class AddContact extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            Phone: "",
            lastName: ""
        }

    }



    addContact = async (value) => {
        let selectedId = await Utility.getFromLocalStorge('selectedId')
        console.log("selectedId", selectedId)
        let token = await Utility.getFromLocalStorge('token')
        let userId = await Utility.getFromLocalStorge('userId')
        console.log("alert mesge :::::", token)
        if (Utility.isFieldEmpty(this.state.firstName && this.state.lastName && this.state.Phone
        )) {
            Alert.alert("", "Please enter All Field")
            return
        }

        else {
            let body = {
                wRecord: false,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                phoneNo: this.state.Phone,
                customerId: selectedId,
                userId: userId
            }
            await this.setState({
                isLoading: true
            })
            const res = await Service.put(Url.ADD_CONTACT_INPERSON, token, body)
            console.log("alert mesge :::::", res.message)
            await this.setState({
                isLoading: false
            })
            if (res.message == "Contact Added Successfully") {
                Alert.alert(
                    '',
                    res.message,
                    [


                        { text: 'OK', onPress: () => this.goToPersonVisit() },
                    ],
                    { cancelable: false },
                );
            }
            else {
                Alert.alert(responsee.message)
            }


        }
    }
    goToPersonVisit = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPersonVisit' })]
            })
        );
        this.props.navigation.navigate('InPersonVisit')
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <SubHeader title="ADD CONTACT"></SubHeader>
                    <View style={styles.mainView}>
                        <View style={styles.input}>
                            <Image
                                source={require('../../../../assets/userGrey.png')}
                                style={styles.icon}
                            />
                            <View style={styles.inputViewStyle}>
                                <TextInput
                                    require
                                    placeholder="First Name"
                                    placeholderTextColor="#000"
                                    onChangeText={(firstName) => this.setState({ firstName })}
                                    value={this.state.firstName}
                                    style={styles.txtInput}
                                />
                            </View>
                        </View>
                        <View style={styles.input}>
                            <Image
                                source={require('../../../../assets/userGrey.png')}
                                style={styles.icon}
                            />
                            <View style={styles.inputViewStyle}>
                                <TextInput
                                    require
                                    placeholder="Last Name"
                                    placeholderTextColor="#000"
                                    onChangeText={(lastName) => this.setState({ lastName })}
                                    value={this.state.lastName}
                                    style={styles.txtInput}
                                />
                            </View>
                        </View>
                        <View style={styles.input}>
                            <Image
                                source={require('../../../../assets/phone_number.png')}
                                style={styles.icon}
                            />
                            <View style={styles.inputViewStyle}>
                                <TextInput
                                    require
                                    placeholder="Phone No."
                                    placeholderTextColor="#000"
                                    keyboardType={"numeric"}
                                    onChangeText={(Phone) => this.setState({ Phone })}
                                    value={this.state.Phone}
                                    style={styles.txtInput}
                                />
                            </View>
                        </View>
                    </View>


                    <View style={styles.btnView}>
                        <TouchableOpacity
                            style={[styles.Btn, styles.shadow]}

                            onPress={() => this.addContact()}
                        >
                            <Text style={styles.BtnTxt2}>SAVE</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

            </Container>

        );

    }
}
