import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({


    input: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 8
    },
    txtInput: {
        color: '#000',
        flex: 1,
    },
    inputViewStyle: {
        flex: 1,
        borderBottomColor: '#000',
        borderBottomWidth: 1,
        height: 40,
    },
    icon: {
        width: 20,
        height: 20,
        marginRight: '5%',
    },
    mainView: { paddingLeft: '6.5%', paddingRight: '6.5%', marginTop: 24 },
    BtnTxt2: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '70%',
        alignSelf: 'center',
        backgroundColor: colors.primaryColor,
        borderRadius: 10,
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
    btnView: { marginBottom: '5%', marginTop: '40%' },


})

export default styles;