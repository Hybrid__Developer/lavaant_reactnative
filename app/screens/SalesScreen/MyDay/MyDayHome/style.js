import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({

    subHeaderView: {
        paddingTop: '8%',
        paddingLeft: '4%',
        paddingBottom: '9%',
        width: '100%',
        backgroundColor: '#199bf1'
    },
    currentDateTxt: {
        fontSize: 30,
        color: 'white',
        fontWeight: '900',
        textAlign: 'center'
    },
    QoutesView: {
        width: '90%',
        padding: 7,
        alignSelf: 'center',
        borderColor: 'grey',
        backgroundColor: 'white',
        borderWidth: 1,
        bottom: 20


    },

    QoutesTxt: {
        marginLeft: 10,
        fontSize: 20,
        fontWeight: 'bold'
    },
    QoutesTxt2: {
        marginLeft: 14,
        fontSize: 14,
    },
    dayPlanner: {
        width: '90%',
        padding: 7,
        alignSelf: 'center',
        borderColor: 'grey',
        backgroundColor: 'white',
        borderWidth: 1,
        marginTop: 20

    },
    dayPlannerTxt: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    BtnMainView: {
        width: '90%',
        alignSelf: 'center',
    },
    BtnSubView: {
        marginTop: '27%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    AddImg: {
        height: 94,
        width: 94
    },
    BtnTxtView: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    BtnTxt: {
        textDecorationLine: 'underline',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 17,
        right: 4
    },

})

export default styles;