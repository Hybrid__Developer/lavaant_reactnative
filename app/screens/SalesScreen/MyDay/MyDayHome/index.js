import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import Loader from '../../../../Components/Loader';
import * as Utility from '../../../../utility/index';
import Subheader from "../../../../Components/SubHeader"
import { NavigationActions, StackActions } from 'react-navigation';

import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year
export default class MyDayHome extends Component {

    constructor(props) {
        super(props);
        this.state = {

            Qoutes: "",
            Phone: "Phone",
            InPerson: "InPerson"

        }

    }

    componentDidMount() {

        this.getQuotes()

    }
    getQuotes = async () => {
        await this.setState({
            isLoading: true
        })
        const res = await Service.get(Url.GET_QUOTES_URL)
        var responsee = res
        await this.setState({
            isLoading: false
        })
        if (res.data)
            var responsee = res.data
        console.log("add category ::::response", responsee)

        this.setState({
            isLoading: false,
            Qoutes: responsee.title
        })
    }
    goToInPerson = () => {
        Utility.setInLocalStorge('route', this.state.InPerson)

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPerson' })]
            })
        );
        this.props.navigation.navigate('InPerson')
    }
    goToInPhone = () => {
        Utility.setInLocalStorge('route', this.state.Phone)
        console.log("aaaaaa:::", this.state.Phone)
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPerson' })]
            })
        );
        this.props.navigation.navigate('InPerson')
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <Subheader
                        title={month + ' ' + date + ', ' + year}></Subheader>
                    <View style={styles.QoutesView}>
                        <Text style={styles.QoutesTxt}>Quote of the Day</Text>
                        <Text style={styles.QoutesTxt2}>{this.state.Qoutes}</Text>
                    </View>
                    <View style={styles.dayPlanner}>
                        <Text style={styles.dayPlannerTxt}>DAY PLANNER</Text>
                    </View>
                    <View style={styles.BtnMainView}>
                        <View style={styles.BtnSubView}>
                            <TouchableOpacity onPress={() => this.goToInPerson()}>
                                <Image source={require('../../../../assets/add.png')} style={styles.AddImg}></Image>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.goToInPhone()}>
                                <Image source={require('../../../../assets/add.png')} style={styles.AddImg}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.BtnTxtView}>
                            <Text style={styles.BtnTxt}>IN-PERSON</Text>
                            <Text style={styles.BtnTxt}>PHONE</Text>

                        </View>
                    </View>
                </ScrollView>

            </Container>

        );

    }
}
