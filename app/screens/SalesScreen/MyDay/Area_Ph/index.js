import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'


import {
    Container,
    Card,
    Picker,
    Icon

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class AreaPh extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, name: "Jimcarry", area: "SEW", useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: true },
                { id: 2, name: "James", area: "SEW", useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },
                { id: 3, name: "Thomson", area: "SEW", useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },


            ],
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
            isModalVisible: false,
            checked: false,
            Qoutes: "",
            search: "",
            CustomerList: [],
            id: "",
            selected_id: ''
        }

    }






    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <SubHeader title="AREA"></SubHeader>
                    <View style={styles.mainSearchView}>
                        <Card>
                            <View style={styles.searchView} >
                                <Picker
                                    mode={"dropdown"}

                                    placeholder="Start Year"
                                    iosIcon={<Icon name="ios-arrow-down" />}
                                    selectedValue={this.state.selected_id}
                                    style={styles.searchTextInput}
                                    onValueChange={(v) => this.setState({ selected_id: v })} // use it the way it suites you
                                >

                                    <Picker.Item key={-1} label={'Select Area'} value="" />
                                    <Picker.Item label="SW" value="SW" />
                                    <Picker.Item label="NW" value="NW" />
                                    <Picker.Item label="SEW" value="SEW" />
                                    <Picker.Item label="NY" value="NY" />

                                </Picker>


                            </View>

                        </Card>

                    </View>
                    {

                        this.state.data.map((item, key) => (
                            <TouchableOpacity >


                                <View style={styles.mainView}>


                                    <View style={styles.subView}>
                                        <Text style={styles.nameTxt}>Area :{item.area}</Text>

                                        <Text style={styles.areaTxt}>{item.name}</Text>
                                        <Text style={styles.adressTxt}>{item.useradress}</Text>

                                    </View>
                                    <View style={styles.checkBoxView}>
                                        <Image source={require("../../../../assets/call.png")} style={styles.callImg}></Image>
                                        <Image source={require("../../../../assets/message.png")} style={styles.mesgImg}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                </ScrollView>

            </Container>

        );

    }
}
