import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({

    subHeaderView: {
        paddingTop: '4%',
        paddingLeft: '4%',
        paddingBottom: '4%',
        width: '100%',
        backgroundColor: '#199bf1'
    },
    currentDateTxt: {
        fontSize: 20,
        color: 'white',
        fontWeight: '900',
        textAlign: 'center',
        fontWeight: "bold",

    },
    mainView: {
        flexDirection: 'row',
        marginTop: 40,
        justifyContent: 'space-evenly'
    },
    subView: {
        height: 80,
        width: 74,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#ddd',
        borderBottomWidth: 2,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 10,

        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backGroundImg: {
        height: 40,
        width: 37,
        alignItems: "center",
        justifyContent: "center"
    },
    oneTxt: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    dayTxt: {
        fontWeight: 'bold',
        fontSize: 14,
        top: 7
    },
    BtnView: {
        alignSelf: "center",
        marginTop: '30%'
    },
    AddImg: { height: 70, width: 70, borderRadius: 10 },
    setCustomTxt: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 10
    },
});

export default styles;