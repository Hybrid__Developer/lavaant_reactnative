import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ImageBackground } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import styles from './style'

import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate();
var month = monthNames[new Date().getMonth()];
var year = new Date().getFullYear();
export default class TaskReminder extends Component {


    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Subheader
                        title={month + ' ' + date + ', ' + year}
                        middleTitle="TASK / REMINDER"
                    ></Subheader>

                    <View style={styles.mainView}>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <View style={{ alignItems: "center" }}>

                                    <Text style={styles.oneTxt}>1</Text>
                                </View>

                            </ImageBackground>
                            <Text style={styles.dayTxt}>DAY</Text>
                        </View>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={styles.oneTxt}>1</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>WEEK</Text>
                        </View>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={styles.oneTxt}>2</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>WEEK</Text>
                        </View>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={styles.oneTxt}>1</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>MONTH</Text>
                        </View>
                    </View>
                    <View style={styles.BtnView}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("AddReminder")}>
                            <Image source={require("../../../../assets/add.png")} style={styles.AddImg}></Image>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.setCustomTxt}>SET CUSTOM</Text>

                </ScrollView>

            </Container >

        );

    }
}
