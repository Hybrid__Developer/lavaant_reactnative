import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Modal from 'react-native-modal';
import * as Utility from '../../../../utility/index';
import styles from './style'


import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class Area extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, name: 'John', age: 18, useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: true },
                { id: 2, name: 'Lilli', age: 23, useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },
                { id: 3, name: 'Lavera', age: 46, useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },


            ],
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
            isModalVisible: false,
            checked: false,
            search: "",
            CustomerList: [],
            id: "",
            isModalVisible: false,
            checked: false,
            selected: 'Area',
            route: ""
        }

    }

    _showModal = () => {
        if (this.state.isModalVisible) {
            this.setState({ isModalVisible: false })
        }
        else {
            this.setState({ isModalVisible: true })
        }

    }
    _hideModal = () => {
        this.setState({ isModalVisible: false })
    }
    changeColor = (value) => {
        this.setState({
            selected: value

        })
        console.log("selected area", this.state.selected)
    }
    onPressCheckbox(item) {
        const checkedCheckBox = this.state.data.find((cb) => cb.id === item.id);
        checkedCheckBox.checked = !checkedCheckBox.checked;
        console.log('checked checkbox :: ', checkedCheckBox);

        let shipings = this.state.data.map((shiping) => {
            return shiping.id === checkedCheckBox.id ? checkedCheckBox : shiping
        });
        console.log('categories after check box checked', shipings);
        this.setState({
            shipingdataDetails: shipings
        })
    }
    goToInPersonVist = (value) => {

        this.props.navigation.navigate("InPersonVisit")
    }
    componentDidMount() {

        Utility.setInLocalStorge('route', this.state.selected)
        console.log("aaaaaa:::", this.state.selected)
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <View style={styles.subHeaderView}>
                        <Text style={styles.currentDateTxt}>{month + ' ' + date + ', ' + year}</Text>
                        <Text style={styles.leftTitle}>SELECT CUSTOMER BY AREA:</Text>
                    </View>

                    <View style={styles.mainSearchView}>
                        <Card>
                            <View style={styles.searchView} >
                                <TextInput
                                    style={styles.searchTextInput}
                                    placeholder=" Search Customer"
                                    placeholderTextColor="grey"
                                    onChangeText={(search) => this.setState({ search })}
                                />
                                <TouchableOpacity>
                                    <Image
                                        source={require('../../../../assets/search.png')}
                                        style={styles.searchIcon}
                                    />
                                </TouchableOpacity>
                                <Modal

                                    position='absolute'
                                    backdropOpacity={1}
                                    backdropColor="black"
                                    hasBackdrop={true}
                                    visible={this.state.isModalVisible}
                                    onBackdropPress={() => this._showModal()}
                                    onRequestClose={() => this._hideModal()}>
                                    <View style={styles.modalMainView}>
                                        <View style={styles.modalSubView}>

                                            <Text style={styles.padding}>
                                                Select Area
   </Text>
                                        </View>

                                        <View style={styles.modalSubView}>

                                            <Text style={styles.padding}>
                                                NY
   </Text>
                                            <CheckBox
                                                style={styles.margin}
                                                onPress={() => {
                                                    this.changeColor("NY")
                                                }
                                                }
                                                checked={this.state.selected == "NY"}
                                            />
                                        </View>
                                        <View style={styles.modalSubView}>

                                            <TouchableOpacity>
                                                <Text style={styles.padding}>
                                                    SEW
   </Text>
                                            </TouchableOpacity>
                                            <CheckBox
                                                style={styles.margin}
                                                onPress={() => {
                                                    this.changeColor("SEW")
                                                }
                                                }
                                                checked={this.state.selected == "SEW"}
                                            />
                                        </View>
                                        <View style={styles.modalSubView}>

                                            <TouchableOpacity>
                                                <Text style={styles.padding}>
                                                    NW
</Text>
                                            </TouchableOpacity>
                                            <CheckBox
                                                style={styles.margin}
                                                onPress={() => {
                                                    this.changeColor("NW")
                                                }
                                                }
                                                checked={this.state.selected == "NW"}
                                            />
                                        </View>
                                        <View style={styles.modalSubView}>

                                            <TouchableOpacity>
                                                <Text style={styles.padding}>
                                                    SW
</Text>
                                            </TouchableOpacity>
                                            <CheckBox
                                                style={styles.margin}
                                                onPress={() => {
                                                    this.changeColor("SW")
                                                }
                                                }
                                                checked={this.state.selected == "SW"}
                                            />
                                        </View>
                                    </View>

                                </Modal>

                            </View>

                        </Card>
                        <View style={styles.filterView}>
                            <TouchableOpacity onPress={() => this._showModal()}>
                                <Image
                                    source={require('../../../../assets/reportFilter1.png')}
                                    style={styles.filterIcon}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {

                        this.state.data.map((item, key) => (
                            <TouchableOpacity >


                                <View style={styles.mainView}>

                                    <Image
                                        style={styles.userImg}
                                        resizeMode='cover'
                                        source={{ uri: this.state.pic }}

                                    />
                                    <Image
                                        style={styles.userImgBackgroung}
                                        resizeMode='cover'
                                        source={require('../../../../assets/image_view1.png')}
                                    />
                                    {/* <Image
                                        style={styles.userImg}
                                        resizeMode='cover'
                                        source={{ uri: this.state.pic }}

                                    />
                                    <Image
                                        style={styles.userImgBackgroung}
                                        resizeMode='cover'
                                        source={require('../../../../assets/image_view1.png')}
                                    /> */}

                                    <View style={styles.subView}>
                                        <Text style={styles.nameTxt}>{item.name}</Text>
                                        <Text style={styles.adressTxt}>{item.useradress}</Text>

                                    </View>
                                    <View style={styles.checkBoxView}>
                                        <CheckBox style={styles.checkBox}
                                            checked={item.checked}
                                            onPress={() => this.onPressCheckbox(item)}
                                        />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                    <View style={styles.btnView}>
                        <TouchableOpacity
                            style={[styles.Btn, styles.shadow]}
                            onPress={() => this.goToInPersonVist("Area")}
                        // onPress={() => this.goToShipment()}
                        >
                            <Text style={styles.BtnTxt2}>SELECT</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

            </Container>

        );

    }
}
