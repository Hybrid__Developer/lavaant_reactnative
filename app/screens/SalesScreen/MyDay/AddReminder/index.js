import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ImageBackground } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import styles from './style'

import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class AddReminder extends Component {


    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <Subheader title={month + ' ' + date + ', ' + year}
                        middleTitle="TASK DUE : 27-7-2020"></Subheader>
                    <View style={styles.TextInput}>
                        <TextInput
                            multiline={true}
                            placeholder="Enter Quotes"
                        >

                        </TextInput>
                    </View>
                    <View style={{ marginTop: 40, alignItems: 'center' }}>
                        <Image source={require("../../../../assets/bellicon.png")} style={{ height: 40, width: 40 }}></Image>
                    </View>
                    <View style={styles.mainView}>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={styles.oneTxt}>1</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>DAY</Text>
                        </View>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={styles.oneTxt}>2</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>DAY</Text>
                        </View>
                        <View style={styles.subView}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={styles.oneTxt}>1</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>WEEK</Text>
                        </View>
                        <TouchableOpacity style={styles.subView} onPress={() => this.props.navigation.navigate('Custom')}>
                            <ImageBackground source={require('../../../../assets/polygon3.png')} style={styles.backGroundImg}>
                                <Text style={[styles.oneTxt, { fontSize: 30 }]}>+</Text>
                            </ImageBackground>
                            <Text style={styles.dayTxt}>CUSTOM</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>

            </Container >

        );

    }
}
