import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, Dimensions, ScrollView } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import Geocoder from 'react-native-geocoding';
import MapView from 'react-native-maps';
import {
    Container,
    Card,
    CheckBox,
    Icon

} from 'native-base';
import Loader from '../../../../Components/Loader';
import { NavigationActions, StackActions } from 'react-navigation';
import styles from './style'
const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.005
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year
export default class ShipToInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            contact: '',
            region: {
                latitude: 28.644800,
                longitude: 77.216721,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            marker: {
                latitude: 0,
                longitude: 0,
            },
            pinAddress: '',
            loaded: false,
            descriptio: ''
        }

        Geocoder.init("AIzaSyCgd-rD47NFwKVpQ30skw_D-qWUMHrxjO4");

    }


    goToInPerson = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPerson' })]
            })
        );
        this.props.navigation.navigate('InPerson')
    }

    render() {
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Subheader
                        title={month + ' ' + date + ', ' + year}
                        leftTitle="SHIP TO INFO:"
                    />
                    <Loader isLoading={this.state.isLoading} />
                    <View style={{ padding: '6%', }}>
                        <Card style={{ borderRadius: 5, height: 200, width: 300, alignSelf: "center" }}>
                            <Icon name="md-pin"
                                style={{
                                    zIndex: 3,
                                    position: 'absolute',
                                    marginTop: -37,
                                    marginLeft: -11,
                                    left: '50%',
                                    top: '50%',
                                    color: "red"
                                }}
                                size={40}
                                color="#f00" />
                            <MapView
                                style={styles.mapView}
                                showsUserLocation={true}
                                scrollEnabled={true}
                                followsUserLocation={true}
                                zoomEnabled={true}

                                region={{
                                    latitude: 42.882004,
                                    longitude: 74.582748,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                                // region={this.state.region}
                                showsIndoors={true}
                                zoom={20}
                            >
                            </MapView>
                        </Card>
                    </View>
                    <Card style={{ borderRadius: 5, marginLeft: '8%', marginRight: '8%', }}>
                        <View style={styles.SectionStyle}>
                            <Image
                                source={require('../../../../assets/userGrey.png')}
                                style={styles.icon}
                            />
                            <View style={styles.textInputView}>
                                <TextInput
                                    allowFontScaling={false}
                                    require
                                    placeholder="Contact"
                                    // placeholderTextColor="#fff"
                                    onChangeText={(contact) => this.setState({ contact })}
                                    style={styles.txtInput}
                                />
                            </View>

                        </View>

                        <View style={styles.SectionStyle}>
                            <Image
                                source={require('../../../../assets/personal_notes.png')}
                                style={styles.icon}
                            />
                            <View style={styles.textInputView}>
                                <TextInput
                                    allowFontScaling={false}
                                    require
                                    placeholder="Personal Notes"
                                    // placeholderTextColor="#fff"
                                    onChangeText={(note1) => this.setState({ note1 })}
                                    style={styles.txtInput}
                                />
                            </View>

                        </View>


                        <View style={[styles.SectionStyle, { marginBottom: "5%" }]}>
                            <Image
                                source={require('../../../../assets/personal_notes.png')}
                                style={styles.icon}
                            />
                            <View style={styles.textInputView}>
                                <TextInput
                                    allowFontScaling={false}
                                    require
                                    placeholder="Personal Notes"
                                    // placeholderTextColor="#fff"
                                    onChangeText={(note1) => this.setState({ note1 })}
                                    style={styles.txtInput}
                                />
                            </View>

                        </View>
                    </Card>

                    <View style={styles.btnView}>
                        <TouchableOpacity
                            style={[styles.Btn, styles.shadow]}
                            onPress={() => this.goToInPerson()}
                        >
                            <Text style={styles.BtnTxt2}>SUBMIT</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}
