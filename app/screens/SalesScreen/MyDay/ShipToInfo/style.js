import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../utility/index';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '70%',
        width: '100%',
        marginTop: '2%',
        zIndex: -1
    },
    mapView: {
        ...StyleSheet.absoluteFillObject,
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // marginBottom: hp("3%"),
        padding: '3%'
    },
    mapView: {

        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    icon: {
        width: 22,
        height: 22,
        marginRight: 10,
        // marginTop: 5,
    },
    txtInput: {
        fontSize: wp('4%'),
        // color: colors.whiteColor,
    },
    textInputView: {
        flex: 1,
        // borderBottomColor: '#FFF',
        borderBottomWidth: 1,
    },
    btnView: { marginBottom: '5%', marginTop: '20%' },
    BtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: colors.primaryColor,
        borderRadius: 10,
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
});
export default styles;