import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'


import {
    Container,
    Card,

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class ShipToPh extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [
                { id: 1, name: "Jimcarry", area: "SEW", useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: true },
                { id: 2, name: "James", area: "SEW", useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },
                { id: 3, name: "Thomson", area: "SEW", useradress: '207-C, Rafael Towers, 8/2, Old Palasia,', checked: false },


            ],
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
            isModalVisible: false,
            checked: false,
            Qoutes: "",
            search: "",
            CustomerList: [],
            id: "",
        }

    }






    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />

                <ScrollView>
                    <SubHeader title="SHIP TO:"></SubHeader>
                    <View style={styles.mainSearchView}>
                        <Card>
                            <View style={styles.searchView} >
                                <TextInput
                                    style={styles.searchTextInput}
                                    placeholder="Search ShipTo"
                                    placeholderTextColor="grey"
                                    onChangeText={(search) => this.setState({ search })}
                                />
                                <TouchableOpacity >
                                    <Image
                                        source={require('../../../../assets/search.png')}
                                        style={styles.searchIcon}
                                    />
                                </TouchableOpacity>


                            </View>

                        </Card>

                    </View>
                    {

                        this.state.data.map((item, key) => (
                            <TouchableOpacity >


                                <View style={styles.mainView}>


                                    <View style={styles.subView}>
                                        <Text style={styles.nameTxt}>{item.name}</Text>
                                        <Text style={styles.areaTxt}>Area :{item.area}</Text>
                                        <Text style={styles.adressTxt}>{item.useradress}</Text>

                                    </View>
                                    <View style={styles.checkBoxView}>
                                        <Image source={require("../../../../assets/call.png")} style={styles.callImg}></Image>
                                        <Image source={require("../../../../assets/message.png")} style={styles.mesgImg}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}

                </ScrollView>

            </Container>

        );

    }
}
