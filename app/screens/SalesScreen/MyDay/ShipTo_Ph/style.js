import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';

const styles = StyleSheet.create({


    mainSearchView: {
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row',
        bottom: 20
    },

    searchView: {
        flexDirection: 'row',
        width: '100%',
        height: 35,

    },
    searchTextInput: {
        width: '87%',
        height: 35,
        marginLeft: 5
    },
    searchIcon: {
        color: 'white',
        flex: 1,
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: '1%',
    },

    callImg: {
        height: 40, width: 40
    },

    mesgImg: {
        height: 40, width: 40, marginLeft: 12
    },
    mainView: {
        justifyContent: 'space-between',
        bottom: 4,
        padding: '4%', marginTop: 10,
        width: '90%', alignSelf: 'center',
        flexDirection: 'row',

        // borderWidth: 1,
        // borderRadius: 10,
        // borderColor: '#ddd',
        // borderBottomWidth: 2,
        // shadowColor: '#000000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.9,
        // shadowRadius: 10,
        borderRadius: 1,
        elevation: 2
    },
    subView: {
        flexDirection: 'column',
        width: '66%',
        marginLeft: '4%'
    },


    checkBoxView: {
        alignItems: "center", flexDirection: "row",
        marginRight: 20,
        justifyContent: "center",
        paddingBottom: 16,
    },
    nameTxt: { fontSize: 15, },
    areaTxt: { fontSize: 15, },
    adressTxt: { fontSize: 12, }
})

export default styles;