import { Dimensions, StyleSheet, Platform } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../../../utility/index';
const styles = StyleSheet.create({
    headings: {
        fontWeight: 'bold',
        color: colors.primaryColor,
        fontSize: 18,
    },
    datesMainView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'pink',
        marginBottom: '5%'
    },
    dateIndividualView: {
        width: '45%',
        marginTop: '2%'
    },
    inputViewStyle: {
        // flex: 1,
        borderColor: 'gray',
        borderWidth: 1,
        height: 30,
        marginTop: '5%',
        borderRadius: 5
    },
    dateInput: {
        borderWidth: 0,
    },
    dateText: {
        // left: -28,
        color: '#000',
    },
    btnView: {
        marginBottom: '3%',
        marginTop: hp('30%'),
        flex: 1,
        // justifyContent: 'flex-end',
        marginBottom: '2%',
        // flexDirection: 'column'
    },
    BtnTxt2: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    Btn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: '90%',
        alignSelf: 'center',
        backgroundColor: colors.primaryColor,
        borderRadius: 10,
    },
    shadow: {
        elevation: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        shadowOffset: {
            width: 0, height: 1
        },
    },
});
export default styles;