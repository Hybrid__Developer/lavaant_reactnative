import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ScrollView } from 'react-native';
import MainHeader from '../../../../Components/MainHeader'
import Subheader from '../../../../Components/SubHeader'
import * as Utility from '../../../../utility/index';
import DatePicker from 'react-native-datepicker'
import moment from "moment";

import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import Loader from '../../../../Components/Loader';
import styles from './style'

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var that = this;
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class Custom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            from: '',
            to: '',
            note: ''
        }
    }

    render() {
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Subheader title={month + ' ' + date + ', ' + year}
                        leftTitle="CUSTOM TASK/REMINDER"
                    ></Subheader>
                    <Loader isLoading={this.state.isLoading} />
                    <View style={{ padding: '6%', }}>
                        <Card style={{ borderRadius: 5, }}>
                            <View style={{ padding: '5%' }}>
                                <Text style={styles.headings}>SELECT DATE</Text>
                                <View style={styles.datesMainView}>
                                    <View style={styles.dateIndividualView}>
                                        <Text> From:</Text>
                                        <View style={styles.inputViewStyle}>
                                            <DatePicker
                                                allowFontScaling={false}
                                                showIcon={false}
                                                date={this.state.from}
                                                mode="date"
                                                placeholder=" "
                                                format="DD-MM-YYYY"
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                minDate={new Date()}
                                                // maxDate={new Date()}
                                                customStyles={{
                                                    dateInput: styles.dateInput,
                                                    dateText: styles.dateText,
                                                    // placeholderText: styles.placeholderText,
                                                    // btnTextConfirm: styles.btnTextConfirm,
                                                }}
                                                onDateChange={(date) => { this.setState({ from: date }) }}
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.dateIndividualView}>
                                        <Text>To:</Text>
                                        <View style={styles.inputViewStyle}>
                                            <DatePicker
                                                allowFontScaling={false}
                                                showIcon={false}
                                                date={this.state.to}
                                                mode="date"
                                                placeholder=" "
                                                format="DD-MM-YYYY"
                                                minDate={this.state.from}
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                // maxDate={new Date()}
                                                customStyles={{
                                                    dateInput: styles.dateInput,
                                                    dateText: styles.dateText,
                                                    // placeholderText: styles.placeholderText,
                                                    btnTextConfirm: styles.btnTextConfirm,
                                                }}
                                                onDateChange={(date) => { this.setState({ to: date }) }}
                                            />
                                        </View>
                                    </View>
                                </View>

                                {/* notes section */}
                                <Text style={styles.headings}>ADDITIONAL NOTES:</Text>
                                <View style={{ borderWidth: 1, borderRadius: 5, marginTop: '5%', minheight: 100, borderColor: 'gray', }}>
                                    <TextInput
                                        style={{ padding: '2%', }}
                                        multiline={true}
                                        numberOfLines={4}
                                        onChangeText={(note) => this.setState({ note })}

                                    />
                                </View>
                            </View>
                        </Card>
                        <View style={styles.btnView}>
                            <TouchableOpacity
                                style={[styles.Btn, styles.shadow]}
                            // onPress={() => }

                            >
                                <Text style={styles.BtnTxt2}>SUBMIT</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}