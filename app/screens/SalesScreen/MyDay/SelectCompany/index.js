import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ImageBackground } from 'react-native';
import SubHeader from '../../../../Components/SubHeader'
import MainHeader from '../../../../Components/MainHeader'
import styles from './style'
import * as Utility from '../../../../utility/index';
import * as Url from '../../../../constants/urls'
import * as Service from '../../../../api/services'
import Loader from '../../../../Components/Loader';
import {
    Container,
    Card,
    CheckBox

} from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
var that = this;
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var date = new Date().getDate(); //Current Date
var month = monthNames[new Date().getMonth()]; //Current Month
var year = new Date().getFullYear(); //Current Year

export default class SelectCompany extends Component {

    constructor(props) {
        super(props);
        this.state = {

            companyList: [],
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
            search: "",
            selected: "company"
        }

    }
    componentDidMount() {
        this.getCompanyList(),
            Utility.setInLocalStorge('route', this.state.selected)
        console.log("aaaaaa:::", this.state.selected)
    }

    searchCompany = async () => {

        console.log('inside  search company list')
        this.setState({
            isLoading: true
        })
        let token = await Utility.getFromLocalStorge('token')
        console.log('token :: ', token);
        const res = await Service.get(`${Url.GET_SEARCH_COMPANY_URL}companyName=${this.state.search}`, token)
        console.log('response of search customerLit :: ::', res);
        if (res.data) {
            this.setState({
                companyList: res.data,
                isLoading: false
            })


        }
    }

    getCompanyList = async () => {
        console.log('inside company list')
        this.setState({
            isLoading: true
        })
        let token = await Utility.getFromLocalStorge('token')
        console.log('token :: ', token);
        const res = await Service.get(Url.GET_COMPANYLIST_URL, token)
        console.log('response of customerLit :: ::', res);
        if (res.data) {
            this.setState({
                companyList: res.data,
                isLoading: false
            })

        }
    }


    goToInPersonVist = (favourites) => {
        let body = []
        if (favourites && favourites.length) {
            favourites.forEach(item => {
                if (item.checked) {
                    body.push({ name: item.name, id: item.id })
                }
            });
            console.log('body', body)
            // this.props.navigation.navigate("InPersonVisit")
        }

    }
    onCheckBoxPress = async (value, key) => {
        console.log('onCheckBoxPress.....', key)
        let a = false
        this.state.companyList.forEach(item => {
            if (item.name == value.name) {
                if (item.checked) {
                    item.checked = false;
                }
                else {
                    a = true

                    return item.checked = true;
                }
            }
        });
        await this.setState({
            companyList: this.state.companyList,
        });
    }
    selectedCompany = async (item) => {
        let token = await Utility.getFromLocalStorge('token')
        this.setState({

            isShow: !item.companySelected,
            companyName: item.companyName
        })
        await this.setState({
            isLoading: true
        })
        let body = {
            workDetails: {
                companyName: this.state.companyName,
                companySelected: this.state.isShow
            }

        }
        console.log(body)
        const res = await Service.put(Url.SELECTED_COMPANY_URL, token, body)
        await this.setState({
            isLoading: false
        })
        this.getCompanyList()
    }
    render() {

        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <Loader isLoading={this.state.isLoading} />
                    <View style={styles.subHeaderView}>
                        <Text style={styles.currentDateTxt}>{month + ' ' + date + ', ' + year}</Text>
                        <Text style={styles.leftTitle}>SELECT COMPANY:</Text>
                    </View>
                    <View style={styles.mainSearchView}>
                        <Card>
                            <View style={styles.searchView} >
                                <TextInput
                                    style={styles.searchTextInput}
                                    placeholder=" Search Company"
                                    placeholderTextColor="grey"
                                    onChangeText={(search) => this.setState({ search })}
                                />
                                <TouchableOpacity onPress={() => this.searchCompany()}>
                                    <Image
                                        source={require('../../../../assets/search.png')}
                                        style={styles.searchIcon}
                                    />
                                </TouchableOpacity>

                            </View>

                        </Card>

                    </View>
                    {

                        this.state.companyList.map((item, key) => (
                            <TouchableOpacity >


                                <View style={styles.mainView}>

                                    <Image
                                        style={styles.userImg}
                                        resizeMode='cover'
                                        source={require('../../../../assets/intel.png')}

                                    />
                                    <Image
                                        style={styles.userImgBackgroung}
                                        resizeMode='cover'
                                        source={require('../../../../assets/image_view1.png')}
                                    />

                                    <View style={styles.subView}>
                                        <Text style={styles.nameTxt}>{item.companyName}</Text>
                                        <Text style={styles.adressTxt}>{item.companyAddress}</Text>

                                    </View>
                                    <View style={styles.checkBoxView}>
                                        <CheckBox style={styles.checkBox}
                                            checked={item.companySelected}
                                            onPress={() => this.selectedCompany(item)}
                                        />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                    <View style={styles.btnView}>
                        <TouchableOpacity
                            style={[styles.Btn, styles.shadow]}
                            onPress={() => this.goToInPersonVist(this.state.companyList)}
                        // onPress={() => this.goToShipment()}
                        >
                            <Text style={styles.BtnTxt2}>SELECT</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>

            </Container>

        );

    }
}
