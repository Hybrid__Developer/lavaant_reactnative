import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import KeyboardView from '../../screens/KeyboardView';
import styles from './style';
import * as Utility from '../../utility/index';
import * as Url from '../../constants/urls'
import * as Service from '../../api/services'
import Loader from '../../Components/Loader'
import { NavigationActions, StackActions } from 'react-navigation';

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      email: 'isha131722@gmail.com',
      password: '123456789',
      isLoading: false
    };
  }

  login = async () => {
    if (Utility.isFieldEmpty(this.state.email && this.state.password)) {
      Alert.alert("", "Please enter all the fields")
    }
    else if (Utility.isValidEmail(this.state.email)) {
      Alert.alert("", "Please Enter Valid Email")
    }
    else {
      let body = {
        email: this.state.email.toLowerCase(),
        password: this.state.password
      }
      console.log('body', body)

      this.setState({
        isLoading: true
      })
      try {
        const res = await Service.post(Url.LOGIN_URL, '', body)
        console.log('data', res)
        if (res.data) {
          console.log('login::data', res.data)
          this.setState({
            isLoading: false
          })
          global.user = res.data;
          Utility.setInLocalStorge('user', JSON.stringify(global.user))
          await Utility.setInLocalStorge('user', JSON.stringify(global.user))

          Utility.setInLocalStorge('token', res.data.token)
          Utility.setInLocalStorge('userId', res.data.id)
          console.log('userId...........................................', res.data.id)
          let cartItems = []
          Utility.setInLocalStorge('cartItems', cartItems)
          Utility.setInLocalStorge('role', res.data.role)

          console.log('global.user.customerGroup global.user.customerGroup', global.user.customerGroup)
          if (res.data.role.toLowerCase() == 'vendor') {
            this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'SignIn' })]
              })
            );
            this.props.navigation.navigate('BottomTab2');
          } else if (res.data.role.toLowerCase() == 'customer' && (global.user.customerGroup === '' || global.user.customerGroup === null || global.user.customerGroup === undefined)) {
            this.props.navigation.navigate('MyProfile')
          }
          else {
            await Utility.setInLocalStorge('customerGroup', global.user.customerGroup.id)
            console.log('in else')
            this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'SignIn' })]
              })
            );
            this.props.navigation.navigate('BottomTab');
          }
        }
        else {
          Alert.alert('', res.err.message)
          console.log('err-res', res)
          this.setState({
            isLoading: false
          })
        }

      } catch (err) {
        this.setState({
          isLoading: false
        })
        Alert.alert('', err.message)
        console.log('err', err)
      }
    }
  };

  render() {
    return (
      <KeyboardView behavior="padding" style={styles.wrapper}>
        <ImageBackground
          source={require('../../assets/background.png')}
          style={styles.backgroundImage}>
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            ref={ref => (this.scrollView = ref)}
            onContentSizeChange={(contentWidth, contentHeight) => {
              this.scrollView.scrollToEnd({ animated: true });
            }}
          >
            <View style={styles.logoContainer}>
              <Image
                source={require('../../assets/logo.png')}
                style={styles.logo}
              />
            </View>
            <View style={styles.formView}>
              <Loader isLoading={this.state.isLoading} />
              <View style={styles.SectionStyle}>
                <Image
                  source={require('../../assets/loginR/e-mail.png')}
                  style={styles.icon}
                />
                <View style={styles.textInputView}>
                  <TextInput
                    allowFontScaling={false}
                    require
                    placeholder="E-mail id"
                    placeholderTextColor="#fff"
                    onChangeText={(email) => this.setState({ email })}
                    style={styles.txtInput}
                  />
                </View>

              </View>
              <View style={styles.SectionStyle}>
                <Image
                  source={require('../../assets/loginR/password.png')}
                  style={styles.icon}
                />
                <View style={styles.textInputView}>
                  <TextInput
                    require
                    allowFontScaling={false}
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#fff"
                    onChangeText={(password) => this.setState({ password })}
                    style={styles.txtInput}
                  />
                </View>
              </View>
              <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                  <Text allowFontScaling={false}
                    style={styles.forgotPasswordText}>
                    Forgot password?
                    </Text>
                </TouchableOpacity>
              </View>
              <View >
                <TouchableOpacity
                  style={[styles.LoginBtn, styles.AJ]}
                  onPress={() => this.login()}>
                  <Text
                    allowFontScaling={false}
                    style={styles.LoginBtnTxt}>LOGIN</Text>
                </TouchableOpacity>
              </View>
              <View>
                <Text
                  allowFontScaling={false}
                  style={styles.orTxt}>
                  OR
              </Text>
              </View>
              <View>
                <TouchableOpacity style={[styles.FbBtn, styles.AJ]}>
                  <Text
                    allowFontScaling={false}
                    style={styles.LoginBtnTxt2}>Login with facebook</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.bottomView}>
              <View
                style={styles.linkView}>
                <Text
                  allowFontScaling={false}
                  style={styles.linkedAccText}>
                  Not Registered Yet?
              </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.replace('SignUp')}>
                  <Text
                    allowFontScaling={false}
                    style={styles.linkedText}>
                    {' '}
                  Register
                </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
      </KeyboardView >
    );
  }

}

