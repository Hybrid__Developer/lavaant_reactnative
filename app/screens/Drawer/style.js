import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import * as colors from '../../constants/colors';
const styles = StyleSheet.create({

    cantainer: {
        // backgroundColor: colors.primaryColor,
        height: '100%'

    },
    txt: {
        fontSize: 20,
        color: '#fff'
    },
    icon: {
        height: 20,
        width: 20,
        marginRight: 12,
        marginBottom: 30
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        height: window.height
    },
});
export default styles;
