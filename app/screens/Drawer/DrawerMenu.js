import React, { Component } from 'react'
import { Alert, Text, View, SafeAreaView, Image, TouchableOpacity, ScrollView, Switch, BackHandler, StatusBar, ImageBackground } from 'react-native'
import Styles from './style'
import * as Utility from "../../utility/index"
import { NavigationActions, StackActions } from 'react-navigation';
export default class DrawerMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pic: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png",
        };

    }

    componentDidUpdate() {
        const isDrawerOpen = this.props.navigation.state.isDrawerOpen;
        if (!isDrawerOpen) {
            this.props.navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'BottomTab' })]
                })
            );
            // navigation.navigate('ScreenTwo')
            this.props.navigation.navigate('tab2');
        }
    }

    resetUserId = async () => {
        await Utility.removeAuthKey('token')
        await Utility.removeAuthKey('user')
        await Utility.removeAuthKey('userId')
       global.favouriteSubCategories = []
        console.warn('userDetail successfully removed')
        this.props.navigation.navigate('SignIn')
    }
    SignOut = () => {
        Alert.alert(
            "",
            "Do You Want to Sign Out",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => this.resetUserId() },

            ],
            { cancelable: false },
        );
        return true;

    }
    Settings = () => {

        this.props.navigation.navigate('EditProfile')

    }
    changePassword = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ChangePassword' })]
            })
        );
        this.props.navigation.navigate('ChangePassword')
        // this.props.navigation.dispatch(DrawerActions.closeDrawer())
    }
    contactUs = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ContactUs' })]
            })
        );
        this.props.navigation.navigate('ContactUs')
        // this.props.navigation.dispatch(DrawerActions.closeDrawer())
    }
    profile = () => {

        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'MyProfile' })]
            })
        );
        this.props.navigation.navigate('MyProfile')
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ImageBackground
                    source={require('../../assets/bg_draw.png')}
                    style={Styles.backgroundImage}
                    resizeMode='cover'
                    resizeMethod='scale'>
                    <View style={Styles.cantainer}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 48, marginBottom: 50 }}>
                            <ImageBackground source={{ uri: global.user.profilePic || this.state.pic }} style={{
                                height: 160, width: 160,
                            }}>
                                <Image source={require('../../assets/polygon2.png')} style={{ height: 160, width: 160, }} />
                            </ImageBackground>
                        </View>
                        <View style={{
                            ...Platform.select({
                                ios: {
                                    borderWidth: 0.5,
                                },
                                android: {
                                    borderWidth: 0.2,

                                }
                            }),
                            borderColor: '#fff'
                        }}></View>
                        <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
                            <Text
                                allowFontScaling={false}
                            >{this.state.name}</Text>
                            <Text
                                allowFontScaling={false}
                                style={{ color: '#fff' }}>{this.state.email}</Text>
                        </View>
                        <View style={{ marginLeft: 20, }}>
                            <TouchableOpacity onPress={this.profile}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/userNew.png')} style={Styles.icon} />
                                    <Text
                                        allowFontScaling={false}
                                        style={Styles.txt}>My Profile</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.changePassword}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/lockNew.png')} style={Styles.icon} />
                                    <Text
                                        allowFontScaling={false}
                                        style={Styles.txt}>Change Password</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.contactUs}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/settingNew.png')} style={Styles.icon} />
                                    <Text
                                        allowFontScaling={false}
                                        style={Styles.txt}>Contact Us</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.SignOut()}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../assets/logoutNew.png')} style={Styles.icon} />
                                    <Text
                                        allowFontScaling={false}
                                        style={Styles.txt} >Log Out</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </SafeAreaView>
        )
    }
}