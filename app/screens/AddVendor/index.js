import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';
import {
    Container,
    Card,
} from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import styles from './styles';
import SubHeader from '../../Components/SubHeader';
import MainHeader from '../../Components/MainHeader';
import Loader from '../../Components/Loader';
import * as Service from '../../api/services';
import * as Url from '../../constants/urls';
import * as commanFn from '../../utility/index';
import * as Utility from '../../utility/index';
export default class AddVendor extends Component {
    constructor(props) {
        super(props);
        this.searchVendor = this.searchVendor.bind(this)
        this.state = {
            isLoading: false,
            vendorList: [],

        };
    }

    componentDidMount() {
        // let vendorList = [
        //     {
        //         imageUrl: 'https://casaplex.com/wp-content/uploads/2013/08/jbl-logo-wide.jpg',
        //         name: 'Lee',
        //         email: 'lee@gmail.com'
        //     },
        //     {
        //         imageUrl: 'https://casaplex.com/wp-content/uploads/2013/08/jbl-logo-wide.jpg',
        //         name: 'Imperial Estates',
        //         email: 'imperial@gmail.com'
        //     }, {
        //         imageUrl: 'https://casaplex.com/wp-content/uploads/2013/08/jbl-logo-wide.jpg',
        //         name: 'Pinewheel Estates',
        //         email: 'pine@gmail.com'
        //     }
        // ];
        // this.setState({
        //     vendorList: vendorList
        // })
    }


    addVendor = async (item) => {
        console.log('inside ADD VENDORRRRRR]]]]]]]]]]]]]]]]]')
        let token = await Utility.getFromLocalStorge('token')
        let body = {
            id: item.id
        }
        console.log("body", body)
        const res = await Service.put(Url.PUT_ADDVENDOR_URL, token, body)
        console.log("addvendorrrrrr&&&&&&&&&", res)

        if (res.data) {
            this.props.navigation.navigate('Vendor')
        }
        else {
            Alert.alert('', 'Something Went Wrong')
        }

    }

    searchVendor = async (searchString) => {
        console.log('inside search vendor')
        console.log('Search String : ', searchString);
        this.setState({
            isLoading: true
        })

        try {
            let token = await commanFn.getFromLocalStorge('token')
            console.log('token :: ', token);
            const res = await Service.get(Url.SEARCH_VENDOR_URL + `searchString=${searchString}`, token)
            console.log('searchVendor', res)
            if (res.data) {
                this.setState({
                    vendorList: res.data,
                    isLoading: false
                })
            }
            else {
                this.setState({ isLoading: false })
                Alert.alert('', 'Something Went Wrong')
            }
        } catch (err) {
            this.setState({ isLoading: false })
            Alert.alert('', err.message)
        }
    }
    // const { params } = this.props.navigation.state
    render() {
        return (
            <Container>
                <MainHeader navigate={this.props.navigation} />
                <ScrollView>
                    <SubHeader title="Add Vendor"
                        displaySearchBar={true}
                        onSearchVendor={this.searchVendor} />
                    <Loader isLoading={this.state.isLoading} />
                    <View style={styles.mainHeadingView}>
                        {this.state.vendorList.map((item, key) =>
                            <View>
                                <Card style={styles.cardView}>
                                    <View style={styles.vendorImageView}>
                                        <Image source={require('../../assets/company.png')} style={styles.imageStyling} />
                                        {/* <Image source={{ uri: item.imageUrl }} style={styles.imageStyling} /> */}
                                    </View >
                                    <View style={styles.vendorDetailsView}>
                                        <View>
                                            <Text style={styles.mainHeading}>VENDOR NAME</Text>
                                            <Text style={styles.heading}>{item.name}</Text>
                                        </View>
                                        <View style={{ marginTop: '10%' }}>
                                            <Text style={styles.mainHeading}>VENDOR EMAIL</Text>
                                            <Text style={styles.heading}>{item.email}</Text>
                                        </View>

                                    </View>
                                    <View style={styles.addUserView}>
                                        <TouchableOpacity onPress={() => this.addVendor(item)}>
                                            <Image source={require('../../assets/usericon_blue.png')} style={styles.addUserImage} />
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </View>
                        )}

                    </View>
                </ScrollView>
            </Container>

        );
    }
}