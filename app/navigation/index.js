
import React from 'react';
import { Image, Text, ImageBackground, View } from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from '../screens/Splash';
import SignInScreen from '../screens/SignIn';
import SignUpScreen from '../screens/SignUp';
import MyProfileScreen from '../screens/MyProfile';
import SelectVendor from '../screens/Vendor';
import ProductsScreen from '../screens/Products/';
import ProductDetailScreen from '../screens/ProductDetails/'
import CartScreen from '../screens/Cart/'
import AddAddressScreen from '../screens/AddAddress'
import ChooseAddressScreen from '../screens/ChooseAddress/'
import ReportsScreen from '../screens/Reports'
import OrderDetailScreen from '../screens/OrderDetail/'
import ChangePasswordScreen from '../screens/ChangePassword/'
import ContactUsScreen from '../screens/ContactUs/';
import ForgotPasswordScreen from '../screens/Forgotpassword';
import OtpVerificationScreen from '../screens/OtpVerification';
import ResetPasswordScreen from '../screens/ResetPassword';
import AddVendorScreen from '../screens/AddVendor';
import FiltersScreen from '../screens/Filters';
import VendorDrawerNavigator from '../navigation/drawer'
import PdfViewerScreen from '../screens/Pdfview/index';
// sales screen //

import RemainderScreen from '../screens/SalesScreen/RemainderScreen';
import HomeScreen from '../screens/SalesScreen/Home';
import CustomerListScreen from '../screens/SalesScreen/CustomerModule/CustomerList'
import SelectCustomerScreen from '../screens/SalesScreen/CustomerModule/SelectCustomer'
import OrderListScreen from '../screens/SalesScreen/OrderModule/OrderList'
import AcceptNewOrderScreen from '../screens/SalesScreen/OrderModule/AcceptNewOrder';
import AddNewTaskScreen from '../screens/SalesScreen/TaskModule/AddNewTaskScreen'
import AllTaskScreen from '../screens/SalesScreen/TaskModule/AllTask'
import AddEventScreen from '../screens/SalesScreen/CalenderModule/AddEvent'
import TaskDetailScreen from '../screens/SalesScreen/CalenderModule/TaskDetail'
import CalanderScreenScreen from '../screens/SalesScreen/CalenderModule/calanderScreen'
import CompanyListScreen from '../screens/SalesScreen/CompanyModule/CompanyList'
import CompanyUserScreen from '../screens/SalesScreen/CompanyModule/CompanyUser'
import CategoryScreen from '../screens/SalesScreen/TaskModule/CategoryScreen'
import WeeklyCalandarScreen from '../screens/SalesScreen/CalenderModule/WeeklyCalandar'
import EditEventScreen from '../screens/SalesScreen/CalenderModule/EditEvent'
import EditCustomerScreen from '../screens/SalesScreen/CustomerModule/EditCustomer'
import AddCustomerScreen from '../screens/SalesScreen/CustomerModule/AddCustomer'
import MyDayHomeScreen from '../screens/SalesScreen/MyDay/MyDayHome'
import InPersonScreen from '../screens/SalesScreen/MyDay/InPerson'
import ShipToScreen from '../screens/SalesScreen/MyDay/ShipTo';
import InPersonVisitScreen from '../screens/SalesScreen/MyDay/InPersonVisit';
import ShipToInfoScreen from '../screens/SalesScreen/MyDay/ShipToInfo';
import TaskReminderScreen from '../screens/SalesScreen/MyDay/TaskReminder'
import AddReminderScreen from '../screens/SalesScreen/MyDay/AddReminder'
import AddNotesScreen from '../screens/SalesScreen/MyDay/AddNotes'
import SelectCustomersScreen from '../screens/SalesScreen/MyDay/SelectCustomers'
import AreaScreen from '../screens/SalesScreen/MyDay/Area'
import SelectCompanyScreen from "../screens/SalesScreen/MyDay/SelectCompany"
import * as colors from '../constants/colors'
import DarwerMenu from '../screens/Drawer/DrawerMenu';
import CustomScreen from '../screens/SalesScreen/MyDay/Custom'
import CompanyPhScreen from '../screens/SalesScreen/MyDay/Company_Ph'
import ShipToPhScreen from '../screens/SalesScreen/MyDay/ShipTo_Ph'
import AreaPhScreen from '../screens/SalesScreen/MyDay/Area_Ph'
import CustomerPhScreen from "../screens/SalesScreen/MyDay/Customer_Ph"
import NotesScreen from "../screens/SalesScreen/MyDay/Notes"
import AddContactScreen from "../screens/SalesScreen/MyDay/AddContact"
import { color } from 'react-native-reanimated';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../utility/index';
const AppStack = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen,
      navigationOptions: {
        header: null,
      },
    },

    SignIn: {
      screen: SignInScreen,
      navigationOptions: {
        header: null,
        visible: false
      },
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: {
        header: null,
        visible: false
      },
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: {
        header: null,
      },
    },
    OtpVerification: {
      screen: OtpVerificationScreen,
      navigationOptions: {
        header: null,
      },
    },
    AddVendor: {
      screen: AddVendorScreen,
      navigationOptions: {
        header: null,
      },
    },
    ResetPassword: {
      screen: ResetPasswordScreen,
      navigationOptions: {
        header: null,
      },
    },
    MyProfile: {
      screen: MyProfileScreen,
      navigationOptions: {
        header: null,
      },
    },
    Vendor: {
      screen: SelectVendor,
      navigationOptions: {
        header: null,
      },
    },
    Products: {
      screen: ProductsScreen,
      navigationOptions: {
        header: null,
      },
    },
    ProductDetails: {
      screen: ProductDetailScreen,
      navigationOptions: {
        header: null,
      },
    },
    Cart: {
      screen: CartScreen,
      navigationOptions: {
        header: null,
      },
    },
    AddAddress: {
      screen: AddAddressScreen,
      navigationOptions: {
        header: null,
      },
    },
    ChooseAddress: {
      screen: ChooseAddressScreen,
      navigationOptions: {
        header: null,
      },
    },
    Reports: {
      screen: ReportsScreen,
      navigationOptions: {
        header: null,
      },
    },
    Filters: {
      screen: FiltersScreen,
      navigationOptions: {
        header: null,
      },
    },
    OrderDetail: {
      screen: OrderDetailScreen,
      navigationOptions: {
        header: null,
      },
    },
    ChangePassword: {
      screen: ChangePasswordScreen,
      navigationOptions: {
        header: null
      }
    },
    ContactUs: {
      screen: ContactUsScreen,
      navigationOptions: {
        header: null
      }
    },
    PdfViewer: {
      screen: PdfViewerScreen,
      navigationOptions: {
        header: null
      }
    },

    // sales navigation//



    RemainderScreen: {
      screen: RemainderScreen,
      navigationOptions: {
        header: null,
      },
    },


    Home: {
      screen: HomeScreen,
      navigationOptions: {
        header: null,
      },
    },

    CustomerList: {
      screen: CustomerListScreen,
      navigationOptions: {
        header: null,
      },
    },
    EditCustomer: {
      screen: EditCustomerScreen,
      navigationOptions: {
        header: null,
      },
    },
    SelectCustomer: {
      screen: SelectCustomerScreen,
      navigationOptions: {
        header: null,
      },
    },



    AddNewTaskScreen: {
      screen: AddNewTaskScreen,
      navigationOptions: {
        header: null,
      },
    },
    AddEvent: {
      screen: AddEventScreen,
      navigationOptions: {
        header: null,
      }
    },
    EditEvent: {
      screen: EditEventScreen,
      navigationOptions: {
        header: null,
      }
    },
    AllTask: {
      screen: AllTaskScreen,
      navigationOptions: {
        header: null,
      },
    },
    CategoryScreen: {
      screen: CategoryScreen,
      navigationOptions: {
        header: null,
      },
    },
    OrderList: {
      screen: OrderListScreen,
      navigationOptions: {
        header: null,
      }
    },
    AcceptNewOrder: {
      screen: AcceptNewOrderScreen,
      navigationOptions: {
        header: null
      }
    },
    CalanderScreen: {
      screen: CalanderScreenScreen,
      navigationOptions: {
        header: null
      }
    },
    TaskDetail: {
      screen: TaskDetailScreen,
      navigationOptions: {
        header: null
      }
    },
    CompanyList: {
      screen: CompanyListScreen,
      navigationOptions: {
        header: null
      }
    },
    CompanyUser: {
      screen: CompanyUserScreen,
      navigationOptions: {
        header: null
      }
    },
    WeeklyCalandar: {
      screen: WeeklyCalandarScreen,
      navigationOptions: {
        header: null
      }
    },
    AddCustomer: {
      screen: AddCustomerScreen,
      navigationOptions: {
        header: null
      }
    },
    MyDayHome: {
      screen: MyDayHomeScreen,
      navigationOptions: {
        header: null
      }
    },
    InPerson: {
      screen: InPersonScreen,
      navigationOptions: {
        header: null
      }
    },
    TaskReminder: {
      screen: TaskReminderScreen,
      navigationOptions: {
        header: null
      }
    },
    InPersonVisit: {
      screen: InPersonVisitScreen,
      navigationOptions: {
        header: null
      }
    },
    AddNotes: {
      screen: AddNotesScreen,
      navigationOptions: {
        header: null
      }
    },
    AddReminder: {
      screen: AddReminderScreen,
      navigationOptions: {
        header: null
      }
    },
    ShipTo: {
      screen: ShipToScreen,
      navigationOptions: {
        header: null
      }
    },
    ShipToInfo: {
      screen: ShipToInfoScreen,
      navigationOptions: {
        header: null
      }
    },
    AddReminder: {
      screen: AddReminderScreen,
      navigationOptions: {
        header: null

      }
    },
    SelectCustomers: {
      screen: SelectCustomersScreen,
      navigationOptions: {
        header: null

      }
    },
    Area: {
      screen: AreaScreen,
      navigationOptions: {
        header: null

      }
    },
    SelectCompany: {
      screen: SelectCompanyScreen,
      navigationOptions: {
        header: null

      }
    },
    Custom: {
      screen: CustomScreen,
      navigationOptions: {
        header: null
      }
    },
    CompanyPh: {
      screen: CompanyPhScreen,
      navigationOptions: {
        header: null
      }
    },
    ShipToPh: {
      screen: ShipToPhScreen,
      navigationOptions: {
        header: null
      }
    },
    AreaPh: {
      screen: AreaPhScreen,
      navigationOptions: {
        header: null
      }
    },
    CustomerPh: {
      screen: CustomerPhScreen,
      navigationOptions: {
        header: null
      }
    },

    Notes: {
      screen: NotesScreen,
      navigationOptions: {
        header: null
      }
    },
    AddContact: {
      screen: AddContactScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'Splash',
  },
);

const DrawerNavigator = createDrawerNavigator({
  Vendor: {
    screen: SelectVendor,
  },
  MyProfile: {
    screen: MyProfileScreen,
  },
  ChangePassword: {
    screen: ChangePasswordScreen,
  },
  ContactUs: {
    screen: ContactUsScreen,
  },
},
  {
    contentOptions: {
      style: {
        backgroundColor: 'white',
        flex: 1,
      }
    },
    navigationOptions: {
      drawerLockMode: 'locked-closed'
    },
    contentComponent: DarwerMenu,
  },
);

const TabNavigator = createBottomTabNavigator({
  tab1: {
    screen: ReportsScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "REPORTS",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "tab1") {
          icon = focused ? require('../assets/order_blue.png') :
            require('../assets/orders.png')
        }
        // else if (navigation.state.routeName === "tab2") {
        //   icon = focused ? require('../assets/order_blue.png') : require('../assets/orders.png')


        // }

        return <Image
          source={icon}
          style={{
            width: 30,
            height: 30,
          }}></Image>

      }
    })
  },
  tab2: {
    screen: SelectVendor,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "VENDOR",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "tab2") {
          icon = focused ? require('../assets/vendors_blues.png') : require('../assets/vendors.png')

        }
        return <Image
          source={icon}
          style={{
            width: 30,
            height: 30,
          }}></Image>

      }
    })
  },
  tab3: {
    screen: ProductsScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "PRODUCTS",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "tab3") {
          icon = focused ? require('../assets/products.png') :
            require('../assets/products_b.png')
        }

        return <Image
          source={icon}
          style={{
            width: 30,
            height: 30,
          }}></Image>

      }
    })
  },
}, {
  initialRouteName: "tab2",
  tabBarPosition: 'bottom',
  swipeEnabled: true,
  tabBarOptions: {
    activeTintColor: colors.primaryColor,
    activeBackgroundColor: '#FFFF',
    inactiveTintColor: 'black',
    inactiveBackgroundColor: '#FFF',
    showIcon: true,
    showLabel: true,
    style: { height: 60, elevation: 1, borderTopWidth: 1 },
    labelStyle: {
      fontSize: 10,
      fontWeight: "bold",
      marginBottom: 4
    }
  }
}
);







// sales app bottom navigation



const TabNavigator2 = createBottomTabNavigator({



  MyDay: {
    screen: MyDayHomeScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "My Day",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "MyDay") {
          icon = focused ? require('../assets/mydayblue.png') :
            require('../assets/mydaygrey.png')

        }
        let txt
        if (navigation.state.routeName === "MyDay") {
          txt = focused ? <Text style={{ color: "#199bf1", fontSize: 10, fontWeight: "bold" }}>MY DAY</Text> :
            <Text style={{ fontSize: 10, fontWeight: "bold" }}>MY DAY</Text>

        }
        return <View style={{ marginTop: 20, marginLeft: 70, marginBottom: 14, }}><Image
          source={icon}
          style={{
            width: 24,
            height: 24, marginLeft: 7
          }}></Image>

          <View>
            <Text >{txt}</Text></View>
        </View>
      }
    })
  },

  Home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon


        return <View style={{ marginTop: 20, marginBottom: 17, borderRightWidth: 1, borderRightColor: 'grey', marginRight: 30, height: 34, }}>
          <Image
            source={icon}
            style={{
              width: 2,
              height: 2,
            }}></Image>
          <Text></Text>
        </View>
      }
    })

  },
  Reports: {
    screen: ReportsScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "Reoprts",

      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "Reports") {
          icon = focused ? require('../assets/reports_blue.png') :
            require('../assets/reports.png')
        }
        let txt
        if (navigation.state.routeName === "Reports") {
          txt = focused ? <Text style={{ color: "#199bf1", fontSize: 10, fontWeight: "bold" }}>REPORTS</Text> :
            <Text style={{ fontSize: 10, fontWeight: "bold" }}>REPORTS</Text>

        }
        return <View style={{ marginTop: 20, marginRight: 70, marginBottom: 14 }}><Image
          source={icon}
          style={{
            width: 24,
            height: 24, marginLeft: 10
          }}></Image>
          <Text>{txt}</Text>
        </View>

      }
    })


  },
}, {
  initialRouteName: "Home",
  tabBarPosition: 'bottom',
  swipeEnabled: true,
  tabBarOptions: {
    activeTintColor: 'white',
    activeBackgroundColor: '#FFFF',
    inactiveTintColor: 'white',
    inactiveBackgroundColor: '#FFF',
    showIcon: true,
    showLabel: true,
    style: { height: 44, borderTopColor: 'white', borderTopWidth: 1, elevation: 20 },
    labelStyle: {
      fontSize: 1,
      fontWeight: "bold",

    }
  }
}
);

const Routes = createAppContainer(
  createSwitchNavigator({
    App: AppStack,
    Drawer: DrawerNavigator,
    VendorDrawerNavigator: VendorDrawerNavigator,
    BottomTab: TabNavigator,


    // BottomTab2 is sales app bottom navigation
    BottomTab2: TabNavigator2,
  }),
);
export default Routes;
