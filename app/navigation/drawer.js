import { createDrawerNavigator } from 'react-navigation-drawer';
import DarwerMenu from '../screens/Drawer/DrawerMenu';
import MyProfileScreen from '../screens/MyProfile';
import { createAppContainer } from 'react-navigation';
import ChangePasswordScreen from '../screens/ChangePassword/'
import ContactUsScreen from '../screens/ContactUs/';
import * as Utility from "../utility/index"
import HomeScreen from '../screens/SalesScreen/Home';
//  function initRout() {
//         Utility.getFromLocalStorge('role').then((role) => {
//             if (role) {
//                 resolve(role);
//             } else {
//               console.log('reject..')
//             }
//     })
//     // if (role.toLowerCase() == 'vendor') {
//     //     return role
//     // }
// }
// let role = initRout()
// console.log('////roleinitRout', role)

const VendorDrawerNavigator = createDrawerNavigator({
    Vendor: {
        screen: HomeScreen
    },
    MyProfile: {
        screen: MyProfileScreen,
    },
    ChangePassword: {
        screen: ChangePasswordScreen,
    },
    ContactUs: {
        screen: ContactUsScreen,
    },
},
    {
        initialRouteName: 'Vendor',
        contentOptions: {
            style: {
                backgroundColor: 'white',
                flex: 1,
            }
        },
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        },
        contentComponent: DarwerMenu,
    },
);

export default MyApp = createAppContainer(VendorDrawerNavigator);
