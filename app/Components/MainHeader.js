import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Platform } from 'react-native';
import {
    Icon, Header, Left, Right, Badge, Body
} from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import * as commanFn from '../utility/index';
import * as Utility from '../utility/index';

export default class mainHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            navigationRoute: '',
            navigateFrom: null,
            backButtonDisable: false,
            cartItems: 0,
            role: "",
            Phone: "Phone",
            InPerson: "InPerson"

        }
    }

    componentDidMount = async () => {
        let navigate = this.props.navigate
        await this.setState({
            navigationRoute: navigate.state.routeName,
        })
        let natigateFromScreen = await commanFn.getFromLocalStorge('navigateFrom')
        var role = await commanFn.getFromLocalStorge('role');
        this.setState({
            role: role
        })
        var cartItemLength = await commanFn.getFromLocalStorge('cartItemLength');
        console.log('cartItemcartItemcartItemcartItem', cartItemLength)
        await this.setState({
            cartItems: cartItemLength || 0
        })
        if (role == 'customer' && (global.user.customerGroup === '' || global.user.customerGroup === null || global.user.customerGroup === undefined)) {
            await this.setState({
                backButtonDisable: true
            })
        } else {
            await this.setState({
                backButtonDisable: false
            })
        }
        console.log('mainheader::componentDidMount::natigateFromScreen.........', natigateFromScreen)

        if (natigateFromScreen !== null) {
            await this.setState({
                navigateFrom: natigateFromScreen
            })
            await commanFn.removeAuthKey('navigateFrom')
        }
    }
    goToOrder = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                // actions: [NavigationActions.navigate({ routeName: 'PersonalScreen' })]
                actions: [NavigationActions.navigate({ routeName: 'tab1' })]
            })
        );
        navigate.navigate('tab1');

    }

    goToHome = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Home' })]
            })
        );
        navigate.navigate('BottomTab2');
    }
    goToAddNewTask = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AddNewTaskScreen' })]
            })
        );
        navigate.navigate('AddNewTaskScreen');
    }
    goToAllTask = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AllTask' })]
            })
        );
        navigate.navigate('AllTask');
    }
    goToCalandarScreen = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CalanderScreen' })]
            })
        );
        navigate.navigate('CalanderScreen');
    }
    goToCompanyList = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CompanyList' })]
            })
        );
        navigate.navigate('CompanyList');
    }
    goToOrderList = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'OrderList' })]
            })
        );
        navigate.navigate('OrderList');
    }
    goToCustomerList = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'CustomerList' })]
            })
        );
        navigate.navigate('CustomerList');
    }
    goToMyDay = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'MyDayHome' })]
            })
        );
        navigate.navigate('MyDay')
    }
    goToInPerson = (navigate) => {
        Utility.setInLocalStorge('route', this.state.InPerson)
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPerson' })]
            })
        );
        navigate.navigate('InPerson')
    }
    goToInPersonPhone = (navigate) => {
        Utility.setInLocalStorge('route', this.state.Phone)
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPerson' })]
            })
        );
        navigate.navigate('InPerson')
    }
    goToShip = async (navigate) => {
        let route = await Utility.getFromLocalStorge('route')
        console.log("route", route)
        if (route == "company") {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'SelectCompany' })]
                })
            );
            navigate.navigate('SelectCompany')
        }
        else if (route == "Area") {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Area' })]
                })
            );
            navigate.navigate('Area')
            return;
        }
        else if (route == "customer") {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'SelectCustomers' })]
                })
            );
            navigate.navigate('SelectCustomers')
            return;
        }
        else if (route == "ShipTo") {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'ShipTo' })]
                })
            );
            navigate.navigate('ShipTo')
            return;
        }
    }

    goToTaskReminder = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'TaskReminder' })]
            })
        );
        navigate.navigate('TaskReminder')
    }
    goToAddNotes = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AddNotes' })]
            })
        );
        navigate.navigate('AddNotes')
    }
    goToCustomerUser = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AddCustomer' })]
            })
        );
        navigate.navigate('AddCustomer');
    }
    goBack = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'tab3' })]
                // actions: [NavigationActions.navigate({ routeName: 'SignIn' })]
            })
        );
        navigate.navigate('tab3');

    }
    goBackToVendor = async (navigate) => {
        let role = await Utility.getFromLocalStorge('role')

        if (role == "vendor") {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Home' })]
                })
            );
            navigate.navigate('BottomTab2');
            console.log("role:::::::", role)
        }

        else {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Vendor' })]
                })
            );
            navigate.navigate('BottomTab');
        }
    }
    goBack = async (navigate) => {
        let role = await Utility.getFromLocalStorge('role')
        if (role == "vendor") {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Home' })]
                })
            );
            navigate.navigate('BottomTab2');
            console.log("role:::::::", role)
        }

        else {
            navigate.dispatch(
                StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Vendor' })]
                })
            );
            navigate.navigate('BottomTab');
        }
    }
    goToFilters = (navigate) => {

        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'tab1' })]
            })
        );
        navigate.navigate('tab1');

    }

    goToAddReminder = (navigate) => {

        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AddReminder' })]
            })
        );
        navigate.navigate('AddReminder');

    }

    goToPersonVisit = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'InPersonVisit' })]
            })
        );
        navigate.navigate('InPersonVisit')
    }
    goBackToAddress = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'AddAddress' })]
            })
        );
        navigate.navigate('AddAddress');

    }



    goBackToSalesScreen = (navigate) => {

        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Customers' })]
            })
        );
        navigate.navigate('Customers');

    }
    goToCart = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Cart' })]
            })
        );
        navigate.navigate('Cart');
    }
    goToChooseAddress = (navigate) => {
        navigate.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'ChooseAddress' })]
            })
        );
        navigate.navigate('ChooseAddress');
    }
    render() {
        let navigate = this.props.navigate
        let navigateRought = this.state.navigationRoute
        let backButton

        const backArrowBtn = <Image source={require('../assets/back.png')} resizeMode='cover' style={{ width: 25, height: 25 }} />
        const menuBtn = <Icon name="md-menu" size={30} style={{ color: '#3492d4' }} />
        console.log('............navigate.................', navigate.state.routeName)



        if (navigateRought == 'tab1') {
            backButton = (< TouchableOpacity onPress={() => navigate.goBack(null)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == "OrderDetail") {
            backButton = (< TouchableOpacity onPress={() => this.goToOrder(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == "ProductDetails") {
            backButton = (< TouchableOpacity onPress={() => this.goBack(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'tab2') {
            backButton = (<TouchableOpacity onPress={() => navigate.dispatch(DrawerActions.openDrawer())}>
                {menuBtn}
            </TouchableOpacity>)
        }
        else if (navigateRought == 'tab3') {
            if (this.state.navigateFrom === 'productscreen') {
                console.log('navigateRought::productscreen', navigateRought)
                backButton = (< TouchableOpacity onPress={() => this.goToChooseAddress(navigate)}>
                    {backArrowBtn}
                </TouchableOpacity >)
            } else {
                console.log('navigateRought::productscreen::else', navigateRought)
                backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
                    {backArrowBtn}
                </TouchableOpacity >)
            }
        }
        else if (navigateRought == 'AddAddress') {
            backButton = (< TouchableOpacity onPress={() => this.goToChooseAddress(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)

        }
        else if (navigateRought == 'ChangePassword') {
            backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'ContactUs') {
            backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'MyProfile') {
            if (this.state.backButtonDisable) {
                backButton = (null)
            } else {
                backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
                    {backArrowBtn}
                </TouchableOpacity >)
            }
        } else if (navigateRought == 'ChooseAddress') {
            backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        } else if (navigateRought == 'Home') {
            backButton = (<TouchableOpacity onPress={() => navigate.dispatch(DrawerActions.openDrawer())}>
                {menuBtn}
            </TouchableOpacity>)

        } else if (navigateRought == 'AllTask' || navigateRought == 'CalanderScreen' || navigateRought == 'CompanyList' || navigateRought == 'OrderList' || navigateRought == 'CustomerList' || navigateRought == 'MyDay') {
            backButton = (< TouchableOpacity onPress={() => this.goToHome(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'CompanyUser') {
            backButton = (< TouchableOpacity onPress={() => this.goToCompanyList(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        } else if (navigateRought == 'CategoryScreen') {
            console.log('this.state.navigateFrom ', this.state.navigateFrom)
            if (this.state.navigateFrom === 'AddNewTaskScreen') {
                console.log('navigateRought::CategoryScreen', navigateRought)
                backButton = (< TouchableOpacity onPress={() => this.goToAddNewTask(navigate)}>
                    {backArrowBtn}
                </TouchableOpacity >)
            } else {
                console.log('navigateRought::productscreen::else', navigateRought)
                backButton = (< TouchableOpacity onPress={() => this.goToAllTask(navigate)}>
                    {backArrowBtn}
                </TouchableOpacity >)
            }

        } else if (navigateRought == 'AddNewTaskScreen') {
            backButton = (< TouchableOpacity onPress={() => this.goToAllTask(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'WeeklyCalandar' || navigateRought == 'AddEvent' || navigateRought == 'EditEvent') {
            backButton = (< TouchableOpacity onPress={() => this.goToCalandarScreen(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }

        else if (navigateRought == 'CompanyUser') {
            backButton = (< TouchableOpacity onPress={() => this.goToCompanyList(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'AcceptNewOrder') {
            backButton = (< TouchableOpacity onPress={() => this.goToOrderList(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'InPerson') {
            backButton = (< TouchableOpacity onPress={() => this.goToMyDay(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'TaskReminder' || navigateRought == "Notes" || navigateRought == "AddContact") {
            backButton = (< TouchableOpacity onPress={() => this.goToAddNotes(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }

        else if (navigateRought == 'ShipTo' || navigateRought == 'Area' || navigateRought == 'SelectCustomers' || navigateRought == 'SelectCompany') {
            backButton = (< TouchableOpacity onPress={() => this.goToInPerson(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'InPersonVisit') {
            backButton = (< TouchableOpacity onPress={() => this.goToShip(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'AddReminder') {
            backButton = (< TouchableOpacity onPress={() => this.goToTaskReminder(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }


        else if (navigateRought == 'AddCustomer' || navigateRought == 'EditCustomer') {
            backButton = (< TouchableOpacity onPress={() => this.goToCustomerList(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }

        // else if (navigateRought == 'AddVendor') {
        //     backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
        //         {backArrowBtn}
        //     </TouchableOpacity >)
        // }
        else if (navigateRought == 'AddVendor') {
            backButton = (< TouchableOpacity onPress={() => this.goBackToVendor(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'Reports') {
            backButton = (< TouchableOpacity onPress={() => this.goBack(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'Filters') {
            backButton = (< TouchableOpacity onPress={() => this.goToFilters(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'Custom') {
            backButton = (< TouchableOpacity onPress={() => this.goToAddReminder(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'ShipToInfo' || navigateRought == 'AddNotes') {
            backButton = (< TouchableOpacity onPress={() => this.goToPersonVisit(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }
        else if (navigateRought == 'ShipToPh' || navigateRought == 'CompanyPh' || navigateRought == 'AreaPh' || navigateRought == 'CustomerPh') {
            backButton = (< TouchableOpacity onPress={() => this.goToInPersonPhone(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }



        else {
            backButton = (< TouchableOpacity onPress={() => this.goBack(navigate)}>
                {backArrowBtn}
            </TouchableOpacity >)
        }

        return (
            <Header style={[{ backgroundColor: '#dcf0fe', paddingBottom: 7 }]}>
                <Left>
                    {backButton}
                </Left>
                <Body style={{ flex: 8, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        {this.state.role == "customer" ?
                            <Image source={require('../assets/logoCustomerApp.png')} style={{ width: '48%', height: 40 }} /> : null}

                        {this.state.role == "vendor" && navigateRought == "AllTask" || navigateRought == "AddNewTaskScreen" || navigateRought == "CategoryScreen" ?
                            <Image source={require('../assets/logoTask.png')} style={{ width: '48%', height: 47 }} /> : null}
                        {this.state.role == "vendor" && navigateRought == "CustomerList" || navigateRought == "EditCustomer" || navigateRought == "AddCustomer"
                            || navigateRought == "CalanderScreen" || navigateRought == "AddEvent" || navigateRought == "EditEvent" || navigateRought == "WeeklyCalandar"
                            || navigateRought == "CompanyList" || navigateRought == "CompanyUser" || navigateRought == "OrderList" || navigateRought == "AcceptNewOrder"
                            || navigateRought == "AddNotes" || navigateRought == "AddReminder" || navigateRought == "Area" || navigateRought == "Custom"
                            || navigateRought == "InPerson" || navigateRought == "InPersonVisit" || navigateRought == "MyDay" || navigateRought == "SelectCustomers"
                            || navigateRought == "SelectCompany" || navigateRought == "ShipTo" || navigateRought == "ShipInfo" || navigateRought == "TaskReminder"
                            || navigateRought == "ShipToPh" || navigateRought == "CompanyPh" || navigateRought == "AreaPh" || navigateRought == "CustomerPh" || navigateRought == "Notes"
                            || navigateRought == "AddContact" || navigateRought == "ShipToInfo"
                            ?
                            <Image source={require('../assets/logoSalesApp.png')} style={{ width: '48%', height: 44 }} /> : null
                        }

                    </View>

                </Body>
                <Right>
                    {navigateRought == 'tab3' ?
                        < TouchableOpacity onPress={() => this.goToCart(navigate)}>
                            <View style={{
                                width: 40,
                                alignContent: 'center'
                            }}>
                                <Image source={require('../assets/cart.png')} style={{ width: 25, height: 25, }} />
                                {this.state.cartItems !== 0 ? <Badge style={{
                                    position: 'absolute',
                                    right: 1,
                                    top: -10,
                                    minWidth: 22,
                                    height: 22,
                                    borderColor: '#fff',
                                    borderWidth: 1,
                                    backgroundColor: '#199bf1',
                                }}><Text style={{ color: '#fff', fontWeight: 'bold', justifyContent: 'center', textAlign: 'center' }}>{this.state.cartItems}</Text>
                                </Badge> : null}
                            </View>
                        </TouchableOpacity>
                        : null}
                    {navigateRought == 'CustomerList' ?
                        < TouchableOpacity onPress={() => this.goToCustomerUser(navigate)}>
                            <View style={{
                                width: 40,
                                alignContent: 'center'
                            }}>
                                <Image source={require('../assets/adduserblue.png')} style={{ width: 33, height: 25, }} />

                            </View>
                        </TouchableOpacity>
                        : null}
                </Right>
            </Header >

        );
    }
}
